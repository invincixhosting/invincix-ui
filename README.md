# Telto Project

This project is built from [INVINCIX UI](https://gitlab.com/invincixhosting/invincix-ui) repository

## Add ui components

Use the pre-made script:

```sh
pnpm ui:add <component-name>
```

> This works just like the add command in the `shadcn/ui` CLI.

## What's inside?

This **Turborepo** includes the following packages/apps:

### Apps and Packages

- `web`: another [Next.js](https://nextjs.org/) app
- `ui`: a stub React component library shared by `web` applications (🚀 powered by **shadcn/ui**)
- `eslint-config-custom`: `eslint` configurations (includes `eslint-config-next` and `eslint-config-prettier`)
- `tsconfig`: `tsconfig.json`s used throughout the monorepo

Each package/app is 100% [TypeScript](https://www.typescriptlang.org/).

### Utilities

This Turborepo has some additional tools already setup for you:

- [TypeScript](https://www.typescriptlang.org/) for static type checking
- [ESLint](https://eslint.org/) for code linting
- [Prettier](https://prettier.io) for code formatting

### Installation

To install all apps and packages, run the following command:

```sh
pnpm install
```

### Develop

To develop all apps and packages, run the following command:

```sh
pnpm run dev
```

## How to develop properly?

> A guide to this project's structure

### Internationalize Configuration (i18n)

#### Add new language

- Open file `apps/web/i18n/settings.ts` and change the configurations.
- Add a new language folder inside `apps/web/i18n/dictionaries`, separate each module by a namespace.
  For example, to add new language for a new page (called "example") inside `mechanic app`, create a new object `example` then add the translations inside `dictionaries/en/mechanic.json` file

#### Use the translations inside components:

- For client side components:

  ```ts
  "use client";

  import { useTranslation } from "@/i18n/client";
  import { LocaleTypes } from "@/i18n/settings";

  const Header = (locale: LocaleTypes) => {
    const { t } = useTranslation(locale, "common");

    // will return "Hello" in /en files
    // and return "Xin Chào" in /vi files
    return <header>{t("hello")}</header>;
  };

  export default Header;
  ```

- For server side components:

  ```ts
  import { createTranslation } from "@/i18n/server";
  import { LocaleTypes } from "@/i18n/settings";

  export default async function Page({
    params,
  }: {
    params: { locale: LocaleTypes };
  }) {
    const { t } = await createTranslation(params.locale, "common");

    // will return "Hello" in /en files
    // and return "Xin Chào" in /vi files
    return <div>{t("hello")}</div>;
  }
  ```

#### Built-in Formatting

It is very easy to format most of our data since [i18next](https://www.i18next.com/) comes with a lot of utilities we can use.

- [Number](https://www.i18next.com/translation-function/formatting#number)
- [Currency](https://www.i18next.com/translation-function/formatting#currency)
- [DateTime](https://www.i18next.com/translation-function/formatting#datetime)
- [RelativeTime](https://www.i18next.com/translation-function/formatting#relativetime)
- [List](https://www.i18next.com/translation-function/formatting#list)

##### 1. Set up translation files

`en/demo.json`

```json
{
  "number": "Number: {{val, number}}",
  "currency": "Currency: {{val, currency}}",
  "dateTime": "Date/Time: {{val, datetime}}",
  "relativeTime": "Relative Time: {{val, relativetime}}",
  "list": "List: {{val, list}}",
  "weekdays": ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
}
```

`vi/demo.json`

```json
{
  "number": "Số: {{val, number}}",
  "currency": "Tiền tệ: {{val, currency}}",
  "dateTime": "Ngày/Giờ: {{val, datetime}}",
  "relativeTime": "Giờ tương ứng: {{val, relativetime}}",
  "list": "Danh sách: {{val, list}}",
  "weekdays": ["Thứ Hai", "Thứ Ba", "Thứ Tư", "Thứ Năm", "Thứ sáu"]
}
```

##### 2. Create the component

We'll use client component in this example

```ts
"use client";
import React from "react";
import { useTranslation } from "@/i18n/client";
import type { LocaleTypes } from "@/i18n/settings";
import { useParams } from "next/navigation";

const BuiltInFormatsDemo = () => {
  let locale = useParams()?.locale as LocaleTypes;

  const { t } = useTranslation(locale, "demo");

  return (
    <div>
      <p>
        {/* "number": "Number: {{val, number}}", */}
        {t("number", {
          val: 123456789.0123,
        })}
      </p>
      <p>
        {/* "currency": "Currency: {{val, currency}}", */}
        {t("currency", {
          val: 123456789.0123,
          style: "currency",
          currency: "USD",
        })}
      </p>

      <p>
        {/* "dateTime": "Date/Time: {{val, datetime}}", */}
        {t("dateTime", {
          val: new Date(1234567890123),
          formatParams: {
            val: {
              weekday: "long",
              year: "numeric",
              month: "long",
              day: "numeric",
            },
          },
        })}
      </p>

      <p>
        {/* "relativeTime": "Relative Time: {{val, relativetime}}", */}
        {t("relativeTime", {
          val: 12,
          style: "long",
        })}
      </p>

      <p>
        {/* "list": "List: {{val, list}}", */}
        {t("list", {
          // https://www.i18next.com/translation-function/objects-and-arrays#objects
          // Check the link for more details on `returnObjects`
          val: t("weekdays", { returnObjects: true }),
        })}
      </p>
    </div>
  );
};

export default BuiltInFormatsDemo;
```
