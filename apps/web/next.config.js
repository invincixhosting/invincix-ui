module.exports = {
  reactStrictMode: true,
  transpilePackages: ["ui"],
  pageExtensions: ['ts', 'tsx']
};
