"use client";

import { Avatar, Button, Tooltip } from "@ui/components/common";
import { TDataTableColumn } from "@ui/components/data-table";
import { DataTable } from "@ui/components/data-table";
import moment from "moment";
import { convertMinutes, millisToMinutesAndSeconds } from "@/lib";
import { JobCardT } from "@/services/job-card/dto";

export const columns: TDataTableColumn<JobCardT, any>[] = [
  {
    field: "image",
    title: "Image",
    cellStyle: "text-primary font-bold",
    renderCell: ({ row }) => {
      const image_path: any = row.original.module;
      return (
        <Avatar
          imgSrc={
            image_path !== "Complaint"
              ? `${process.env.NEXT_PUBLIC_API_ENDPOINT}/${image_path.replace(
                  "uploads/",
                  ""
                )}`
              : ""
          }
          imgAlt={row.original.task}
          fallback="V"
        />
      );
    },
  },
  {
    field: "task",
    title: "Service",
    sort: true,
    cellStyle: "text-primary font-bold",
    renderCell: ({ row }) => {
      return (
        <div className="flex items-center space-x-3">
          <p className="mt-2 text-primary font-bold">{row.original.task}</p>
        </div>
      );
    },
  },
  {
    field: "details",
    title: "Vehicle",
    renderCell: ({ row }) => {
      const details: any = row.original.details;
      return (
        <div>
          <p className="text-bold text-primary">{details.vehicle_number}</p>
          <div className="flex space-x-2 mt-4 items-center">
            <span
              className={`w-5 h-5 rounded-sm border-[0.5px] border-[yellow]`}
              style={{ backgroundColor: details.color }}
            ></span>
            <p>{details.model_number}</p>
          </div>
        </div>
      );
    },
  },
  {
    field: "duration",
    title: "ESD",
    sort: true,
    renderCell: ({ row }) => {
      return <p>{convertMinutes(row.original.duration)}</p>;
    },
  },
  {
    field: "hour_log_time",
    title: "Processing Time",
    sort: true,
    renderCell: ({ row }) => {
      return (
        <p>{millisToMinutesAndSeconds(row.original.hour_log_time) + " mins"}</p>
      );
    },
  },
  {
    field: "assigned_by",
    title: "Assigned By",
    sort: true,
  },
  {
    field: "start_time",
    title: "Start At",
    sort: true,
    renderCell: ({ row }) => {
      return (
        <p>
          {row.original.start_time
            ? moment(row.original.start_time).format("LT")
            : "N/A"}
        </p>
      );
    },
  },
  {
    field: "end_time",
    title: "End At",
    sort: true,
    renderCell: ({ row }) => {
      return (
        <p>
          {row.original.end_time
            ? moment(row.original.end_time).format("LT")
            : "N/A"}
        </p>
      );
    },
  },
];

export function CompletedTasks(data: any) {
  const jobs = data?.data;
  const completedTasks = jobs
    .filter((ele) => ele.status === "Completed")
    .map((item) => {
      return {
        ...item,
        details: item.details[0],
      };
    });

  return (
    <>
      <DataTable columns={columns} data={completedTasks} />
    </>
  );
}
