"use client";

import { returnSparePart } from "@/services/job-card/requests";
import { Avatar, Button, Dialog, Tooltip } from "@ui/components/common";
import { DataTable, TDataTableColumn } from "@ui/components/data-table";
import { DialogClose } from "@ui/components/ui/dialog";
import { useToast } from "@ui/components/ui/use-toast";
import { Undo2 } from "lucide-react";
import { useState } from "react";

type SpareParts = {
  product_image: string;
  quantity: number;
  product_name: string;
  product_id: string;
  id: string;
};

export function ReturnSpareParts({
  spareParts,
  user,
  jobCardId,
}: {
  spareParts: any;
  user: any;
  jobCardId: string;
}) {
  const { toast } = useToast();
  const [loading, setLoading] = useState(false);

  async function handleReturnSparePart(rowData) {
    try {
      setLoading(true);
      const data = {
        id: rowData.id,
        partner_id: user.partner_id,
        jobCard_id: jobCardId,
        product_id: rowData.productId,
        quantity: rowData.quantity,
        is_estimated: "0",
        created_by: user.id,
      };

      const res = await returnSparePart({ jobcardSpareParts: [data] });

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "",
        });
      } else {
        toast({
          variant: "success",
          title: "Successfully returned spare part!",
          description: "",
        });
      }
      setLoading(false);
    } catch (error: any) {
      console.log(
        "🚀 ~ file: return-spare-parts.tsx:59 ~ handleReturnSparePart ~ error:",
        error
      );
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: JSON.stringify(error),
      });
    }
  }

  const sparePartColumns: TDataTableColumn<SpareParts, any>[] = [
    {
      field: "product_image",
      title: "Image",
      cellStyle: "text-primary font-bold",
      renderCell: ({ row }) => {
        const image: string = row.original.product_image;
        return (
          <Avatar
            imgSrc={`${process.env.NEXT_PUBLIC_API_ENDPOINT}/${image.replace(
              "uploads/",
              ""
            )}`}
            imgAlt={row.original.product_name}
            fallback="V"
          />
        );
      },
    },
    {
      field: "product_name",
      title: "Name",
    },
    {
      field: "quantity",
      title: "Quantity",
    },
    {
      field: "actions",
      title: "Actions",
      headerStyle: "text-right",
      renderCell: ({ row }) => {
        const id: string = row.original.id;
        const productId: string = row.original.product_id;
        const image: string = row.original.product_image;
        const name: string = row.original.product_name;
        const quantity: number = row.original.quantity;
        return (
          <div className="flex justify-end items-center space-x-4">
            <Dialog
              title="Are you sure to return this spare part?"
              content={
                <div className="p-6 flex items-center justify-center">
                  <div className="max-w-[200px] border border-secondary flex flex-col items-center justify-center p-5 space-y-4 rounded-sm">
                    <Avatar
                      imgSrc={`${
                        process.env.NEXT_PUBLIC_API_ENDPOINT
                      }/${image.replace("uploads/", "")}`}
                      imgAlt="general Checkup"
                      fallback="V"
                    />
                    <p className="text-bold">{name}</p>
                    <p className="text-muted">{quantity} Unit</p>
                  </div>
                </div>
              }
              description={`This action will return the spare part back to the inventory`}
              footer={
                <>
                  <DialogClose asChild>
                    <Button variant="outline">Cancel</Button>
                  </DialogClose>
                  <Button
                    loading={loading}
                    onClick={() =>
                      handleReturnSparePart({ id, productId, quantity })
                    }
                  >
                    Confirm
                  </Button>
                </>
              }
            >
              <Tooltip title="Return">
                <div className="rounded-full bg-primary p-2 ml-5 pointer-cursor">
                  <Undo2 className="text-primary-foreground w-4 h-4" />
                </div>
              </Tooltip>
            </Dialog>
          </div>
        );
      },
    },
  ];

  return (
    <div className="px-6 py-6">
      <DataTable columns={sparePartColumns} data={spareParts} />
    </div>
  );
}
