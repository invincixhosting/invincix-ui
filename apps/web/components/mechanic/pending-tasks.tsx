"use client";

import {
  CheckCircle,
  FolderSymlinkIcon,
  MoveUpRight,
  PackageMinus,
  PackagePlus,
  PauseOctagon,
  PlayCircle,
} from "lucide-react";
import { Avatar, Button, Tooltip } from "@ui/components/common";
import { TDataTableColumn } from "@ui/components/data-table";
import { DataTable } from "@ui/components/data-table";
import moment from "moment";
import { convertMinutes } from "@/lib";
import { Dialog } from "@ui/components/common";
import JobCardDetails from "./job-card-details";
import { ReturnSpareParts } from "./return-spare-parts";
import { SelectBay } from "./select-bay";
import { useSession } from "next-auth/react";
import { DialogClose } from "@ui/components/ui/dialog";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { BayT } from "@/services/bay/dto";
import { JobCardT, UpdateJobCardStatusDTO } from "@/services/job-card/dto";
import { updateJobCardStatus } from "@/services/job-card/requests";

export function PendingTasks({
  data,
  bayDetails,
}: {
  data: any;
  bayDetails: BayT[];
}): JSX.Element {
  const router = useRouter();
  const session = useSession();
  const { toast } = useToast();
  const [loading, setLoading] = useState<"Start" | "Paused" | "Completed" | "">(
    ""
  );

  let userFunctions = [];
  session.data.user.transformedData.forEach((item) => {
    let functions = [];
    item.category.forEach((category) => {
      functions = [...functions, ...category.functions];
    });
    userFunctions = [...userFunctions, ...functions];
  });

  const jobs = data;
  const pendingStatus = ["Start", "StartAgain", "Paused", "Pending"];
  const pendingTasks = jobs
    ?.filter((ele) => pendingStatus?.includes(ele.status))
    ?.map((item) => {
      return {
        ...item,
        details: item.details[0],
      };
    });

  async function handleUpdateJobCardStatus(
    changeToStatus: "Start" | "Paused" | "Completed",
    rowData: { id: string; jobCard_id: string; start_time: string | null }
  ) {
    try {
      setLoading(changeToStatus);
      let startTime = null;
      let endTime = null;
      let status = "";

      switch (changeToStatus) {
        case "Start":
          startTime = moment().format();
          endTime = null;
          status = `${rowData.start_time === null ? "Start" : "StartAgain"}`;
          break;
        case "Paused":
          startTime = moment().format();
          endTime = null;
          status = `Paused`;
          break;
        case "Completed":
          startTime = null;
          endTime = moment().format();
          status = `Completed`;
          break;
      }

      const data: UpdateJobCardStatusDTO = {
        id: rowData.id,
        endtime: endTime,
        starttime: startTime,
        status: status,
        updatedBy: session.data.user.id,
        Jobcard_id: rowData.jobCard_id,
      };

      const res = await updateJobCardStatus(data);
      setLoading("");

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully ${changeToStatus} job task!`,
          description: "",
        });
      }
    } catch (error) {
      console.log(
        "🚀 ~ file: pending-tasks.tsx:55 ~ handleUpdateJobCardStatus ~ error:",
        error
      );
      setLoading("");
      toast({
        variant: "error",
        title: "Error occurred!",
        description: JSON.stringify(error),
      });
    }
  }

  const columns: TDataTableColumn<JobCardT, any>[] = [
    {
      field: "image",
      title: "Image",
      cellStyle: "text-primary font-bold",
      renderCell: ({ row }) => {
        const image_path: any = row.original.module;
        return (
          <Avatar
            imgSrc={
              image_path !== "Complaint"
                ? `${process.env.NEXT_PUBLIC_API_ENDPOINT}/${image_path.replace(
                    "uploads/",
                    ""
                  )}`
                : ""
            }
            imgAlt={row.original.task}
            fallback="V"
          />
        );
      },
    },
    {
      field: "task",
      sort: true,
      title: "Service",
      cellStyle: "text-primary font-bold",
      renderCell: ({ row }) => {
        return (
          <div className="flex items-center space-x-3">
            <p className="mt-2 text-primary font-bold">{row.original.task}</p>
          </div>
        );
      },
    },
    {
      field: "details",
      title: "Vehicle",
      renderCell: ({ row }) => {
        const details: any = row.original.details;
        return (
          <div>
            <p className="text-bold text-primary">{details.vehicle_number}</p>
            <div className="flex space-x-2 mt-4 items-center">
              <span
                className={`w-5 h-5 rounded-sm border-[0.5px] border-[yellow]`}
                style={{ backgroundColor: details.color }}
              ></span>
              <p>{details.model_number}</p>
            </div>
          </div>
        );
      },
    },
    {
      field: "duration",
      sort: true,
      title: "ESD",
      renderCell: ({ row }) => {
        return <p>{convertMinutes(row.original.duration)}</p>;
      },
    },
    {
      field: "assigned_by",
      title: "Assigned By",
      sort: true,
    },
    {
      field: "start_time",
      title: "Start At",
      sort: true,
      renderCell: ({ row }) => {
        return (
          <p>
            {row.original.start_time
              ? moment(row.original.start_time).format("LT")
              : "N/A"}
          </p>
        );
      },
    },
    {
      field: "bay_name",
      title: "Bay",
      sort: true,
      cellStyle: "text-primary font-bold",
    },
    {
      field: "actions",
      title: "Actions",
      headerStyle: "text-right",
      renderCell: ({ row }) => {
        const status: string = row.original.status;
        const details: any = row.original.details;
        const services: any = details.services;
        const spare: any = details.spare;
        const complaints: any = details.complaints;
        const rowData = { details, services, spare, complaints };
        return (
          <div className="flex justify-end items-center space-x-3">
            {(userFunctions.includes("MechanicBay") ||
              session.data.user.transformedData.some(
                (item) => item.rolename == "Supervisor"
              )) && (
              <Dialog
                title="Select Bay"
                content={
                  <SelectBay
                    currentBayId={details.bay}
                    bayDetails={bayDetails}
                    jobCardId={row.original.jobCard_id}
                  />
                }
                description="Change bay of the job card"
                footer={<></>}
              >
                <Tooltip title="Select Bay">
                  <FolderSymlinkIcon className="text-muted" />
                </Tooltip>
              </Dialog>
            )}

            {(userFunctions.includes("Spare Parts Request") ||
              session.data.user.transformedData.some(
                (item) => item.rolename == "Supervisor"
              )) && (
              <Button
                className="p-0"
                variant="ghost"
                onClick={() => {
                  router.push(
                    `/mechanic/request-spares/${row.original.jobCard_id}`
                  );
                }}
              >
                <Tooltip title="Request Spare">
                  <PackagePlus className="text-muted" />
                </Tooltip>
              </Button>
            )}

            {(userFunctions.includes("Spare Parts Request") ||
              session.data.user.transformedData.some(
                (item) => item.rolename == "Supervisor"
              )) && (
              <Dialog
                title="Return Spare Parts"
                content={
                  <ReturnSpareParts
                    user={session.data.user}
                    spareParts={spare}
                    jobCardId={row.original.jobCard_id}
                  />
                }
                description="Return Spare parts after use"
                footer={
                  <>
                    <DialogClose asChild>
                      <Button variant="outline">Cancel</Button>
                    </DialogClose>
                  </>
                }
              >
                <Tooltip title="Return Spare Parts">
                  <PackageMinus className="text-muted" />
                </Tooltip>
              </Dialog>
            )}

            {(userFunctions.includes("Update Status") ||
              session.data.user.transformedData.some(
                (item) => item.rolename == "Supervisor"
              )) &&
              status.includes("Start") && (
                <Dialog
                  title="Are you sure to pause this job?"
                  content=""
                  description="This action will pause the operating job"
                  footer={
                    <>
                      <DialogClose asChild>
                        <Button variant="outline">Cancel</Button>
                      </DialogClose>
                      <Button
                        loading={loading == "Paused"}
                        onClick={() =>
                          handleUpdateJobCardStatus("Paused", {
                            id: row.original.id,
                            jobCard_id: row.original.jobCard_id,
                            start_time: row.original.start_time,
                          })
                        }
                      >
                        Confirm
                      </Button>
                    </>
                  }
                >
                  <Tooltip title="Pause">
                    <PauseOctagon className="text-muted" />
                  </Tooltip>
                </Dialog>
              )}

            {(userFunctions.includes("Update Status") ||
              session.data.user.transformedData.some(
                (item) => item.rolename == "Supervisor"
              )) && (
              <>
                {status === "Pause" || status === "Pending" ? (
                  <Dialog
                    title="Are you sure to start this job?"
                    content=""
                    description="This action will start the job"
                    footer={
                      <>
                        <DialogClose asChild>
                          <Button variant="outline">Cancel</Button>
                        </DialogClose>
                        <Button
                          loading={loading == "Start"}
                          onClick={() =>
                            handleUpdateJobCardStatus("Start", {
                              id: row.original.id,
                              jobCard_id: row.original.jobCard_id,
                              start_time: row.original.start_time,
                            })
                          }
                        >
                          Confirm
                        </Button>
                      </>
                    }
                  >
                    <Tooltip title="Start">
                      <PlayCircle className="text-muted" />
                    </Tooltip>
                  </Dialog>
                ) : (
                  <Dialog
                    title="Are you sure to complete this job?"
                    content=""
                    description="This action will complete the job"
                    footer={
                      <>
                        <DialogClose asChild>
                          <Button variant="outline">Cancel</Button>
                        </DialogClose>
                        <Button
                          loading={loading == "Completed"}
                          onClick={() =>
                            handleUpdateJobCardStatus("Completed", {
                              id: row.original.id,
                              jobCard_id: row.original.jobCard_id,
                              start_time: row.original.start_time,
                            })
                          }
                        >
                          Confirm
                        </Button>
                      </>
                    }
                  >
                    <Tooltip title="Completed">
                      <CheckCircle className="text-muted" />
                    </Tooltip>
                  </Dialog>
                )}
              </>
            )}

            <Dialog
              title="Job card details"
              content={<JobCardDetails data={rowData} />}
              description={`This is the job card details of ${details.model_number} (${details.vehicle_number})`}
              footer={
                <DialogClose asChild>
                  <Button variant="outline">Cancel</Button>
                </DialogClose>
              }
            >
              <Tooltip title="View Job Details">
                <div className="rounded-full bg-primary p-2">
                  <MoveUpRight className="text-primary-foreground w-4 h-4" />
                </div>
              </Tooltip>
            </Dialog>
          </div>
        );
      },
    },
  ];

  return (
    <>
      <DataTable columns={columns} data={pendingTasks} />
    </>
  );
}
