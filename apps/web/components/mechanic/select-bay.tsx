"use client";

import { Button } from "@ui/components/common";
import { useForm } from "react-hook-form";
import { Form } from "@ui/components/ui/form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { Radio } from "@ui/components/form";
import { DialogClose } from "@ui/components/ui/dialog";
import { updateBayByMechanic } from "@/services/job-card/requests";

const formSchema = z.object({
  bay: z.string(),
});

export function SelectBay({
  bayDetails,
  currentBayId,
  jobCardId,
}: {
  bayDetails: any;
  currentBayId: string;
  jobCardId: string;
}) {
  const { toast } = useToast();
  const [loading, setLoading] = useState(false);
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      bay: currentBayId,
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      setLoading(true);

      const res = await updateBayByMechanic({
        jobcardTask: {
          bay: values.bay,
          jobcard_id: jobCardId,
        },
      });
      setLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "",
        });
      } else {
        toast({
          variant: "success",
          title: "Successfully updated bay!",
          description: "",
        });
      }
    } catch (error: any) {
      console.log("🚀 ~ file: select-bay.tsx:64 ~ onSubmit ~ error:", error);
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: JSON.stringify(error),
      });
    }
  }

  return (
    <div className="p-6">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-6 gap-4 py-4 px-6">
            <Radio
              options={bayDetails.map((item) => {
                return { value: item.id, label: item.name };
              })}
              form={form}
              name="bay"
              defaultValue={currentBayId}
            />
          </div>
          <div className="flex w-full justify-end mt-5 space-x-3">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button loading={loading} type="submit">
              Confirm
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
