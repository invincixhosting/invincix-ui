import { Avatar } from "@ui/components/common";
import {
  Tabs,
  TabsContent,
  TabsList,
  TabsTrigger,
} from "@ui/components/ui/tabs";

interface JobCardDataT {
  services: any;
  complaints: any;
  spare: any;
}

export default function JobCardDetails({ data }: { data: JobCardDataT }) {
  return (
    <div className="grid gap-4 py-4 px-6">
      <Tabs defaultValue="services" className="w-full">
        <TabsList>
          <TabsTrigger value="services">Services</TabsTrigger>
          <TabsTrigger value="spares">Spares</TabsTrigger>
          <TabsTrigger value="complaints">Complaints</TabsTrigger>
        </TabsList>
        <TabsContent value="services">
          <div className="grid grid-cols-4 gap-4 mt-5">
            {data?.services?.length ? (
              (data?.services || [])?.map((item, i) => (
                <div
                  key={item?.service_name || `service_${i}`}
                  className="border border-secondary flex flex-col items-center justify-center p-5 space-y-4 rounded-sm"
                >
                  <Avatar
                    imgSrc={`${
                      process.env.NEXT_PUBLIC_API_ENDPOINT
                    }/${item?.service_image?.replace("uploads/", "")}`}
                    imgAlt="general Checkup"
                    fallback="V"
                  />
                  <p className="text-bold">
                    {data?.services?.length && item?.service_name}
                  </p>
                </div>
              ))
            ) : (
              <p>No service data available.</p>
            )}
          </div>
        </TabsContent>
        <TabsContent value="spares">
          <div className="grid grid-cols-4 gap-4 mt-5">
            {data?.spare?.length ? (
              (data?.spare || [])?.map((item, i) => (
                <div
                  key={item?.product_name || `spare_${i}`}
                  className="border border-secondary flex flex-col items-center justify-center p-5 space-y-4 rounded-sm"
                >
                  <Avatar
                    imgSrc={`${
                      process.env.NEXT_PUBLIC_API_ENDPOINT
                    }/${item?.product_image?.replace("uploads/", "")}`}
                    imgAlt="general Checkup"
                    fallback="V"
                  />
                  <p className="text-bold">
                    {data?.services?.length && item?.product_name}
                  </p>
                  <p className="text-muted">
                    {data?.services?.length && item?.quantity} Unit
                  </p>
                </div>
              ))
            ) : (
              <p>No spare data available.</p>
            )}
          </div>
        </TabsContent>
        <TabsContent value="complaints">
          <div className="grid grid-cols-4 gap-4 mt-5">
            {data?.complaints?.length ? (
              (data?.complaints || [])?.map((item, i) => (
                <div
                  key={item?.complaints || `complaint_${i}`}
                  className="border border-secondary flex flex-col items-center justify-center p-5 space-y-4 rounded-sm"
                >
                  <Avatar imgSrc="" imgAlt="general Checkup" fallback="V" />
                  <p className="text-bold">
                    {data?.services?.length && item?.complaints}
                  </p>
                  <p className="text-muted">
                    {data?.services?.length && item?.duration} Duration
                  </p>
                </div>
              ))
            ) : (
              <p>No complaints data available.</p>
            )}
          </div>
        </TabsContent>
      </Tabs>
    </div>
  );
}
