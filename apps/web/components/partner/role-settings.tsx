"use client";

import { Dialog, Tooltip } from "@ui/components/common";
import { DataTable } from "@ui/components/data-table";
import { PenLine } from "lucide-react";
import { useSession } from "next-auth/react";

export default function RoleSettings({ roleList }: { roleList: any }) {
  const session = useSession();
  const userFunctions = session.data.user.transformedData
    .find((item) => item.rolename == "Partner")
    .category.find((item) => item.name == "Partner").functions;

  let columns = [
    {
      field: "rolename",
      title: "Role Name",
      cellStyle: "text-primary font-bold",
    },
    {
      field: "functionName",
      title: "Function Access",
      renderCell: ({ row }) => {
        const functionName: string[] = row.original.functionName;
        return (
          <div className="flex items-center flex-wrap">
            {functionName.map((item) => (
              <span
                key={item}
                className="m-2 py-2 px-4 rounded-lg bg-secondary flex items-center justify-center"
              >
                {item}
              </span>
            ))}
          </div>
        );
      },
    },
  ];

  // if (userFunctions.includes("Role Function Mapping")) {
  //   columns = [
  //     ...columns,
  //     {
  //       field: "actions",
  //       title: "Actions",
  //       headerStyle: "text-right",
  //       renderCell: ({ row }) => {
  //         const roles: string[] = row.getValue("role");
  //         return (
  //           <div className="flex justify-end items-center">
  //             <Dialog
  //               title="Edit role"
  //               content={<></>}
  //               description="Edit role information"
  //               footer={<></>}
  //             >
  //               <Tooltip title="Edit role">
  //                 <div className="rounded-full bg-primary p-2">
  //                   <PenLine className="text-primary-foreground w-4 h-4" />
  //                 </div>
  //               </Tooltip>
  //             </Dialog>
  //           </div>
  //         );
  //       },
  //     },
  //   ];
  // }

  return (
    <div>
      <DataTable columns={columns} data={roleList} />
    </div>
  );
}
