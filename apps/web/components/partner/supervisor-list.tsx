"use client";

import Chip from "@ui/components/common/chip";
import Rating from "@ui/components/common/rating";
import { DataTable } from "@ui/components/data-table";

export default function MechanicList({
  data,
  currentUser,
}: {
  data: any;
  currentUser: any;
}) {
  const columns = [
    {
      field: "serial_no",
      title: "Serial No",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "name",
      title: "Name",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "email",
      title: "Email",
      sort: true,
    },
  ];

  return (
    <div>
      <div className="flex justify-start items-center w-full mb-5 space-x-5">
        <h1 className="text-2xl">Supervisor List</h1>
      </div>
      <DataTable columns={columns} data={data} />
    </div>
  );
}
