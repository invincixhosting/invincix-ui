"use client";

import Chip from "@ui/components/common/chip";
import Rating from "@ui/components/common/rating";
import { DataTable } from "@ui/components/data-table";

export default function MechanicList({
  data,
  currentUser,
}: {
  data: any;
  currentUser: any;
}) {
  const columns = [
    {
      field: "serial_no",
      title: "Serial No",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "name",
      title: "Name",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "pending_count",
      title: "Pending Task",
      sort: true,
    },
    {
      field: "start_count",
      title: "Start Task",
      sort: true,
    },
    {
      field: "pause_count",
      title: "Pause Task",
      sort: true,
    },
    {
      field: "complete_count",
      title: "Complete Task",
      sort: true,
    },
    {
      field: "taskCount",
      title: "Total Task",
      sort: true,
    },
    {
      field: "average_rating",
      title: "Average Rating",
      headerStyle: "text-center",
      renderCell: ({ row }) => {
        const averageRating = row.original.average_rating;
        return (
          <div className="flex justify-center items-center">
            <Rating score={averageRating} />
          </div>
        );
      },
    },
  ];

  return (
    <div>
      <div className="flex justify-start items-center w-full mb-5 space-x-5">
        <h1 className="text-2xl">Mechanic List</h1>
      </div>
      <DataTable columns={columns} data={data} />
    </div>
  );
}
