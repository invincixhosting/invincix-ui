"use client";

import ReportDateRangeForm from "@/components/partner/report-date-range-form";
import { Dialog } from "@ui/components/common";
import { useState } from "react";
import { useRouter } from "next/navigation";
import moment from "moment";
import { useToast } from "@ui/components/ui/use-toast";
import { DataTable } from "@ui/components/data-table";
import Spin from "../spin";
import { getSparePartReport } from "@/services/job-card/requests";

export default function SparePartReport({ currentUser }: { currentUser: any }) {
  const { toast } = useToast();
  const router = useRouter();
  const [formOpen, setFormOpen] = useState(true);
  const [loading, setLoading] = useState(false);
  const [tableData, setTableData] = useState([]);

  const handleSubmit = async (dateRange: { from: Date; to: Date }) => {
    try {
      setLoading(true);

      const fromDate = moment(dateRange?.from).format("YYYY-MM-DD");
      const toDate = moment(dateRange?.to).format("YYYY-MM-DD");

      const data = await getSparePartReport({
        id: currentUser?.partner_id,
        startDate: fromDate || "",
        endDate: toDate || "",
      });
      setTableData(
        data.map((item, i) => {
          return {
            ...item,
            serial_no: i + 1,
            assignedSparePart: item.assignedSparePart.join(", "),
            useSpareParts: item.useSpareParts.join(", "),
          };
        })
      );

      setLoading(false);
      setFormOpen(false);
    } catch (error) {
      console.log(
        "🚀 ~ file: garage-report.tsx:19 ~ handleSubmit ~ error:",
        error
      );
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  };

  const tableColumns = [
    {
      field: "serial_no",
      title: "Serial No",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "mechanic",
      title: "Mechanic",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "assignedSparePart",
      title: "Assigned Parts",
      sort: true,
    },
    {
      field: "useSpareParts",
      title: "Used Parts",
      sort: true,
    },
  ];

  return (
    <>
      <Dialog
        content={
          <ReportDateRangeForm
            loading={loading}
            onSubmit={(e) => {
              handleSubmit(e);
            }}
            onCancel={() => {
              setFormOpen(false);
              router.push("/partner/reports");
            }}
          />
        }
        open={formOpen}
        title="Spare Part Report"
        description="Please select date range to generate report"
      >
        <></>
      </Dialog>
      <div>
        <h1 className="text-2xl mb-5">Spare Part Report</h1>
        {loading ? (
          <Spin />
        ) : (
          <DataTable columns={tableColumns} data={tableData} />
        )}
      </div>
    </>
  );
}
