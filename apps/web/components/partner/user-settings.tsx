"use client";

import { Dialog, Tooltip } from "@ui/components/common";
import Chip from "@ui/components/common/chip";
import { DataTable } from "@ui/components/data-table";
import { PenLine, Plus } from "lucide-react";
import UserForm from "./user-form";
import { first } from "lodash";

export default function UserSettings({
  data,
  allRoles,
  currentUser,
}: {
  data: any;
  allRoles: any;
  currentUser: any;
}) {
  const columns = [
    {
      field: "name",
      title: "Name",
      sort: true,
      cellStyle: "text-primary font-bold",
    },
    {
      field: "email",
      title: "Email",
      sort: true,
    },
    {
      field: "roleName",
      title: "Role",
      sort: true,
    },
    {
      field: "is_active",
      title: "Status",
      headerStyle: "text-center",
      renderCell: ({ row }) => {
        const isActive = row.original.is_active;
        return (
          <div className="flex justify-center items-center">
            <Chip
              title={isActive ? "Active" : "Inactive"}
              color={isActive ? "primary" : "destructive"}
            />
          </div>
        );
      },
    },
    {
      field: "actions",
      title: "Actions",
      headerStyle: "text-right",
      renderCell: ({ row }) => {
        const roles: string[] = row.original.role;
        return (
          <div className="flex justify-end items-center">
            <Dialog
              title="Edit user"
              content={
                <UserForm
                  initData={{
                    id: row.original.id,
                    email: row.original.email,
                    user_type: row.original.user_type,
                    name: row.original.name,
                    role: first(roles),
                    is_active: row.original.is_active,
                  }}
                  allRoles={allRoles}
                  currentUser={currentUser}
                  type="edit"
                />
              }
              description="Edit user information"
              footer={<></>}
            >
              <Tooltip title="Edit user">
                <div className="rounded-full bg-primary p-2">
                  <PenLine className="text-primary-foreground w-4 h-4" />
                </div>
              </Tooltip>
            </Dialog>
          </div>
        );
      },
    },
  ];

  return (
    <div>
      <div className="flex justify-start items-center w-full mb-5 space-x-5">
        <h1 className="text-2xl">Users</h1>
        <Dialog
          title="Add user"
          content={
            <UserForm
              allRoles={allRoles}
              currentUser={currentUser}
              type="add"
            />
          }
          description="Add a user to your garage"
          footer={<></>}
        >
          <Tooltip title="Add User">
            <div className="rounded-full bg-primary p-2">
              <Plus className="text-primary-foreground w-6 h-6" />
            </div>
          </Tooltip>
        </Dialog>
      </div>
      <DataTable columns={columns} data={data} />
    </div>
  );
}
