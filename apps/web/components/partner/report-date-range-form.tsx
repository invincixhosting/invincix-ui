"use client";

import { Button } from "@ui/components/common";
import { DatePicker, RangePicker } from "@ui/components/form";
import { useState } from "react";

export default function ReportDateRangeForm({
  onSubmit,
  onCancel,
  loading,
}: {
  onSubmit: (e: any) => void;
  onCancel: () => void;
  loading: boolean;
}) {
  const [range, setRange] = useState({ from: new Date(), to: new Date() });

  return (
    <div className="p-6">
      <p className="mb-3">Please select a date range</p>
      <RangePicker
        selectedRange={range}
        onSelect={(e) => {
          setRange(e);
        }}
      />
      <div className="mt-5 flex justify-end items-center space-x-3">
        <Button variant="outline" onClick={onCancel}>
          Cancel
        </Button>
        <Button loading={loading} onClick={() => onSubmit(range)}>
          Submit
        </Button>
      </div>
    </div>
  );
}
