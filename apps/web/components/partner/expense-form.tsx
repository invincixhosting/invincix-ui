"use client";

import { Input, PasswordInput, Select, Switch } from "@ui/components/form";
import { Form } from "@ui/components/ui/form";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { DialogClose } from "@ui/components/ui/dialog";
import { Button } from "@ui/components/common";
import { createExpenseLog } from "@/services/expense-log/requests";

const formSchema = z.object({
  title: z.string().min(2).max(50),
  description: z.string().min(2).max(100),
  is_expense: z.boolean(),
  category: z.string().min(2, "Please select a category"),
  amount: z.string(),
});

export default function ExpenseForm({ currentUser }: { currentUser: any }) {
  const [loading, setLoading] = useState<boolean>(false);

  const { toast } = useToast();
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      title: "",
      description: "",
      is_expense: false,
      category: "stock",
      amount: "",
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      setLoading(true);

      const data = {
        partner_id: currentUser.partner_id,
        title: values.title,
        description: values.description,
        isExpenses: values.is_expense,
        category: values.category,
        amount: values.amount,
      };

      let res = await createExpenseLog(data);

      setLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "Please try again later!",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully added expense!`,
          description: "",
        });
      }
    } catch (error: any) {
      console.log("🚀 ~ file: expense-form.tsx:68 ~ onSubmit ~ error:", error)
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  }

  const expenseCategory = [
    {
      value: "stock",
      label: "Stock",
    },
    {
      value: "other",
      label: "Other",
    },
  ];

  return (
    <div className="pt-8 px-8">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-2 gap-4 bg-card p-8 rounded-lg">
            <Input form={form} name="title" placeholder="Title" />
            <Input form={form} name="description" placeholder="Description" />
            <Input form={form} name="amount" placeholder="Amount" />
            <Select
              form={form}
              name="category"
              options={expenseCategory}
              placeholder="Select Category"
            />
            <Switch name="is_expense" label="Is Expense" form={form} />
          </div>
          <div className="flex items-center justify-end space-x-3 mt-8">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button type="submit" loading={loading}>
              Submit
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
