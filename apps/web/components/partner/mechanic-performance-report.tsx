"use client";

import ReportDateRangeForm from "@/components/partner/report-date-range-form";
import { Dialog } from "@ui/components/common";
import { useState } from "react";
import { useRouter } from "next/navigation";
import moment from "moment";
import { useToast } from "@ui/components/ui/use-toast";
import { DataTable } from "@ui/components/data-table";
import Spin from "../spin";
import Rating from "@ui/components/common/rating";
import { getMechanicReport } from "@/services/job-card/requests";

export default function MechanicPerformanceReport({
  currentUser,
}: {
  currentUser: any;
}) {
  const { toast } = useToast();
  const router = useRouter();
  const [formOpen, setFormOpen] = useState(true);
  const [loading, setLoading] = useState(false);
  const [tableData, setTableData] = useState([]);

  const handleSubmit = async (dateRange: { from: Date; to: Date }) => {
    try {
      setLoading(true);

      const fromDate = moment(dateRange?.from).format("YYYY-MM-DD");
      const toDate = moment(dateRange?.to).format("YYYY-MM-DD");

      const data = await getMechanicReport({
        id: currentUser?.partner_id,
        startDate: fromDate || "",
        endDate: toDate || "",
      });
      setTableData(
        data?.mechanic.map((item, i) => {
          return {
            ...item,
            serial_no: i + 1,
            pending_count: item?.Pending || "0",
            pause_count: item?.Paused || "0",
            complete_count: item?.Completed || "0",
            average_rating: Math.round(item?.averageRating || 0),
          };
        })
      );

      setLoading(false);
      setFormOpen(false);
    } catch (error) {
      console.log(
        "🚀 ~ file: garage-report.tsx:19 ~ handleSubmit ~ error:",
        error
      );
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  };

  const tableColumns = [
    {
      field: "serial_no",
      title: "Serial No",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "name",
      title: "Name",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "pending_count",
      title: "Pending Task",
      sort: true,
    },
    {
      field: "pause_count",
      title: "Pause Task",
      sort: true,
    },
    {
      field: "complete_count",
      title: "Complete Task",
      sort: true,
    },
    {
      field: "taskCount",
      title: "Total Task",
      sort: true,
    },
    {
      field: "Revenue_Earned",
      title: "Revenue Earned",
      sort: true,
    },
    {
      field: "average_rating",
      title: "Average Rating",
      headerStyle: "text-center",
      renderCell: ({ row }) => {
        const averageRating = row.original.average_rating;
        return (
          <div className="flex justify-center items-center">
            <Rating score={averageRating} />
          </div>
        );
      },
    },
  ];

  return (
    <>
      <Dialog
        content={
          <ReportDateRangeForm
            loading={loading}
            onSubmit={(e) => {
              handleSubmit(e);
            }}
            onCancel={() => {
              setFormOpen(false);
              router.push("/partner/reports");
            }}
          />
        }
        open={formOpen}
        title="Mechanic Performance Report"
        description="Please select date range to generate report"
      >
        <></>
      </Dialog>
      <div>
        <h1 className="text-2xl mb-5">Mechanic Performance Report</h1>
        {loading ? (
          <Spin />
        ) : (
          <DataTable columns={tableColumns} data={tableData} />
        )}
      </div>
    </>
  );
}
