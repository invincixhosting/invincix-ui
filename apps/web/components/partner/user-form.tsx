"use client";

import { Input, PasswordInput, Select, Switch } from "@ui/components/form";
import { Form } from "@ui/components/ui/form";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { DialogClose } from "@ui/components/ui/dialog";
import { Button } from "@ui/components/common";
import { createUser, updateUser } from "@/services/user/requests";

const addFormSchema = z.object({
  email: z.string().min(2).max(50).email(),
  fullName: z.string().min(2).max(30),
  role: z.string().min(2, "You must select a role"),
  password: z.string().min(8).max(16),
});

const editFormSchema = z.object({
  email: z.string().min(2).max(50).email(),
  fullName: z.string().min(2),
  role: z.string(),
  isActive: z.boolean(),
});

export default function UserForm({
  type,
  allRoles,
  currentUser,
  initData,
}: {
  type: "add" | "edit";
  allRoles: any;
  currentUser: any;
  initData?: any;
}) {
  const [loading, setLoading] = useState<boolean>(false);

  const { toast } = useToast();
  const addForm = useForm<z.infer<typeof addFormSchema>>({
    resolver: zodResolver(addFormSchema),
    defaultValues: {
      email: "",
      fullName: "",
      role: "",
      password: "",
    },
  });

  const editForm = useForm<z.infer<typeof editFormSchema>>({
    resolver: zodResolver(editFormSchema),
    defaultValues: {
      email: initData?.email,
      fullName: initData?.name,
      role: initData?.role,
      isActive: initData?.is_active,
    },
  });

  const handleAddUser = async (values: z.infer<typeof addFormSchema>) => {
    const data = {
      email: values.email,
      name: values.fullName,
      role: [values.role],
      password: values.password,
    };
    await onSubmit(data);
  };

  const handleEditUser = async (values: z.infer<typeof editFormSchema>) => {
    const data = {
      email: values.email,
      name: values.fullName,
      role: values.role,
      is_active: values.isActive,
    };
    await onSubmit(data);
  };

  async function onSubmit(data: any) {
    try {
      setLoading(true);

      let res;
      if (type == "add") {
        res = await createUser({
          ...data,
          slug: "user",
          partner_id: currentUser.partner_id,
          created_by: currentUser.id,
          user_type: "User",
        });
      } else {
        res = await updateUser({
          ...data,
          slug: "user",
          partner_id: currentUser.partner_id,
          is_active: data.is_active ? true : false,
          user_id: initData?.id,
          updated_by: currentUser.id,
          user_type: initData?.user_type || "User",
        });
      }

      setLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "Please try again later!",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully ${type == "add" ? "added" : "edited"} user!`,
          description: "",
        });
      }
    } catch (error: any) {
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  }

  return (
    <div className="pt-8 px-8">
      <Form {...(type == "add" ? addForm : editForm)}>
        <form
          onSubmit={(type == "add" ? addForm : editForm).handleSubmit(
            type == "add" ? handleAddUser : handleEditUser
          )}
        >
          <div className="grid grid-cols-2 gap-4 bg-card p-8 rounded-lg">
            <Input
              form={type == "add" ? addForm : editForm}
              name="fullName"
              placeholder="Full Name"
            />
            <Input
              form={type == "add" ? addForm : editForm}
              name="email"
              placeholder="Email"
              disabled={type == "edit"}
            />
            {type == "add" && (
              <PasswordInput
                className="col-span-2"
                form={addForm}
                name="password"
              />
            )}
            <Select
              className="col-span-2"
              form={type == "add" ? addForm : editForm}
              name="role"
              disabled={type == "edit"}
              options={allRoles?.map((role) => {
                return {
                  value: role.id,
                  label: role.name,
                };
              })}
              placeholder="Select role"
            />
            {type == "edit" && (
              <Switch name="isActive" label="Status" form={editForm} />
            )}
          </div>
          <div className="flex items-center justify-end space-x-3 mt-8">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button type="submit" loading={loading}>
              Submit
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
