"use client";

import { Dialog, Tooltip } from "@ui/components/common";
import Chip from "@ui/components/common/chip";
import { DataTable } from "@ui/components/data-table";
import { Plus } from "lucide-react";
import ExpenseForm from "./expense-form";

export default function ExpenseLog({
  data,
  currentUser,
}: {
  data: any;
  currentUser: any;
}) {
  const columns = [
    {
      field: "title",
      title: "Title",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "description",
      title: "Description",
    },
    {
      field: "amount",
      title: "Amount",
      sort: true,
    },
    {
      field: "category",
      title: "Category",
    },
    {
      field: "is_expenses",
      title: "Expenses/Revenue",
      headerStyle: "text-center",
      renderCell: ({ row }) => {
        const is_expenses = row.original.is_expenses;
        return (
          <div className="flex justify-center items-center">
            <Chip
              title={is_expenses ? "Expenses" : "Revenue"}
              color={is_expenses ? "destructive" : "primary"}
            />
          </div>
        );
      },
    },
  ];

  return (
    <div>
      <div className="flex justify-start items-center w-full mb-5 space-x-5">
        <h1 className="text-2xl">Expenses</h1>
        <Dialog
          title="Add expenses"
          content={<ExpenseForm currentUser={currentUser} />}
          description=""
          footer={<></>}
        >
          <Tooltip title="Add expenses">
            <div className="rounded-full bg-primary p-2">
              <Plus className="text-primary-foreground w-6 h-6" />
            </div>
          </Tooltip>
        </Dialog>
      </div>
      <DataTable columns={columns} data={data} />
    </div>
  );
}
