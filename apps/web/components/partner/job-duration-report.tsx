"use client";

import ReportDateRangeForm from "@/components/partner/report-date-range-form";
import { Dialog } from "@ui/components/common";
import { useState } from "react";
import { useRouter } from "next/navigation";
import moment from "moment";
import { useToast } from "@ui/components/ui/use-toast";
import { DataTable } from "@ui/components/data-table";
import Spin from "../spin";
import Chip from "@ui/components/common/chip";
import { getJobDurationReport } from "@/services/job-card/requests";

export default function JobDurationReport({
  currentUser,
}: {
  currentUser: any;
}) {
  const { toast } = useToast();
  const router = useRouter();
  const [formOpen, setFormOpen] = useState(true);
  const [loading, setLoading] = useState(false);
  const [tableData, setTableData] = useState([]);

  const handleSubmit = async (dateRange: { from: Date; to: Date }) => {
    try {
      setLoading(true);

      const fromDate = moment(dateRange?.from).format("YYYY-MM-DD");
      const toDate = moment(dateRange?.to).format("YYYY-MM-DD");

      const data = await getJobDurationReport({
        id: currentUser?.partner_id,
        startDate: fromDate || "",
        endDate: toDate || "",
      });
      setTableData(
        data.map((item, i) => {
          return {
            ...item,
            serial_no: i + 1,
            created_at: moment(item.created_at).format("DD/MM/YYYY"),
            modified_at: moment(item.modified_at).format("DD/MM/YYYY"),
          };
        })
      );

      setLoading(false);
      setFormOpen(false);
    } catch (error) {
      console.log(
        "🚀 ~ file: garage-report.tsx:19 ~ handleSubmit ~ error:",
        error
      );
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  };

  const tableColumns = [
    {
      field: "serial_no",
      title: "Serial No",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "supervisor_name",
      title: "Supervisor",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "mechanic_name",
      title: "Mechanic",
      sort: true,
    },
    {
      field: "created_at",
      title: "Start Date",
      sort: true,
    },
    {
      field: "modified_at",
      title: "End Date",
      sort: true,
    },
    {
      field: "duration_days",
      title: "Durations",
      sort: true,
    },
    {
      field: "status",
      title: "Payment Status",
      sort: true,
      renderCell: ({ row }) => {
        const status = row.original.status;
        return (
          <div className="flex justify-start items-center">
            <Chip
              title={status}
              color={status == "Ready" ? "primary" : "danger"}
            />
          </div>
        );
      },
    },
  ];

  return (
    <>
      <Dialog
        content={
          <ReportDateRangeForm
            loading={loading}
            onSubmit={(e) => {
              handleSubmit(e);
            }}
            onCancel={() => {
              setFormOpen(false);
              router.push("/partner/reports");
            }}
          />
        }
        open={formOpen}
        title="Job Duration Report"
        description="Please select date range to generate report"
      >
        <></>
      </Dialog>
      <div>
        <h1 className="text-2xl mb-5">Job Duration Report</h1>
        {loading ? (
          <Spin />
        ) : (
          <DataTable columns={tableColumns} data={tableData} />
        )}
      </div>
    </>
  );
}
