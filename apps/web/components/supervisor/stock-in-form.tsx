"use client";

import { DatePicker, Input, Select } from "@ui/components/form";
import { Form } from "@ui/components/ui/form";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { DialogClose } from "@ui/components/ui/dialog";
import { Button } from "@ui/components/common";
import { createStock, updateStock } from "@/services/inventory/requests";

const formSchema = z.object({
  category: z.string().min(1, "This field is required"),
  product: z.string().min(1, "This field is required").max(50),
  quantity: z.string(),
  base_price: z.string(),
  unit_price: z.string(),
  receipt_number: z.string().min(1, "This field is required").max(256),
  po_reference_number: z.string().min(1, "This field is required").max(256),
  supplier: z.string().min(1, "This field is required").max(256),
});

export default function StockInForm({
  type,
  currentUser,
  initData,
  activeProductCategoryList,
  supplierList,
}: {
  type: "edit" | "add";
  currentUser: any;
  initData?: any;
  activeProductCategoryList: any;
  supplierList: any;
}) {
  const [loading, setLoading] = useState<boolean>(false);
  const [receiptDate, setReceiptDate] = useState(undefined);
  const [poDate, setPoDate] = useState(undefined);
  const [selectProductList, setSelectProductList] = useState(
    activeProductCategoryList?.find(
      (category: any) => category.id === initData?.product_category
    )?.Products || []
  );

  const { toast } = useToast();
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      category: initData?.product_category || "",
      product: initData?.product || "",
      quantity: initData?.quantity || 0,
      base_price: initData?.base_price || "",
      unit_price: initData?.unit_price || "",
      receipt_number: initData?.receipt_number || "",
      po_reference_number: initData?.po_reference_number || "",
      supplier: initData?.supplier || "",
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      setLoading(true);
      const data = {
        product_category: values.category,
        product: values.product,
        quantity: Number(values.quantity),
        base_price: Number(values.base_price),
        unit_price: Number(values.unit_price),
        receipt_number: values.receipt_number,
        receipt_date: receiptDate,
        po_reference_number: values.po_reference_number,
        po_date: poDate,
        supplier: values.supplier,
      };

      let res;
      if (type == "add") {
        res = await createStock({
          ...data,
          created_by: currentUser.id,
          slug: "partner_1",
          partner_id: currentUser?.partner_id,
        });
      } else {
        res = await updateStock({
          ...data,
          IsActive: true,
          stock_id: initData.id,
          updated_by: currentUser.id,
          slug: "partner_1",
          partner_id: currentUser?.partner_id,
        });
      }

      setLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "Please try again later!",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully stocked in`,
          description: "",
        });
      }
    } catch (error: any) {
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  }

  return (
    <div className="pt-8 px-8">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-2 gap-4 bg-card p-8 rounded-lg">
            <Select
              form={form}
              name="category"
              placeholder="Category*"
              onChange={(e) => {
                setSelectProductList(
                  activeProductCategoryList?.find(
                    (category: any) => category.id === e
                  )?.Products || []
                );
              }}
              options={activeProductCategoryList?.map((item) => ({
                label: item.name,
                value: item.id,
              }))}
            />
            <Select
              form={form}
              name="product"
              placeholder="Product*"
              options={selectProductList?.map((item) => ({
                label: item.name,
                value: item.id,
              }))}
            />
            <Select
              form={form}
              name="supplier"
              placeholder="Supplier*"
              options={supplierList?.map((item) => ({
                label: item.name,
                value: item.id,
              }))}
            />
            <Input form={form} name="quantity" placeholder="Quantity" />
            <Input
              form={form}
              name="base_price"
              placeholder="Base Price"
              startIcon="₹"
            />
            <Input
              form={form}
              name="unit_price"
              placeholder="Unit Price"
              startIcon="₹"
            />
            <Input
              form={form}
              name="receipt_number"
              placeholder="Receipt Number*"
            />
            <DatePicker
              title="Receipt Date*"
              selectedDate={receiptDate}
              onSelect={(value) => {
                setReceiptDate(value);
              }}
            />
            <Input
              form={form}
              name="po_reference_number"
              placeholder="PO Reference Number*"
            />
            <DatePicker
              title="PO Date*"
              selectedDate={poDate}
              onSelect={(value) => {
                setPoDate(value);
              }}
            />
          </div>
          <div className="flex items-center justify-end space-x-3 mt-8">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button
              disabled={!poDate || !receiptDate}
              type="submit"
              loading={loading}
            >
              Submit
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
