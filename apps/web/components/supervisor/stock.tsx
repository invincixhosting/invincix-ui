"use client";

import { TDataTableColumn } from "@ui/components/data-table";
import { DataTable } from "@ui/components/data-table";
import { Button, Dialog, Tooltip } from "@ui/components/common";
import { PackageMinus, Plus } from "lucide-react";
import { DialogClose } from "@ui/components/ui/dialog";
import { useToast } from "@ui/components/ui/use-toast";
import { useState } from "react";
import { stockOut } from "@/services/inventory/requests";
import StockInForm from "./stock-in-form";
import { first } from "lodash";

export default function Stock({
  data,
  currentUser,
  activeProductCategoryList,
  supplierList,
}: {
  data: any;
  currentUser: any;
  activeProductCategoryList: any;
  supplierList: any;
}) {
  const { toast } = useToast();
  const [stockOutLoading, setStockOutLoading] = useState<boolean>(false);

  const handleStockOut = async (stockId: string) => {
    try {
      setStockOutLoading(true);
      const res = await stockOut(stockId);

      setStockOutLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "Please try again later!",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully stocked out product!`,
          description: "",
        });
      }
    } catch (error: any) {
      setStockOutLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  };

  const columns: TDataTableColumn<any, any>[] = [
    {
      field: "receipt_date",
      title: "Receipt Date",
      sort: true,
    },
    {
      field: "receipt_number",
      title: "Receipt Number",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "quantity",
      title: "Quantity",
      sort: true,
    },
    {
      field: "unit_price",
      title: "Unit Price",
      sort: true,
    },
    {
      field: "po_reference_number",
      title: "PO Reference Number",
      sort: true,
    },
    {
      field: "po_date",
      title: "PO Date",
      sort: true,
    },
    {
      field: "supplier_name",
      title: "Supplier",
      sort: true,
    },
    {
      field: "actions",
      title: "Actions",
      headerStyle: "text-right",
      renderCell: ({ row }) => {
        return (
          <div className="flex justify-end items-center">
            <Dialog
              title="Stock out confirm"
              content={
                <div className="flex items-center justify-center px-6">
                  Are you sure you want to stock out this item?
                </div>
              }
              description=""
              footer={
                <div className="flex justify-end items-center space-x-3">
                  <DialogClose>
                    <Button variant="outline">No</Button>
                  </DialogClose>
                  <Button
                    loading={stockOutLoading}
                    onClick={() => handleStockOut(row.original.id)}
                  >
                    Yes
                  </Button>
                </div>
              }
            >
              <Tooltip title="Stock out">
                <div className="rounded-full bg-primary p-2">
                  <PackageMinus className="text-primary-foreground w-4 h-4" />
                </div>
              </Tooltip>
            </Dialog>
          </div>
        );
      },
    },
  ];

  return (
    <>
      <div className="flex items-center justify-end">
        <Dialog
          isTriggerBtn
          title="Stock in"
          content={
            <StockInForm
              type="edit"
              supplierList={supplierList}
              activeProductCategoryList={activeProductCategoryList}
              initData={first(data)}
              currentUser={currentUser}
            />
          }
          description=""
          footer={<></>}
        >
          <Button className="flex items-center space-x-3">
            <Plus className="w-5 h-5" />
            <span>Stock In</span>
          </Button>
        </Dialog>
      </div>
      <DataTable columns={columns} data={data} />
    </>
  );
}
