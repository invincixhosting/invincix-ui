"use client";

import { DataTable } from "@ui/components/data-table";

export default function TechnicianList({ data }: { data: any }) {
  const columns = [
    {
      field: "mechanicName",
      title: "Technician",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "pendingCount",
      title: "Pending",
      sort: true,
      renderCell: ({ row }) => <span>{row.original.pendingCount}</span>,
    },
    {
      field: "inProgressCount",
      title: "In Progress",
      sort: true,
      renderCell: ({ row }) => <span>{row.original.pendingCount}</span>,
    },
    {
      field: "completeCount",
      title: "Completed",
      sort: true,
      renderCell: ({ row }) => <span>{row.original.pendingCount}</span>,
    },
  ];

  return (
    <div>
      <DataTable columns={columns} data={data} />
    </div>
  );
}
