"use client";

import { Dialog, Tooltip } from "@ui/components/common";
import { TDataTableColumn } from "@ui/components/data-table";
import { DataTable } from "@ui/components/data-table";
import Chip from "@ui/components/common/chip";
import { Check } from "lucide-react";
import { PaymentT } from "@/services/payment/dto";
import { MarkAsPaidForm } from "./mark-as-paid-form";

export const columns: TDataTableColumn<PaymentT, any>[] = [
  {
    field: "serial_no",
    title: "Serial No",
    cellStyle: "text-primary font-bold",
    sort: true,
  },
  {
    field: "customer_name",
    title: "Customer Name",
    sort: true,
    cellStyle: "text-primary font-bold",
  },
  {
    field: "vehicle_number",
    title: "Vehicle Number",
  },
  {
    field: "contact",
    title: "Contact",
    sort: true,
  },
  {
    field: "amount",
    title: "Amount",
    sort: true,
  },
  {
    field: "status",
    title: "Status",
    sort: true,
    renderCell: ({ row }) => {
      const status = row.original.status;
      return (
        <div className="flex justify-start items-center">
          <Chip
            title={status}
            color={status == "Paid" ? "primary" : "danger"}
          />
        </div>
      );
    },
  },
  {
    field: "payment_method",
    title: "Payment Method",
    sort: true,
  },
  {
    field: "remarks",
    title: "Remarks",
    sort: true,
  },
  {
    field: "actions",
    title: "Actions",
    headerStyle: "text-right",
    renderCell: ({ row }) => {
      return (
        <div className="flex justify-end items-center">
          {row.original.status == "Unpaid" && (
            <Dialog
              title="Mark as paid"
              content={<MarkAsPaidForm paymentId={row.original.id} />}
              description="Mark this payment as paid"
              footer={<></>}
            >
              <Tooltip title="Mark as paid">
                <div className="rounded-full bg-primary p-2">
                  <Check className="text-primary-foreground w-4 h-4" />
                </div>
              </Tooltip>
            </Dialog>
          )}
        </div>
      );
    },
  },
];

export function Payment({ data }: { data: any }) {
  return (
    <>
      <DataTable columns={columns} data={data} />
    </>
  );
}
