"use client";

import { TDataTableColumn } from "@ui/components/data-table";
import { DataTable } from "@ui/components/data-table";
import { ArchiveJobCardT } from "@/services/job-card/dto";

export const columns: TDataTableColumn<ArchiveJobCardT, any>[] = [
  {
    field: "serial_no",
    title: "Serial No",
    cellStyle: "text-primary font-bold",
    sort: true,
  },
  {
    field: "customer_name",
    title: "Customer Name",
    cellStyle: "text-primary font-bold",
    renderCell: ({ row }) => (
      <span className="text-primary font-bold">
        {row.original.customer.customer_name}
      </span>
    ),
  },
  {
    field: "email",
    title: "Email",
    renderCell: ({ row }) => <span>{row.original.customer.email}</span>,
  },
  {
    field: "phone_number",
    title: "Phone Number",
    renderCell: ({ row }) => <span>{row.original.customer.phone_number}</span>,
  },
  {
    field: "vehicle_number",
    title: "Vehicle Number",
    renderCell: ({ row }) => <span>{row.original.vehicle.vehicle_number}</span>,
  },
  {
    field: "vehicle_name",
    title: "Vehicle Name",
    renderCell: ({ row }) => <span>{row.original.vehicle.vehicle_name}</span>,
  },
];

export function ArchiveJobCard({ data }: { data: any }) {
  return (
    <>
      <DataTable columns={columns} data={data} />
    </>
  );
}
