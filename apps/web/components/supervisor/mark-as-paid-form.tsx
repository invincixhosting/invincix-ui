"use client";

import { Button } from "@ui/components/common";
import { useForm } from "react-hook-form";
import { Form } from "@ui/components/ui/form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { Select, Textarea } from "@ui/components/form";
import { DialogClose } from "@ui/components/ui/dialog";
import { updatePaymentRequest } from "@/services/payment/requests";

const formSchema = z.object({
  payment_method: z.string().min(1, "This field is required"),
  remarks: z.string(),
});

export function MarkAsPaidForm({ paymentId }: { paymentId: string }) {
  const { toast } = useToast();
  const [loading, setLoading] = useState(false);
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      payment_method: "",
      remarks: "",
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      setLoading(true);

      const res = await updatePaymentRequest({
        status: "Paid",
        payment_method: values.payment_method,
        remarks: values.remarks,
        payment_id: paymentId,
      });
      setLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "",
        });
      } else {
        toast({
          variant: "success",
          title: "Successfully marked payment as paid!",
          description: "",
        });
      }
    } catch (error: any) {
      console.log("🚀 ~ file: select-bay.tsx:64 ~ onSubmit ~ error:", error);
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: JSON.stringify(error),
      });
    }
  }

  return (
    <div className="p-6">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-1 gap-4 py-4 px-6">
            <Select
              form={form}
              name="payment_method"
              placeholder="Select Payment Method"
              options={[
                {
                  value: "cash",
                  label: "Cash",
                },
                {
                  value: "credit_card",
                  label: "Credit Card",
                },
                {
                  value: "debit_card",
                  label: "Debit Card",
                },
              ]}
            />
            <Textarea form={form} name="remarks" placeholder="Enter Remarks" />
          </div>
          <div className="flex w-full justify-end mt-5 space-x-3">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button loading={loading} type="submit">
              Confirm
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
