"use client";

import { Avatar, Button, Dialog, Tooltip } from "@ui/components/common";
import SupplierForm from "./supplier-form";
import { PenLine, Plus } from "lucide-react";
import ServiceForm from "./service-form";

export function ServiceConfiguration({
  data,
  currentUser,
  activeServiceCategoryList,
}: {
  data: any;
  currentUser: any;
  activeServiceCategoryList: any;
}) {
  return (
    <div>
      <div className="flex justify-start items-center w-full mb-5 space-x-5">
        <h1 className="text-2xl">Services</h1>
        <Dialog
          title="Add service"
          content={
            <ServiceForm
              activeServiceCategoryList={activeServiceCategoryList}
              currentUser={currentUser}
              type="add"
            />
          }
          description="Add a service to your garage"
          footer={<></>}
        >
          <Tooltip title="Add Service">
            <div className="rounded-full bg-primary p-2">
              <Plus className="text-primary-foreground w-6 h-6" />
            </div>
          </Tooltip>
        </Dialog>
      </div>
      {data?.map((item) => (
        <div key={item?.categoryName} className="mb-5">
          <h2 className="mb-3 font-bold">{item?.categoryName}</h2>
          <div className="grid grid-cols-6 gap-4">
            {item?.services?.map((service) => (
              <div
                key={service?.name}
                className="rounded-lg bg-secondary p-6 flex flex-col items-center justify-center"
              >
                <Avatar
                  className="mb-2"
                  imgSrc={`${
                    process.env.NEXT_PUBLIC_API_ENDPOINT
                  }/${service?.image_path?.replace("uploads/", "")}`}
                  imgAlt="item"
                  fallback="V"
                />
                <h2 className="font-bold mb-2">{service?.name}</h2>
                <p className="text-sm">Cost: {service?.price}</p>
                <p className="text-sm text-primary">
                  Estimated Time: {service?.durationInMinute}
                </p>
                <Dialog
                  isTriggerBtn
                  title="Edit service"
                  content={
                    <ServiceForm
                      activeServiceCategoryList={activeServiceCategoryList}
                      currentUser={currentUser}
                      type="edit"
                      initData={service}
                    />
                  }
                  description="Edit service information"
                  footer={<></>}
                >
                  <Button
                    size="sm"
                    className="mt-4 flex items-center justify-center space-x-3"
                    variant="outline"
                  >
                    <PenLine className="w-4 h-4 mr-3" /> Edit
                  </Button>
                </Dialog>
              </div>
            ))}
          </div>
        </div>
      ))}
    </div>
  );
}
