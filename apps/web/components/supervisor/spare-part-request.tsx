"use client";

import { updateSparePartRequest } from "@/services/spare-part/requests";
import { Button, Dialog, Tooltip } from "@ui/components/common";
import Chip from "@ui/components/common/chip";
import { TDataTableColumn } from "@ui/components/data-table";
import { DataTable } from "@ui/components/data-table";
import { DialogClose } from "@ui/components/ui/dialog";
import { useToast } from "@ui/components/ui/use-toast";
import { Check, X } from "lucide-react";
import { useSession } from "next-auth/react";
import { useState } from "react";

export function SparePartRequest({
  data,
  currentUser,
}: {
  data: any;
  currentUser: any;
}) {
  const session = useSession();
  const { toast } = useToast();
  const [rejectLoading, setRejectLoading] = useState<boolean>(false);
  const [approveLoading, setApproveLoading] = useState<boolean>(false);

  let userFunctions = [];
  session.data.user.transformedData.forEach((item) => {
    let functions = [];
    item.category.forEach((category) => {
      functions = [...functions, ...category.functions];
    });
    userFunctions = [...userFunctions, ...functions];
  });

  const handleChangeStatus = async (
    requestDetails,
    status: "Rejected" | "Approved"
  ) => {
    try {
      setRejectLoading(true);

      const res = await updateSparePartRequest({
        request_id: requestDetails.id,
        status: status,
        stock:
          status == "Approved"
            ? requestDetails?.inventoryDetails.map((item) => ({
                inventoryid: item.id,
                quantity: requestDetails.quantity,
              }))
            : [],
        product_id: requestDetails.spare_parts,
        createdby: currentUser.id,
        jobcard_id: requestDetails.jobCard_id,
        partnerid: currentUser.partner_id,
      });
      setRejectLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully ${status} request!`,
          description: "",
        });
      }
    } catch (error: any) {
      console.log(
        "🚀 ~ file: spare-part-request.tsx:62 ~ handleReject ~ error:",
        error
      );
      setRejectLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: JSON.stringify(error),
      });
    }
  };

  const columns: TDataTableColumn<any, any>[] = [
    {
      field: "serial_no",
      title: "Serial No",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "mechanic",
      title: "Mechanic",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "product_names",
      title: "Spare Part",
      sort: true,
    },
    {
      field: "quantity",
      title: "Quantity",
      sort: true,
    },
    {
      field: "status",
      title: "Status",
      renderCell: ({ row }) => {
        const status = row.original.status;
        return (
          <div className="flex justify-start items-center">
            <Chip
              title={status}
              color={
                status == "pending"
                  ? "danger"
                  : status == "Rejected"
                  ? "destructive"
                  : "primary"
              }
            />
          </div>
        );
      },
    },
    {
      field: "actions",
      title: "Actions",
      headerStyle: "text-right",
      renderCell: ({ row }) => {
        return row.original.status == "pending" &&
          userFunctions.some((item) =>
            ["Spare Parts Acceptance", "Update"].includes(item)
          ) ? (
          <div className="flex justify-end items-center space-x-3">
            <Dialog
              title="Reject Request"
              content={
                <div className="px-6 my-5 text-center">
                  <h3 className="mb-2">
                    Are you sure you want to reject this request of spare parts?
                  </h3>
                  <p className="text-primary font-bold text-2xl">{`${row.original.product_names} (${row.original.quantity})`}</p>
                </div>
              }
              description=""
              footer={
                <div className="flex items-center justify-end space-x-3">
                  <DialogClose>
                    <Button variant="outline">Cancel</Button>
                  </DialogClose>
                  <Button
                    loading={rejectLoading}
                    onClick={() => handleChangeStatus(row.original, "Rejected")}
                  >
                    Reject
                  </Button>
                </div>
              }
            >
              <Tooltip title="Reject">
                <div className="rounded-full bg-destructive p-2">
                  <X className="text-destructive-foreground w-4 h-4" />
                </div>
              </Tooltip>
            </Dialog>
            <Dialog
              title="Approve Request"
              content={
                <div className="px-6 my-5 text-center">
                  <h3 className="mb-2">
                    Are you sure you want to approve this request of spare
                    parts?
                  </h3>
                  <p className="text-primary font-bold text-2xl">{`${row.original.product_names} (${row.original.quantity})`}</p>
                </div>
              }
              description=""
              footer={
                <div className="flex items-center justify-end space-x-3">
                  <DialogClose>
                    <Button variant="outline">Cancel</Button>
                  </DialogClose>
                  <Button
                    loading={approveLoading}
                    onClick={() => handleChangeStatus(row.original, "Approved")}
                  >
                    Approve
                  </Button>
                </div>
              }
            >
              <Tooltip title="Approve">
                <div className="rounded-full bg-primary p-2">
                  <Check className="text-primary-foreground w-4 h-4" />
                </div>
              </Tooltip>
            </Dialog>
          </div>
        ) : (
          <></>
        );
      },
    },
  ];

  return (
    <>
      <DataTable columns={columns} data={data} />
    </>
  );
}
