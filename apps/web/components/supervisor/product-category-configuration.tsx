"use client";

import { Dialog, Tooltip } from "@ui/components/common";
import Chip from "@ui/components/common/chip";
import { TDataTableColumn } from "@ui/components/data-table";
import { DataTable } from "@ui/components/data-table";
import { PenLine, Plus } from "lucide-react";
import MasterBayForm from "./master-bay-form";
import ProductCategoryForm from "./product-category-form";

export function ProductCategoryConfiguration({
  data,
  currentUser,
}: {
  data: any;
  currentUser: any;
}) {
  const columns: TDataTableColumn<any, any>[] = [
    {
      field: "name",
      title: "Category Name",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "description",
      title: "Details",
      sort: true,
    },
    {
      field: "is_active",
      title: "Status",
      sort: true,
      renderCell: ({ row }) => {
        const status = row.original.is_active ? "Active" : "Inactive";
        return (
          <div className="flex justify-start items-center">
            <Chip
              title={status}
              color={status == "Active" ? "primary" : "danger"}
            />
          </div>
        );
      },
    },
    {
      field: "actions",
      title: "Actions",
      headerStyle: "text-right",
      renderCell: ({ row }) => {
        return (
          <div className="flex justify-end items-center">
            <Dialog
              title="Edit product category"
              content={
                <ProductCategoryForm
                  currentUser={currentUser}
                  type="edit"
                  initData={row.original}
                />
              }
              description="Edit product category information"
              footer={<></>}
            >
              <Tooltip title="Edit product category">
                <div className="rounded-full bg-primary p-2">
                  <PenLine className="text-primary-foreground w-4 h-4" />
                </div>
              </Tooltip>
            </Dialog>
          </div>
        );
      },
    },
  ];

  return (
    <div>
      <div className="flex justify-start items-center w-full mb-5 space-x-5">
        <h1 className="text-2xl">Product Category</h1>
        <Dialog
          title="Add product category"
          content={<ProductCategoryForm currentUser={currentUser} type="add" />}
          description="Add a product category to your garage"
          footer={<></>}
        >
          <Tooltip title="Add product category">
            <div className="rounded-full bg-primary p-2">
              <Plus className="text-primary-foreground w-6 h-6" />
            </div>
          </Tooltip>
        </Dialog>
      </div>
      <DataTable columns={columns} data={data} />
    </div>
  );
}
