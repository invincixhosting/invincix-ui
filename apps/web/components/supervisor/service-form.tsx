"use client";

import {
  ImageUpload,
  Input,
  Select,
  Switch,
  Textarea,
} from "@ui/components/form";
import { Form } from "@ui/components/ui/form";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { DialogClose } from "@ui/components/ui/dialog";
import { Button } from "@ui/components/common";
import { createService, updateService } from "@/services/inventory/requests";

const formSchema = z.object({
  name: z.string().min(1, "This field is required").max(50),
  description: z.string().min(1, "This field is required").max(256),
  category: z.string().min(1, "This field is required").max(256),
  price: z.string().min(1, "This field is required").max(256),
  durationInMinute: z.string().min(1, "This field is required").max(256),
  image: z.any(),
  is_active: z.boolean(),
});

export default function ServiceForm({
  type,
  currentUser,
  initData,
  activeServiceCategoryList,
}: {
  type: "add" | "edit";
  currentUser: any;
  initData?: any;
  activeServiceCategoryList: any;
}) {
  const [loading, setLoading] = useState<boolean>(false);

  const { toast } = useToast();
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: type == "edit" ? initData?.name : "",
      description: type == "edit" ? initData?.description : "",
      category: type == "edit" ? initData?.serviceCategory.id : "",
      price: type == "edit" ? initData?.price : "",
      durationInMinute: type == "edit" ? initData?.durationInMinute : "",
      image: type == "edit" ? initData?.image_path : "",
      is_active: type == "edit" ? initData?.is_active : true,
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      setLoading(true);
      const data = {
        name: values.name,
        description: values.description,
        service_category_id: values.category,
        price: Number(values.price),
        image_path: values.image.base64,
        durationInMinute: values.durationInMinute,
      };

      let res;
      if (type == "add") {
        res = await createService({
          ...data,
          slug: "service",
          partner_id: currentUser.partner_id,
          created_by: currentUser.id,
        });
      } else {
        res = await updateService({
          ...data,
          slug: initData?.slug || "service",
          service_id: initData?.id,
          partner_id: currentUser.partner_id,
          updated_by: currentUser.id,
          is_active: values.is_active,
        });
      }

      setLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "Please try again later!",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully ${type == "add" ? "added" : "edited"} service!`,
          description: "",
        });
      }
    } catch (error: any) {
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  }

  return (
    <div className="pt-8 px-8">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-2 gap-4 bg-card p-8 rounded-lg">
            <ImageUpload
              className="col-span-2"
              form={form}
              label="Image"
              name="image"
              fileList={[
                type == "edit"
                  ? `${
                      process.env.NEXT_PUBLIC_API_ENDPOINT
                    }/${initData?.image_path?.replace("uploads/", "")}`
                  : "",
              ]}
            />
            <Input form={form} name="name" placeholder="Name" />
            <Select
              form={form}
              name="category"
              placeholder="Category"
              options={activeServiceCategoryList?.map((item) => ({
                label: item.name,
                value: item.id,
              }))}
            />
            <Input form={form} name="price" placeholder="Price" startIcon="₹" />
            <Input
              form={form}
              name="durationInMinute"
              placeholder="Estimated Time"
            />
            <Textarea
              className="col-span-2"
              form={form}
              name="description"
              placeholder="Description"
            />
            {type == "edit" && (
              <Switch name="is_active" label="Status" form={form} />
            )}
          </div>
          <div className="flex items-center justify-end space-x-3 mt-8">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button type="submit" loading={loading}>
              Submit
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
