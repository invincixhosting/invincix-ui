"use client";

import { Avatar, Button, Dialog, Tooltip } from "@ui/components/common";
import { Eye, PackagePlus, PenLine, Plus } from "lucide-react";
import ProductForm from "./product-form";
import { SUPERVISOR_CONFIGURATION_PRODUCT_DETAILS } from "@/const/paths";
import { useRouter } from "next/navigation";
import StockInForm from "./stock-in-form";

export function ProductConfiguration({
  data,
  currentUser,
  activeProductCategoryList,
  supplierList,
}: {
  data: any;
  currentUser: any;
  activeProductCategoryList: any;
  supplierList: any;
}) {
  const router = useRouter();

  return (
    <div>
      <div className="flex justify-start items-center w-full mb-5 space-x-5">
        <h1 className="text-2xl">Products</h1>
        <Dialog
          title="Add product"
          content={
            <ProductForm
              activeProductCategoryList={activeProductCategoryList}
              currentUser={currentUser}
              type="add"
            />
          }
          description="Add a product to your garage"
          footer={<></>}
        >
          <Tooltip title="Add product">
            <div className="rounded-full bg-primary p-2">
              <Plus className="text-primary-foreground w-6 h-6" />
            </div>
          </Tooltip>
        </Dialog>
        <Dialog
          title="Stock in"
          content={
            <StockInForm
              type="add"
              supplierList={supplierList}
              activeProductCategoryList={activeProductCategoryList}
              currentUser={currentUser}
            />
          }
          description=""
          footer={<></>}
        >
          <Tooltip title="Stock in">
            <div className="rounded-full bg-primary p-2">
              <PackagePlus className="text-primary-foreground w-6 h-6" />
            </div>
          </Tooltip>
        </Dialog>
      </div>
      {data?.map((item) => (
        <div key={item?.categoryName} className="mb-5">
          <h2 className="mb-3 font-bold">{item?.categoryName}</h2>
          <div className="grid grid-cols-6 gap-4">
            {item?.products?.map((product) => (
              <div
                key={product?.name}
                className="rounded-lg bg-secondary p-6 flex flex-col items-center justify-center"
              >
                <Avatar
                  className="mb-2"
                  imgSrc={`${
                    process.env.NEXT_PUBLIC_API_ENDPOINT
                  }/${product?.image_path?.replace("uploads/", "")}`}
                  imgAlt="item"
                  fallback="V"
                />
                <h2 className="font-bold mb-2">{product?.name}</h2>
                <p className="text-sm text-primary">
                  Product Code: {product?.product_code}
                </p>
                <div className="flex items-center justify-center space-x-3">
                  <Dialog
                    isTriggerBtn
                    title="Edit product"
                    content={
                      <ProductForm
                        activeProductCategoryList={activeProductCategoryList}
                        currentUser={currentUser}
                        type="edit"
                        initData={product}
                      />
                    }
                    description="Edit product information"
                    footer={<></>}
                  >
                    <Button
                      size="sm"
                      className="mt-4 flex items-center justify-center space-x-3"
                      variant="outline"
                    >
                      <PenLine className="w-4 h-4 mr-3" /> Edit
                    </Button>
                  </Dialog>
                  <Button
                    size="sm"
                    className="mt-4 flex items-center justify-center space-x-3"
                    variant="outline"
                    onClick={() =>
                      router.push(
                        SUPERVISOR_CONFIGURATION_PRODUCT_DETAILS.replace(
                          ":productId",
                          product.id
                        )
                      )
                    }
                  >
                    <Eye className="w-4 h-4 mr-3" /> View
                  </Button>
                </div>
              </div>
            ))}
          </div>
        </div>
      ))}
    </div>
  );
}
