"use client";

import { Input, Switch, Textarea } from "@ui/components/form";
import { Form } from "@ui/components/ui/form";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { DialogClose } from "@ui/components/ui/dialog";
import { Button } from "@ui/components/common";
import {
  createServiceCategory,
  updateServiceCategory,
} from "@/services/inventory/requests";

const formSchema = z.object({
  name: z.string().min(1, "This field is required").max(50),
  details: z.string().min(1, "This field is required").max(256),
  is_active: z.boolean(),
});

export default function ServiceCategoryForm({
  type,
  currentUser,
  initData,
}: {
  type: "add" | "edit";
  currentUser: any;
  initData?: any;
}) {
  const [loading, setLoading] = useState<boolean>(false);

  const { toast } = useToast();
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: type == "edit" ? initData?.name : "",
      details: type == "edit" ? initData?.serviceCategory.description : "",
      is_active: type == "edit" ? initData?.is_active : true,
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      setLoading(true);
      const data = {
        name: values.name,
        description: values.details,
        IsActive: values.is_active,
      };

      let res;
      if (type == "add") {
        res = await createServiceCategory({
          ...data,
          slug: "partner_1",
          partner_id: currentUser.partner_id,
          created_by: currentUser.id,
          services: [],
        });
      } else {
        res = await updateServiceCategory({
          ...data,
          slug: initData?.slug || "test",
          partner_id: currentUser.partner_id,
          service_category_id: initData?.service_category_id,
          updated_by: currentUser.id,
          services: [],
        });
      }

      setLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "Please try again later!",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully ${
            type == "add" ? "added" : "edited"
          } service category!`,
          description: "",
        });
      }
    } catch (error: any) {
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  }

  return (
    <div className="pt-8 px-8">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-1 gap-4 bg-card p-8 rounded-lg">
            <Input form={form} name="name" placeholder="Name" />
            <Textarea form={form} name="details" placeholder="Details" />
            {type == "edit" && (
              <Switch name="is_active" label="Status" form={form} />
            )}
          </div>
          <div className="flex items-center justify-end space-x-3 mt-8">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button type="submit" loading={loading}>
              Submit
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
