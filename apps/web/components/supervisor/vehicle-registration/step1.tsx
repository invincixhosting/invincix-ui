"use client";

import { Button } from "@ui/components/common";
import { ImageUpload, Input, Radio } from "@ui/components/form";
import { Form } from "@ui/components/ui/form";
import { useToast } from "@ui/components/ui/use-toast";
import { useState } from "react";
import * as z from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { STEP } from "./vehicle-registration";

const formSchema = z.object({
  images: z.array(z.string()),
  category: z.string().min(1, "This field is required").max(50),
  type: z.string().min(1, "This field is required").max(256),
  registration_number: z.string().min(1, "This field is required").max(256),
  engine_number: z.string().min(1, "This field is required").max(256),
  chassis_number: z.string().min(1, "This field is required").max(256),
  color: z.string().min(1, "This field is required").max(256),
  odometer: z.string().min(1, "This field is required").max(256),
  customer_mobile_number: z.string().min(1, "This field is required").max(256),
  customer_name: z.string().min(1, "This field is required").max(256),
  customer_email: z.string().email().min(1, "This field is required").max(256),
  customer_address: z.string().min(1, "This field is required").max(256),
  model: z.string().min(1, "This field is required").max(256),
});

export default function Step1({
  setStep,
  setSubmitData,
}: {
  setStep: (step: STEP) => void;
  setSubmitData: (data: any) => void;
}) {
  const [loading, setLoading] = useState<boolean>(false);

  const { toast } = useToast();
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      images: [],
      category: "2wheeler",
      type: "scooter",
      registration_number: "",
      model: "",
      engine_number: "",
      color: "",
      chassis_number: "",
      odometer: "",
      customer_mobile_number: "",
      customer_name: "",
      customer_email: "",
      customer_address: "",
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      const data = {
        ...values,
        images: values.images,
      };
      console.log("🚀 ~ file: step1.tsx:61 ~ onSubmit ~ data:", data)
      setSubmitData(data);
      setStep(STEP.STEP2);
    } catch (error: any) {
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  }

  const vehicleCategories = [
    {
      label: "2 Wheeler",
      value: "2wheeler",
      types: [
        { label: "Scooter", value: "scooter" },
        { label: "Bike", value: "bike" },
      ],
    },
    {
      label: "4 Wheeler",
      value: "4wheeler",
      types: [
        { label: "Hatchback", value: "hatchback" },
        { label: "Sedan", value: "sedan" },
        { label: "SUV", value: "suv" },
        { label: "MiniTruck", value: "miniTruck" },
      ],
    },
  ];

  return (
    <div>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-2 gap-4 bg-card p-8 rounded-lg">
            <h1 className="col-span-2 text-xl font-bold text-primary">
              Vehicle information
            </h1>
            <ImageUpload
              form={form}
              name="images"
              label="Vehicle Image"
              className="col-span-2"
            />
            <Radio
              form={form}
              name="category"
              label="Vehicle Category"
              options={vehicleCategories.map((category) => ({
                label: category.label,
                value: category.value,
              }))}
            />
            <Radio
              form={form}
              name="type"
              label="Vehicle Type"
              options={
                form.getValues("category") == "2wheeler"
                  ? vehicleCategories[0].types
                  : vehicleCategories[1].types
              }
            />
            <Input
              form={form}
              name="registration_number"
              placeholder="Vehicle Registration Number"
            />
            <Input form={form} name="model" placeholder="Vehicle Model" />
            <Input
              form={form}
              name="engine_number"
              placeholder="Engine Number"
            />
            <Input
              form={form}
              name="chassis_number"
              placeholder="Chassis Number"
            />
            <Input
              form={form}
              name="odometer"
              placeholder="Odometer (Km Driven)"
            />
            <Input form={form} name="color" placeholder="Vehicle Color" />
            <h1 className="col-span-2 text-xl font-bold mt-3 text-primary">
              Customer information
            </h1>
            <Input
              form={form}
              name="customer_mobile_number"
              placeholder="Customer Mobile Number"
            />
            <Input
              form={form}
              name="customer_name"
              placeholder="Customer Name"
            />
            <Input form={form} name="customer_email" placeholder="Email" />
            <Input form={form} name="customer_address" placeholder="Address" />
          </div>
          <div className="flex items-center justify-end space-x-3 mt-8">
            <Button type="submit" loading={loading}>
              Next
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
