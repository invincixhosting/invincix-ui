"use client";

import { Avatar, Button, DropdownMenu } from "@ui/components/common";
import { STEP, SelectedService } from "./vehicle-registration";
import { useState } from "react";
import { Card, CardContent } from "@ui/components/ui/card";
import { ChevronDown } from "lucide-react";

export default function Step4({
  selectedServices,
  setStep,
  onSubmit,
  mechanicList,
}: {
  selectedServices: SelectedService[];
  setStep: (step: STEP) => void;
  onSubmit: (data: SelectedService[]) => void;
  mechanicList: any;
}) {
  const [assignedServices, setAssignedServices] = useState(selectedServices);

  return (
    <div>
      <div className="bg-card my-5 p-4 grid grid-cols-6 gap-4 rounded-lg">
        {assignedServices?.map((service) => (
          <Card key={service?.name} className="bg-secondary border-transparent">
            <CardContent className="flex flex-col items-center justify-center my-5 space-y-3 pb-0">
              <div className="flex flex-col items-center">
                <Avatar
                  imgSrc={`${
                    process.env.NEXT_PUBLIC_API_ENDPOINT
                  }/${service.image_path.replace("uploads/", "")}`}
                  imgAlt="Wheel"
                  fallback="V"
                />
                <p className="text-center text-sm text-bold mt-2">
                  {service.name}
                </p>
                <p className="text-sm text-center">₹{service?.price}/-</p>
              </div>
              <div className="my-2">
                <DropdownMenu
                  title={
                    <Button variant="outline" className="flex space-x-3">
                      <span>
                        {mechanicList.find(
                          (item) => item?.id == service?.assigned_to
                        )?.name || "Select Mechanic"}
                      </span>
                      <ChevronDown />
                    </Button>
                  }
                  menuContent={[
                    {
                      type: "label",
                      content: "Select Mechanic",
                    },
                    { type: "separator" },
                    ...mechanicList?.map((mechanic) => ({
                      type: "item",
                      content: mechanic?.name,
                      onClick: () => {
                        const currentService = assignedServices.find(
                          (item) => item.service_id == service.service_id
                        );
                        if (currentService) {
                          let _assignedServices = [...assignedServices];
                          let existedIndex =
                            _assignedServices.indexOf(currentService);
                          _assignedServices[existedIndex]["assigned_to"] =
                            mechanic?.id;
                          setAssignedServices(_assignedServices);
                        }
                      },
                    })),
                  ]}
                />
              </div>
            </CardContent>
          </Card>
        ))}
      </div>
      <div className="flex items-center justify-end space-x-3">
        <Button
          size="lg"
          variant="secondary"
          onClick={() => setStep(STEP.STEP3)}
        >
          Back
        </Button>
        <Button
          size="lg"
          disabled={assignedServices.some((item) => !Boolean(item.assigned_to))}
          onClick={() => {
            onSubmit(assignedServices);
            setStep(STEP.STEP5);
          }}
        >
          NEXT
        </Button>
      </div>
    </div>
  );
}
