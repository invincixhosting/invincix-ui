"use client";

import { Avatar, Button } from "@ui/components/common";
import { DropdownMenuSeparator } from "@ui/components/ui/dropdown-menu";
import { Frown, X } from "lucide-react";
import { Card, CardContent, CardFooter } from "@ui/components/ui/card";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { useRouter } from "next/navigation";
import {
  Tabs,
  TabsContent,
  TabsList,
  TabsTrigger,
} from "@ui/components/ui/tabs";
import { STEP } from "./vehicle-registration";
import { Form } from "@ui/components/ui/form";
import { Input } from "@ui/components/form";
import * as z from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";

const ItemCard = ({
  data,
  handleAddItem,
}: {
  data: any;
  handleAddItem: (item: any) => void;
}) => {
  return (
    <Card className="bg-secondary border-transparent min-w-[200px]">
      <CardContent className="flex flex-col items-center justify-center space-y-3 mt-5">
        <Avatar
          imgSrc={`${
            process.env.NEXT_PUBLIC_API_ENDPOINT
          }/${data.image_path.replace("uploads/", "")}`}
          imgAlt="Wheel"
          fallback="V"
        />
        <h1 className="text-primary">{data.name}</h1>
        <p>Service Cost: ₹{data?.price}/-</p>
        <p>ETD: {data?.durationInMinute} min</p>
      </CardContent>
      <CardFooter className="flex justify-center items-center">
        <Button variant="outline" onClick={() => handleAddItem(data)}>
          ADD
        </Button>
      </CardFooter>
    </Card>
  );
};

const SelectedItem = ({
  data,
  handleRemoveItem,
}: {
  data: any;
  handleRemoveItem: (item: any) => void;
}) => {
  return (
    <Card className="bg-secondary border-transparent min-w-[400px]">
      <CardContent className="flex items-center justify-between my-5 pb-0">
        <div className="flex space-x-5 items-center">
          <Avatar
            imgSrc={`${
              process.env.NEXT_PUBLIC_API_ENDPOINT
            }/${data.image_path.replace("uploads/", "")}`}
            imgAlt="Wheel"
            fallback="V"
          />
          <div>
            <p className="font-bold text-primary">{data.name}</p>
            <p>₹{data?.price}/-</p>
          </div>
        </div>
        <div className="flex space-x-2 items-center">
          <Button
            variant="destructive"
            className="rounded-full p-2 h-8"
            onClick={() => handleRemoveItem(data)}
          >
            <X className="w-4 h-4" />
          </Button>
        </div>
      </CardContent>
    </Card>
  );
};

const EmptyCart = () => {
  return (
    <div className="flex flex-col items-center justify-center text-muted min-h-[350px] max-h-[350px] min-w-[400px]">
      <Frown className="mb-4 w-8 h-8 text-muted" />
      No items selected
    </div>
  );
};

const Complaints = ({
  complaintList,
  setComplaintList,
}: {
  complaintList: string[];
  setComplaintList: (complaints: string[]) => void;
}) => {
  const formSchema = z.object({
    complaint: z.string().min(1, "This field is required"),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      complaint: "",
    },
  });

  function addComplaint(values: z.infer<typeof formSchema>) {
    if (!complaintList.includes(values.complaint)) {
      setComplaintList([...complaintList, values.complaint]);
    }
  }

  return (
    <div className="bg-card rounded-lg my-2 min-h-[calc(100vh-450px)] max-h-[calc(100vh-450px)] overflow-scroll">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(addComplaint)}>
          <div className="grid grid-cols-3 gap-4 p-4 rounded-lg">
            <Input
              className="col-span-2 w-full"
              form={form}
              name="complaint"
              placeholder="Enter Complaint"
            />
            <Button className="w-full" type="submit">
              ADD
            </Button>
          </div>
        </form>
      </Form>
      {complaintList.length ? (
        <div className="grid grid-cols-1 gap-2 px-4">
          {complaintList.map((item, i) => (
            <Card
              key={item || `complaint_step2_${i}`}
              className="bg-secondary border-transparent w-full"
            >
              <CardContent className="flex items-center justify-between my-3 pb-0">
                <p className="font-bold text-primary">{item}</p>
                <div className="flex space-x-2 items-center">
                  <Button
                    variant="destructive"
                    className="rounded-full p-2 h-8"
                    onClick={() => {
                      let _complaints = complaintList;
                      _complaints.splice(complaintList.indexOf(item), 1);
                      setComplaintList([..._complaints]);
                    }}
                  >
                    <X className="w-4 h-4" />
                  </Button>
                </div>
              </CardContent>
            </Card>
          ))}
        </div>
      ) : (
        <EmptyCart />
      )}
    </div>
  );
};

export default function Step2({
  serviceList,
  user,
  setStep,
  onSubmit,
}: {
  serviceList: any;
  user: any;
  setStep: (step: STEP) => void;
  onSubmit: (data: any) => void;
}) {
  const { toast } = useToast();
  const router = useRouter();

  const [loading, setLoading] = useState<boolean>(false);
  const [cartItems, setCartItems] = useState<any>([]);
  const [complaintList, setComplaintList] = useState<string[]>([]);

  const handleAddItem = (item: any) => {
    const existedItem = cartItems.find((cartItem) => cartItem.id == item.id);
    if (existedItem) {
      toast({
        variant: "error",
        title: "Item is already added!",
        description:
          "This item has already been added, please select other service",
      });
    } else {
      setCartItems([...cartItems, { ...item }]);
    }
  };

  const handleRemoveItem = (item: any) => {
    const existedItem = cartItems.find((cartItem) => cartItem.id == item.id);
    if (existedItem) {
      let newCartArr = [...cartItems];
      let existedIndex = newCartArr.indexOf(existedItem);
      newCartArr.splice(existedIndex, 1);
      setCartItems(newCartArr);
    }
  };

  const handleNext = async () => {
    if (cartItems?.length) {
      try {
        const data = cartItems.map((item) => {
          return {
            partner_id: user.partner_id,
            service_id: item.id,
            name: item?.name,
            price: item?.price,
            durationInMinute: item?.durationInMinute,
            image_path: item?.image_path,
            is_estimated: "0",
            assigned_to: "",
            created_by: user?.id,
          };
        });
        setStep(STEP.STEP3);
        onSubmit({ services: data, complaints: complaintList });
      } catch (error) {
        console.log(
          "🚀 ~ file: request-spare-parts.tsx:199 ~ handleNext ~ error:",
          error
        );
        toast({
          variant: "error",
          title: "Error occurred!",
          description: JSON.stringify(error),
        });
      }
    }
  };

  return (
    <div className="flex min-h-[400px]">
      <div className="mr-4 flex flex-col min-h-full justify-between border-r-1 border-secondary">
        <div>
          <Tabs defaultValue="services" className="mt-3">
            <TabsList className="flex items-center justify-center h-0">
              <TabsTrigger value="services" className="w-full py-3">
                Selected services {"  "}
                {cartItems?.length > 0 && <span>({cartItems?.length})</span>}
              </TabsTrigger>
              <TabsTrigger value="complaints" className="w-full py-3">
                Complaints {"  "}
                {complaintList?.length > 0 && (
                  <span>({complaintList?.length})</span>
                )}
              </TabsTrigger>
            </TabsList>
            <TabsContent value="services">
              <div className="bg-card rounded-lg my-2 min-h-[calc(100vh-450px)] max-h-[calc(100vh-450px)] overflow-scroll">
                {cartItems.length ? (
                  <div className="flex-col items-center justify-center space-y-3 p-4">
                    {cartItems.map((item) => (
                      <SelectedItem
                        key={item?.name}
                        data={item}
                        handleRemoveItem={handleRemoveItem}
                      />
                    ))}
                  </div>
                ) : (
                  <EmptyCart />
                )}
              </div>
            </TabsContent>
            <TabsContent value="complaints">
              <Complaints
                complaintList={complaintList}
                setComplaintList={setComplaintList}
              />
            </TabsContent>
          </Tabs>
        </div>
        <div className="flex items-center justify-center space-x-3 w-full">
          <Button
            className="w-full"
            size="lg"
            variant="secondary"
            onClick={() => setStep(STEP.STEP1)}
          >
            Back
          </Button>
          <Button
            loading={loading}
            className="w-full"
            size="lg"
            disabled={!cartItems.length}
            onClick={handleNext}
          >
            NEXT
          </Button>
        </div>
      </div>
      <div className="rounded-lg bg-card px-8 py-4 w-full max-h-[calc(100vh-350px)] overflow-y-scroll">
        <h1 className="text-primary">Services</h1>
        {serviceList?.map((category) => (
          <div key={category?.categoryName} className="mt-8">
            <h2>{category?.categoryName}</h2>
            <DropdownMenuSeparator className="my-3" />
            <div className="flex space-x-3 overflow-x-scroll">
              {category.services?.map((item) => (
                <ItemCard
                  key={item?.name}
                  handleAddItem={handleAddItem}
                  data={item}
                />
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
