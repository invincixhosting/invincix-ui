"use client";

import { cn } from "@ui/lib/utils";
import { Car, ClipboardList, Cog, Receipt, Wrench } from "lucide-react";
import { useState } from "react";
import Step1 from "./step1";
import Step5 from "./step5";
import Step4 from "./step4";
import Step3 from "./step3";
import Step2 from "./step2";
import { useToast } from "@ui/components/ui/use-toast";
import { useRouter } from "next/navigation";
import {
  createJobCard,
  createJobCardFinal,
} from "@/services/job-card/requests";
import { SUPERVISOR_JOB_CARD } from "@/const/paths";

export enum STEP {
  STEP1 = "VEHICLE & CUSTOMER DETAILS",
  STEP2 = "COMPLAINT AND JOB CARD",
  STEP3 = "SPARES",
  STEP4 = "TECH. ASSIGN",
  STEP5 = "ESTIMATE",
}

export interface SelectedService {
  partner_id: string;
  service_id: string;
  is_estimated: string;
  assigned_to: string;
  name: string;
  price: string;
  durationInMinute: string | number;
  image_path: string;
  created_by: string;
}

export interface SelectedSpare {
  partner_id: string;
  product_id: string;
  quantity: number;
  name: string;
  is_estimated: string;
  created_by: string;
  price: string;
}

export interface SubmitVehicleDataT {
  images: string[];
  category: string;
  type: string;
  registration_number: string;
  engine_number: string;
  chassis_number: string;
  odometer: string;
  customer_mobile_number: string;
  color: string;
  customer_name: string;
  customer_email: string;
  customer_address: string;
  extra_charge?: string;
  model: string;
  estimated_date?: Date;
  services: SelectedService[];
  complaints: string[];
  spares: SelectedSpare[];
}

export default function VehicleRegistration({
  serviceList,
  mechanicList,
  currentUser,
  groupedProducts,
}: {
  serviceList: any;
  mechanicList: any;
  currentUser: any;
  groupedProducts: any;
}) {
  const { toast } = useToast();
  const router = useRouter();
  const [currentStep, setCurrentStep] = useState(STEP.STEP1);
  const [loading, setLoading] = useState(false);
  const [submitData, setSubmitData] = useState<SubmitVehicleDataT>({
    images: [],
    category: "",
    type: "",
    registration_number: "",
    engine_number: "",
    chassis_number: "",
    odometer: "",
    color: "",
    customer_mobile_number: "",
    model: "",
    customer_name: "",
    customer_email: "",
    customer_address: "",
    estimated_date: new Date(),
    services: [],
    complaints: [],
    spares: [],
  });

  const steps = [
    {
      name: STEP.STEP1,
      icon: <Car />,
    },
    {
      name: STEP.STEP2,
      icon: <ClipboardList />,
    },
    {
      name: STEP.STEP3,
      icon: <Cog />,
    },
    {
      name: STEP.STEP4,
      icon: <Wrench />,
    },
    {
      name: STEP.STEP5,
      icon: <Receipt />,
    },
  ];

  const handleCreateVehicle = async () => {
    try {
      setLoading(true);
      const firstStageData = {
        vehicle_number: submitData?.registration_number,
        vehicle_type: submitData?.type,
        color: submitData?.color,
        model_number: submitData.model,
        engine_number: submitData.engine_number,
        chasis_number: submitData.chassis_number,
        vehicle_category: submitData.category,
        partner_id: currentUser?.partner_id,
        kilometer_driven: Number(submitData?.odometer),
        image_paths: submitData?.images,
        phone_number: submitData?.customer_mobile_number,
        name: submitData?.customer_name,
        email: submitData?.customer_email,
        address: submitData?.customer_address,
        slug: "jobcard",
        created_by: currentUser?.id,
      };

      const createRes = await createJobCard(firstStageData);
      console.log(
        "🚀 ~ file: vehicle-registration.tsx:151 ~ handleCreateVehicle ~ createRes:",
        createRes
      );

      const secondStageData = {
        jobcardComplain: submitData?.complaints.map((item) => ({
          jobCard_id: createRes?.jobcard?.JobcardId,
          complaints: item,
          duration: "120",
          assigned_to: submitData?.services[0]?.assigned_to,
          created_by: currentUser?.id,
          slug: "jobCard",
          partner_id: currentUser?.partner_id,
        })),
        jobcardService: submitData?.services.map((item) => ({
          jobCard_id: createRes?.jobcard?.JobcardId,
          service_id: item.service_id,
          is_estimated: item.is_estimated,
          assigned_to: item.assigned_to,
          created_by: currentUser?.id,
          price: item?.price,
          partner_id: currentUser?.partner_id,
        })),
        jobcardSpareParts: submitData?.spares.map((item) => ({
          jobCard_id: createRes?.jobcard?.JobcardId,
          product_id: item.product_id,
          quantity: item.quantity.toString(),
          is_estimated: item?.is_estimated,
          created_by: currentUser?.id,
          price: item?.price,
          partner_id: currentUser?.partner_id,
        })),
        jobcardExtraService: [],
      };

      const updateRes = await createJobCardFinal(secondStageData);

      setLoading(false);

      if (createRes?.statusCode >= 300 || updateRes?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully added job card!`,
          description: "",
        });
        router.push(SUPERVISOR_JOB_CARD);
      }
    } catch (error) {
      toast({
        variant: "error",
        title: "Error occurred",
        description: "Something went wrong, please try again later!",
      });
      console.log(
        "🚀 ~ file: vehicle-registration.tsx:117 ~ handleCreateVehicle ~ error:",
        error
      );
    }
  };

  return (
    <>
      <div className="flex items-center justify-around flex-wrap mb-5">
        {steps.map((item) => (
          <div
            key={item?.name}
            className="flex items-center justify-start flex-col space-y-2 max-w-[200px]"
          >
            <span
              className={cn(
                "rounded-full flex items-center justify-center w-[40px] h-[40px] bg-secondary p-3",
                currentStep == item.name && "bg-primary text-primary-foreground"
              )}
            >
              {item.icon}
            </span>
            <p
              className={cn(
                "text-[10px] text-muted text-center",
                currentStep == item.name && "text-primary font-bold"
              )}
            >
              {item.name}
            </p>
          </div>
        ))}
      </div>
      {currentStep == STEP.STEP1 && (
        <Step1 setStep={setCurrentStep} setSubmitData={setSubmitData} />
      )}
      {currentStep == STEP.STEP2 && (
        <Step2
          onSubmit={(data) => {
            setSubmitData({ ...submitData, ...data });
          }}
          setStep={setCurrentStep}
          user={currentUser}
          serviceList={serviceList}
        />
      )}
      {currentStep == STEP.STEP3 && (
        <Step3
          onSubmit={(data) => {
            setSubmitData({ ...submitData, ...data });
          }}
          setStep={setCurrentStep}
          user={currentUser}
          groupedProducts={groupedProducts}
        />
      )}
      {currentStep == STEP.STEP4 && (
        <Step4
          mechanicList={mechanicList}
          selectedServices={submitData.services}
          setStep={setCurrentStep}
          onSubmit={(data) => {
            setSubmitData({ ...submitData, services: [...data] });
          }}
        />
      )}
      {currentStep == STEP.STEP5 && (
        <Step5
          createLoading={loading}
          currentUser={currentUser}
          data={submitData}
          setStep={setCurrentStep}
          onSubmit={async (data) => {
            setSubmitData({ ...submitData, ...data });
            await handleCreateVehicle();
          }}
        />
      )}
    </>
  );
}
