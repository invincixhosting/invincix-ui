"use client";

import { Avatar, Button } from "@ui/components/common";
import { DropdownMenuSeparator } from "@ui/components/ui/dropdown-menu";
import { Frown, Minus, Plus, X } from "lucide-react";
import { Card, CardContent, CardFooter } from "@ui/components/ui/card";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { useRouter } from "next/navigation";
import {
  Tabs,
  TabsContent,
  TabsList,
  TabsTrigger,
} from "@ui/components/ui/tabs";
import { STEP } from "./vehicle-registration";
import { Form } from "@ui/components/ui/form";
import { Input } from "@ui/components/form";
import * as z from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";

interface ProductCategoryT {
  id: string;
  name: string;
  description: string;
  partner_id: string;
  slug: string;
  is_active: boolean;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
}

interface ProductT {
  id: string;
  name: string;
  description: string;
  product_code: string;
  uom: string;
  max_quantity: number;
  min_quantity: number;
  product_category_id: string;
  image_path: string;
  partner_id: string;
  slug: string;
  is_active: boolean;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
  productCategory: ProductCategoryT;
  ProductInventory: any[];
  product_category_name: string;
  product_price: number;
  product_quantity: number;
}

interface GroupProductT {
  categoryName: string;
  products: ProductT[];
}

const ItemCard = ({
  data,
  handleAddItem,
}: {
  data: ProductT;
  handleAddItem: (item: ProductT) => void;
}) => {
  return (
    <Card className="bg-secondary border-transparent min-w-[200px]">
      <CardContent className="flex flex-col items-center justify-center space-y-3 mt-5">
        <Avatar
          imgSrc={`${
            process.env.NEXT_PUBLIC_API_ENDPOINT
          }/${data.image_path.replace("uploads/", "")}`}
          imgAlt="Wheel"
          fallback="V"
        />
        <h1 className="text-xl">{data.name}</h1>
        <p className="text-primary">In Stock: {data.product_quantity}</p>
      </CardContent>
      <CardFooter className="flex justify-center items-center">
        <Button variant="outline" onClick={() => handleAddItem(data)}>
          ADD
        </Button>
      </CardFooter>
    </Card>
  );
};

const SelectedItem = ({
  data,
  handleAddItem,
  handleReduceItem,
}: {
  data: ProductT & { currentQuantity: number };
  handleAddItem: (item: ProductT & { currentQuantity: number }) => void;
  handleReduceItem: (item: ProductT & { currentQuantity: number }) => void;
}) => {
  return (
    <Card className="bg-secondary border-transparent min-w-[400px]">
      <CardContent className="flex items-center justify-between my-5 pb-0">
        <div className="flex space-x-3 items-center">
          <Avatar
            imgSrc={`${
              process.env.NEXT_PUBLIC_API_ENDPOINT
            }/${data.image_path.replace("uploads/", "")}`}
            imgAlt="Wheel"
            fallback="V"
          />
          <p className="text-sm text-bold">{data.name}</p>
        </div>
        <div className="flex space-x-2 items-center">
          <Button
            className="rounded-full p-2 h-8"
            onClick={() => handleAddItem(data)}
          >
            <Plus className="w-4 h-4" />
          </Button>
          <span className="text-sm">{data.currentQuantity}</span>
          <Button
            className="rounded-full p-2 h-8"
            onClick={() => handleReduceItem(data)}
          >
            <Minus className="w-4 h-4" />
          </Button>
        </div>
      </CardContent>
    </Card>
  );
};

const EmptyCart = () => {
  return (
    <div className="flex flex-col items-center justify-center text-muted min-h-[350px] max-h-[350px] min-w-[400px]">
      <Frown className="mb-4 w-8 h-8 text-muted" />
      No items selected
    </div>
  );
};

export default function Step3({
  groupedProducts,
  user,
  setStep,
  onSubmit,
}: {
  groupedProducts: GroupProductT[];
  user: any;
  setStep: (step: STEP) => void;
  onSubmit: (data: any) => void;
}) {
  const { toast } = useToast();
  const router = useRouter();

  const [loading, setLoading] = useState<boolean>(false);
  const [cartItems, setCartItems] = useState<
    (ProductT & { currentQuantity: number })[]
  >([]);

  const handleAddItem = (item: ProductT) => {
    const existedItem = cartItems.find((cartItem) => cartItem.id == item.id);
    if (existedItem) {
      let newCartArr = [...cartItems];
      let existedIndex = newCartArr.indexOf(existedItem);
      if (existedItem.currentQuantity < item.product_quantity) {
        newCartArr[existedIndex].currentQuantity += 1;
      } else {
        toast({
          variant: "error",
          title: "Stock limit has been reached!",
          description: "You have reached the limit of this product",
        });
      }
      setCartItems(newCartArr);
    } else {
      if (item.product_quantity) {
        setCartItems([...cartItems, { ...item, currentQuantity: 1 }]);
      } else {
        toast({
          variant: "error",
          title: "Product is out of stock!",
          description: "Please add another product",
        });
      }
    }
  };

  const handleReduceItem = (item: ProductT & { currentQuantity: number }) => {
    const existedItem = cartItems.find((cartItem) => cartItem.id == item.id);
    if (existedItem) {
      let newCartArr = [...cartItems];
      let existedIndex = newCartArr.indexOf(existedItem);
      if (existedItem.currentQuantity > 1) {
        newCartArr[existedIndex].currentQuantity -= 1;
        setCartItems(newCartArr);
      } else {
        newCartArr.splice(existedIndex, 1);
        setCartItems(newCartArr);
      }
    }
  };

  const handleNext = async () => {
    if (cartItems?.length) {
      try {
        const data = cartItems.map((item) => {
          return {
            partner_id: user.partner_id,
            product_id: item.id,
            quantity: item.currentQuantity,
            name: item?.name,
            price: item?.product_price,
            is_estimated: "0",
            created_by: user.id,
          };
        });
        setStep(STEP.STEP4);
        onSubmit({ spares: data });
      } catch (error) {
        console.log(
          "🚀 ~ file: request-spare-parts.tsx:199 ~ handleNext ~ error:",
          error
        );
        toast({
          variant: "error",
          title: "Error occurred!",
          description: JSON.stringify(error),
        });
      }
    }
  };

  return (
    <div className="flex min-h-[400px]">
      <div className="mr-4 flex flex-col min-h-full justify-between border-r-1 border-secondary">
        <div>
          <h1 className="text-xl text-bold border-b-1 border-secondary">
            Selected items
          </h1>
          <div className="bg-card rounded-lg my-2 min-h-[calc(100vh-450px)] max-h-[calc(100vh-450px)] overflow-scroll">
            {cartItems.length ? (
              <div className="flex-col items-center justify-center space-y-3 p-4">
                {cartItems.map((item) => (
                  <SelectedItem
                    key={item?.id}
                    data={item}
                    handleAddItem={handleAddItem}
                    handleReduceItem={handleReduceItem}
                  />
                ))}
              </div>
            ) : (
              <EmptyCart />
            )}
          </div>
        </div>
        <div className="flex items-center space-x-3 justify-center w-full">
          <Button
            className="w-full"
            size="lg"
            variant="secondary"
            onClick={() => setStep(STEP.STEP2)}
          >
            Back
          </Button>
          <Button
            loading={loading}
            className="w-full"
            size="lg"
            disabled={!cartItems.length}
            onClick={handleNext}
          >
            NEXT
          </Button>
        </div>
      </div>
      <div className="rounded-lg bg-card px-8 py-4 w-full max-h-[calc(100vh-350px)] overflow-y-scroll">
        <h1 className="text-xl text-primary">Spare Parts</h1>
        {groupedProducts?.map((category) => (
          <div key={category?.categoryName} className="mt-8">
            <h2 className="text-xl">{category.categoryName}</h2>
            <DropdownMenuSeparator className="my-3" />
            <div className="flex space-x-3 overflow-x-scroll">
              {category.products?.map((item, i) => (
                <ItemCard
                  key={item?.id || `product_${i}`}
                  handleAddItem={handleAddItem}
                  data={item}
                />
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
