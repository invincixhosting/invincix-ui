"use client";

import { DatePicker, Input } from "@ui/components/form";
import { Form } from "@ui/components/ui/form";
import * as z from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { STEP, SubmitVehicleDataT } from "./vehicle-registration";
import { useState } from "react";
import { Button } from "@ui/components/common";
import { convertMinutes } from "@/lib";

export default function Step5({
  currentUser,
  createLoading,
  data,
  setStep,
  onSubmit,
}: {
  createLoading: boolean;
  currentUser: any;
  data: SubmitVehicleDataT;
  setStep: (step: STEP) => void;
  onSubmit: (data: { extra_charge?: string; estimated_date?: Date }) => void;
}) {
  const [estimatedDate, setEstimatedDate] = useState(new Date());
  const serviceSum = Math.floor(
    data?.services?.reduce((acc, service) => acc + parseInt(service.price), 0)
  );

  const spareSum = Math.floor(
    data?.spares?.reduce((acc, product) => acc + Number(product.price), 0)
  );

  const formSchema = z.object({
    extra_charge: z.string(),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      extra_charge: "0",
    },
  });

  function onCreateVehicle(values: z.infer<typeof formSchema>) {
    onSubmit({ ...values, estimated_date: estimatedDate });
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onCreateVehicle)}>
        <div>
          <div className="min-h-[calc(100vh-350px)] max-h-[calc(100vh-350px)] grid grid-cols-3 gap-4">
            <div className="bg-card rounded-lg p-4 flex flex-col justify-between">
              <h1 className="font-bold text-primary mb-5">Services</h1>
              <div className="min-h-[calc(100vh-500px)] max-h-[calc(100vh-500px)] overflow-y-scroll">
                {data?.services.map((service) => (
                  <div
                    key={service?.name}
                    className="flex items-center justify-between py-2 px-4 my-2 rounded-lg bg-secondary"
                  >
                    <div>
                      <p className="font-bold">{service?.name}</p>
                      <p>ETD (In Minutes) - {service?.durationInMinute}</p>
                    </div>
                    <p className="font-bold">₹ {service?.price}</p>
                  </div>
                ))}
              </div>
              <div className="flex items-center justify-between py-2 px-4 my-2 rounded-lg bg-primary text-primary-foreground">
                <p className="font-bold">Total</p>
                <p className="font-bold">₹ {serviceSum}</p>
              </div>
            </div>
            <div className="bg-card rounded-lg p-4 flex flex-col justify-between">
              <h1 className="font-bold text-primary mb-5">Spares</h1>
              <div className="min-h-[calc(100vh-500px)] max-h-[calc(100vh-500px)] overflow-y-scroll">
                {data?.spares.map((spare) => (
                  <div
                    key={spare?.name}
                    className="flex items-center justify-between py-2 px-4 my-2 rounded-lg bg-secondary"
                  >
                    <div>
                      <p className="font-bold">{spare.name}</p>
                      <p>Quantity: - {spare.quantity}</p>
                    </div>
                    <p className="font-bold">₹ {spare.price}</p>
                  </div>
                ))}
              </div>
              <div className="flex items-center justify-between py-2 px-4 my-2 rounded-lg bg-primary text-primary-foreground">
                <p className="font-bold">Total</p>
                <p className="font-bold">₹ {spareSum}</p>
              </div>
            </div>
            <div className="bg-card rounded-lg p-4 flex flex-col justify-between">
              <h1 className="font-bold text-primary mb-5">
                Estimate Time -{" "}
                {convertMinutes(
                  data?.services.reduce(
                    (acc, service) => acc + Number(service.durationInMinute),
                    0
                  )
                )}
              </h1>
              <div className="min-h-[calc(100vh-500px)] max-h-[calc(100vh-500px)] overflow-y-scroll">
                <div className="rounded-lg bg-secondary py-2 px-4 flex flex-col space-y-2">
                  <div className="flex items-center justify-between">
                    <p>Services</p>
                    <p className="font-bold">₹ {Math.floor(serviceSum)}</p>
                  </div>
                  <div className="flex items-center justify-between">
                    <p>Spares</p>
                    <p className="font-bold">₹ {Math.floor(spareSum)}</p>
                  </div>
                  <div className="flex items-center justify-between">
                    <p>Extra Service</p>
                    <p className="font-bold">
                      ₹ {Math.floor(Number(form.getValues("extra_charge")))}
                    </p>
                  </div>
                  <div className="flex items-center justify-between">
                    <p>Extra Service</p>
                    <p className="font-bold">
                      ₹ {Math.floor(Number(form.getValues("extra_charge")))}
                    </p>
                  </div>
                  <div className="flex items-center justify-between">
                    <p>Total</p>
                    <p className="font-bold">
                      ₹{" "}
                      {Math.floor(
                        serviceSum + Number(form.getValues("extra_charge"))
                      )}
                    </p>
                  </div>
                  <div className="flex items-center justify-between">
                    <p>Tax {currentUser?.tax_percentage}</p>
                    <p className="font-bold">
                      ₹{" "}
                      {Math.floor(
                        ((spareSum +
                          serviceSum +
                          Number(form.getValues("extra_charge"))) *
                          currentUser.tax_percentage) /
                          100
                      )}
                    </p>
                  </div>
                  <div className="flex items-center justify-between">
                    <p className="font-bold">Sub-total</p>
                    <p className="font-bold">
                      ₹{" "}
                      {Math.round(
                        Math.floor(
                          serviceSum +
                            +spareSum +
                            Number(form.getValues("extra_charge"))
                        ) +
                          Math.floor(
                            (serviceSum +
                              spareSum +
                              Number(form.getValues("extra_charge"))) *
                              currentUser.tax_percentage
                          ) /
                            100
                      )}
                    </p>
                  </div>
                </div>
                <div className="mt-5">
                  <Input
                    form={form}
                    className="mb-2"
                    name="extra_charge"
                    placeholder="Extra Service Charges"
                    startIcon="₹"
                  />
                  <DatePicker
                    title="Estimated Date"
                    selectedDate={estimatedDate}
                    onSelect={(value) => {
                      setEstimatedDate(value);
                    }}
                  />
                </div>
              </div>
              <div className="flex items-center justify-between py-2 px-4 my-2 rounded-lg bg-primary text-primary-foreground">
                <p className="font-bold">Total</p>
                <p className="font-bold">
                  ₹{" "}
                  {Math.floor(
                    serviceSum +
                      spareSum +
                      Number(form.getValues("extra_charge"))
                  ) +
                    Math.floor(
                      ((serviceSum +
                        spareSum +
                        Number(form.getValues("extra_charge"))) *
                        currentUser?.tax_percentage) /
                        100
                    )}
                </p>
              </div>
            </div>
          </div>

          <div className="mt-4 flex items-center justify-end space-x-3">
            <Button
              size="lg"
              variant="secondary"
              onClick={() => setStep(STEP.STEP4)}
            >
              Back
            </Button>
            <Button loading={createLoading} size="lg" type="submit">
              Create
            </Button>
          </div>
        </div>
      </form>
    </Form>
  );
}
