"use client";

import { Avatar, Button } from "@ui/components/common";
import { useForm } from "react-hook-form";
import { Form } from "@ui/components/ui/form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { Input, Select, Textarea } from "@ui/components/form";
import { DialogClose } from "@ui/components/ui/dialog";
import { updatePaymentRequest } from "@/services/payment/requests";
import Rating from "@ui/components/common/rating";
import { first } from "lodash";
import Chip from "@ui/components/common/chip";
import moment from "moment";
import { createFeedbackMechanics } from "@/services/job-card/requests";

const formSchema = z.object({
  review: z.string(),
  rating: z.string(),
});

export function MechanicProfile({ mechanicDetails }: { mechanicDetails: any }) {
  const { toast } = useToast();
  const [loading, setLoading] = useState(false);
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      review: "",
      rating: "",
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      setLoading(true);

      const res = await createFeedbackMechanics({
        mechanic_id: mechanicDetails.id,
        partner_id: mechanicDetails.partner_id,
        rating: values.rating,
        review: values.review,
      });
      setLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "",
        });
      } else {
        toast({
          variant: "success",
          title: "Successfully reviewed mechanic!",
          description: "",
        });
      }
    } catch (error: any) {
      console.log("🚀 ~ file: select-bay.tsx:64 ~ onSubmit ~ error:", error);
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: JSON.stringify(error),
      });
    }
  }

  return (
    <div className="p-6">
      <div className="grid grid-cols-2 gap-4 px-6 mb-5">
        <div className="flex items-center space-x-3">
          <span>
            <Avatar
              imgSrc=""
              fallback={first(mechanicDetails.name) || "V"}
              imgAlt={mechanicDetails?.name}
            />
          </span>
          <div>
            <h3 className="text-xl font-bold text-primary mb-2">
              {mechanicDetails?.name || ""}
            </h3>
            <Rating score={mechanicDetails.average_rating} />
          </div>
        </div>
        <div>
          <h4 className="font-bold mb-2">Specialties:</h4>
          <div className="flex items-center space-x-2 flex-wrap">
            {mechanicDetails?.specialities?.map((item) => (
              <Chip key={item} title={item} color="danger" />
            ))}
          </div>
        </div>
      </div>
      <div className="px-6">
        <h4 className="font-bold mb-2">Reviews:</h4>
        <div className="max-h-[300px] overflow-y-scroll">
          {mechanicDetails.feedbackDetails?.length ? (
            mechanicDetails.feedbackDetails?.map((feedback, i) => (
              <div
                key={feedback?.id || `feedback_${i}`}
                className="rounded-lg p-4 border border-secondary mb-2"
              >
                <div className="flex items-center justify-between mb-2">
                  <Rating score={feedback.rating} />
                  <span className="text-muted">
                    {moment(feedback.created_at).format("MMMM Do YYYY")}
                  </span>
                </div>
                <div>{feedback.review}</div>
              </div>
            ))
          ) : (
            <div className="flex items-center justify-center">
              No feedback available.
            </div>
          )}
        </div>
      </div>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-1 gap-4 py-4 px-6">
            <Textarea form={form} name="review" placeholder="Enter Review" />
            <Input form={form} name="rating" placeholder="Enter Ratings" />
          </div>
          <div className="flex w-full justify-end mt-5 space-x-3">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button loading={loading} type="submit">
              Save Feedback
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
