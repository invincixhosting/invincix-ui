"use client";

import { Button, Dialog, Tooltip } from "@ui/components/common";
import Rating from "@ui/components/common/rating";
import { DataTable } from "@ui/components/data-table";
import { DialogClose } from "@ui/components/ui/dialog";
import { ClipboardList, MoveUpRight } from "lucide-react";
import { useRouter } from "next/navigation";
import * as PATH from "@/const/paths";
import { MechanicProfile } from "./mechanic-profile";

export default function Teams({
  data,
  currentUser,
}: {
  data: any;
  currentUser: any;
}) {
  const router = useRouter();
  const columns = [
    {
      field: "serial_no",
      title: "Serial No",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "name",
      title: "Name",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "pending_count",
      title: "Pending Task",
      sort: true,
    },
    {
      field: "start_count",
      title: "Start Task",
      sort: true,
    },
    {
      field: "pause_count",
      title: "Pause Task",
      sort: true,
    },
    {
      field: "complete_count",
      title: "Complete Task",
      sort: true,
    },
    {
      field: "taskCount",
      title: "Total Task",
      sort: true,
    },
    {
      field: "average_rating",
      title: "Average Rating",
      headerStyle: "text-center",
      renderCell: ({ row }) => {
        const averageRating = row.original.average_rating;
        return (
          <div className="flex justify-center items-center">
            <Rating score={averageRating} />
          </div>
        );
      },
    },
    {
      field: "actions",
      title: "Actions",
      headerStyle: "text-right",
      renderCell: ({ row }) => {
        return (
          <div className="flex justify-end items-center space-x-3">
            <Button
              className="p-0"
              variant="ghost"
              onClick={() => {
                router.push(
                  PATH.SUPERVISOR_SPARE_TEAMS_TASK_LIST.replace(
                    ":mechanicId",
                    row.original.id
                  )
                );
              }}
            >
              <Tooltip title="Task List">
                <ClipboardList className="text-muted" />
              </Tooltip>
            </Button>
            <Dialog
              title="Mechanic profile"
              content={<MechanicProfile mechanicDetails={row.original} />}
              description={``}
            >
              <Tooltip title="View Profile">
                <div className="rounded-full bg-primary p-2">
                  <MoveUpRight className="text-primary-foreground w-4 h-4" />
                </div>
              </Tooltip>
            </Dialog>
          </div>
        );
      },
    },
  ];

  return (
    <div>
      <DataTable columns={columns} data={data} />
    </div>
  );
}
