"use client";

import {
  ImageUpload,
  Input,
  Select,
  Switch,
  Textarea,
} from "@ui/components/form";
import { Form } from "@ui/components/ui/form";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { DialogClose } from "@ui/components/ui/dialog";
import { Button } from "@ui/components/common";
import { productUOM } from "@/services/common/requests";
import { createProduct, updateProduct } from "@/services/product/requests";
import { first } from "lodash";

const formSchema = z.object({
  name: z.string().min(1, "This field is required").max(50),
  description: z.string().min(1, "This field is required").max(256),
  category: z.string().min(1, "This field is required").max(256),
  product_code: z.string().min(1, "This field is required").max(256),
  min_quantity: z.string().min(1, "This field is required").max(256),
  max_quantity: z.string().min(1, "This field is required").max(256),
  uom: z.string().min(1, "This field is required").max(256),
  image: z.any(),
  is_active: z.boolean(),
});

export default function ProductForm({
  type,
  currentUser,
  initData,
  activeProductCategoryList,
}: {
  type: "add" | "edit";
  currentUser: any;
  initData?: any;
  activeProductCategoryList: any;
}) {
  const [loading, setLoading] = useState<boolean>(false);

  const { toast } = useToast();
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: type == "edit" ? initData?.name : "",
      description: type == "edit" ? initData?.description : "",
      category: type == "edit" ? initData?.productCategory.id : "",
      product_code: type == "edit" ? initData?.product_code : "",
      min_quantity: type == "edit" ? initData?.min_quantity : "",
      max_quantity: type == "edit" ? initData?.max_quantity : "",
      uom: type == "edit" ? initData?.uom : "",
      image: type == "edit" ? initData?.image_path : "",
      is_active: type == "edit" ? initData?.is_active : true,
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      setLoading(true);
      const data = {
        name: values.name,
        description: values.description,
        product_category_id: values.category,
        product_code: values.product_code,
        min_quantity: Number(values.min_quantity),
        max_quantity: Number(values.max_quantity),
        uom: values.uom,
        image_path: first(values.image),
      };

      let res;
      if (type == "add") {
        res = await createProduct({
          ...data,
          slug: "p_1",
          partner_id: currentUser.partner_id,
          created_by: currentUser.id,
          ProductInventory: [],
        });
      } else {
        res = await updateProduct({
          ...data,
          slug: initData?.slug || "p_1",
          product_id: initData?.id,
          partner_id: currentUser.partner_id,
          updated_by: currentUser.id,
          IsActive: values.is_active,
          ProductInventory: [],
        });
      }

      setLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "Please try again later!",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully ${type == "add" ? "added" : "edited"} product!`,
          description: "",
        });
      }
    } catch (error: any) {
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  }

  return (
    <div className="pt-8 px-8">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-2 gap-4 bg-card p-8 rounded-lg">
            <ImageUpload
              className="col-span-2"
              form={form}
              label="Image"
              name="image"
              fileList={[
                type == "edit"
                  ? `${
                      process.env.NEXT_PUBLIC_API_ENDPOINT
                    }/${initData?.image_path?.replace("uploads/", "")}`
                  : "",
              ]}
            />
            <Input form={form} name="product_code" placeholder="Code" />
            <Input form={form} name="name" placeholder="Name" />
            <Textarea
              className="col-span-2"
              form={form}
              name="description"
              placeholder="Description"
            />
            <Select
              form={form}
              name="category"
              placeholder="Category"
              options={activeProductCategoryList?.map((item) => ({
                label: item.name,
                value: item.id,
              }))}
            />
            <Select
              form={form}
              name="uom"
              placeholder="UOM"
              options={productUOM?.map((item) => ({
                label: item,
                value: item,
              }))}
            />
            <Input form={form} name="min_quantity" placeholder="Min Quantity" />
            <Input form={form} name="max_quantity" placeholder="Max Quantity" />
            {type == "edit" && (
              <Switch name="is_active" label="Status" form={form} />
            )}
          </div>
          <div className="flex items-center justify-end space-x-3 mt-8">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button type="submit" loading={loading}>
              Submit
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
