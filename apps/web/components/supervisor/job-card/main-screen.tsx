"use-client";

import ImageViewer from "@/components/image-viewer";
import { JobCardDetailT } from "@/services/job-card/dto";
import { Button } from "@ui/components/common";
import {
  ClipboardCheck,
  Frown,
  PocketKnife,
  Receipt,
  Wrench,
} from "lucide-react";
import { ScreenEnum } from "./vehicle-detail";

const ActionCard = ({
  icon,
  name,
  showCompleted,
  onClick,
  data,
}: {
  icon: React.ReactNode;
  name: string;
  showCompleted?: boolean;
  onClick: () => void;
  data?: any;
}) => {
  return (
    <Button className="h-20 py-20" variant="secondary" onClick={onClick}>
      <span className="flex flex-col items-center justify-center">
        <span>{icon}</span>
        <h2 className="mt-3">{name}</h2>
        {showCompleted && (
          <h2>
            COMPLETED{" "}
            {data?.filter((item) => item.task_status === "Completed")?.length}/
            {data?.length}
          </h2>
        )}
      </span>
    </Button>
  );
};

export default function MainScreen({
  details,
  mechanicList,
  setScreen,
}: {
  details: JobCardDetailT;
  setScreen: (screen: ScreenEnum) => void;
  mechanicList: any;
}) {
  const actionItems = [
    {
      name: "SERVICES",
      data: details?.jobcard?.services || [],
      showCompleted: true,
      icon: <Wrench className="w-12 h12" />,
      onClick: () => {
        setScreen(ScreenEnum.SERVICE);
      },
    },
    {
      name: "CUSTOMER COMPLAINTS",
      data: details?.jobcard?.complaints || [],
      showCompleted: true,
      icon: <ClipboardCheck className="w-12 h12" />,
      onClick: () => {
        setScreen(ScreenEnum.COMPLAINTS);
      },
    },
    {
      name: "SPARE PARTS",
      data: [],
      showCompleted: false,
      icon: <PocketKnife className="w-12 h12" />,
      onClick: () => {
        setScreen(ScreenEnum.SPARE_PART);
      },
    },
    {
      // TODO: Add invoice to status 'ready' || 'delivered
      name:
        details?.jobcard?.status == "Ready" ||
        details?.jobcard?.status == "Delivered"
          ? "ESTIMATE"
          : "ESTIMATE",
      data: [],
      showCompleted: false,
      icon: <Receipt className="w-12 h12" />,
      onClick: () => {
        setScreen(
          details?.jobcard?.status == "Ready" ||
            details?.jobcard?.status == "Delivered"
            ? ScreenEnum.INVOICE
            : ScreenEnum.ESTIMATE
        );
      },
    },
  ];

  const images = (details?.jobcard?.image_paths || []).map((path) => ({
    src: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/${path?.replace(
      "uploads/",
      ""
    )}`,
    width: 320,
    height: 174,
    caption: path,
  }));

  const assignedMechanicIds = details?.jobcard?.services?.map(
    (service) => service.assigned_to
  );
  const uniqueMechanicIds = assignedMechanicIds?.filter(
    (value, index) => assignedMechanicIds.indexOf(value) === index
  );

  return (
    <>
      <div className="grid grid-cols-4 gap-4">
        {actionItems.map((item) => (
          <ActionCard
            key={item.name}
            name={item.name}
            data={item.data}
            showCompleted={item.showCompleted}
            icon={item.icon}
            onClick={item.onClick}
          />
        ))}
      </div>
      <div className="mt-5">
        <h1 className="text-xl mb-3">Mechanics</h1>
        {details?.jobcard?.services?.length ? (
          <div className="grid grid-cols-3 gap-4">
            {uniqueMechanicIds?.map((id) => {
              const mechanic = mechanicList.find(
                (mechanic) => mechanic.id == id
              );
              return (
                <div key={mechanic?.id} className="bg-secondary rounded-lg p-4">
                  {mechanic?.name || ""}
                </div>
              );
            })}
          </div>
        ) : (
          <div className="flex items-center justify-center flex-col space-y-3 text-muted min-h-[150px]">
            <Frown className="w-10 h-10" />
            <div className="text-xl">No items</div>
          </div>
        )}
      </div>
      <div className="mt-5">
        <h1 className="text-xl mb-3">Vehicle Images</h1>
        {details?.jobcard?.image_paths?.length ? (
          <ImageViewer images={images} />
        ) : (
          <div className="flex items-center justify-center flex-col space-y-3 text-muted min-h-[150px]">
            <Frown className="w-10 h-10" />
            <div className="text-xl">No items</div>
          </div>
        )}
      </div>
    </>
  );
}
