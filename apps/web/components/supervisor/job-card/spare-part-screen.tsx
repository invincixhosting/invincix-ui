"use-client";

import { JobCardDetailT } from "@/services/job-card/dto";
import { Avatar, Button, Dialog, Tooltip } from "@ui/components/common";
import { ArrowLeft, Plus, Repeat, Trash2 } from "lucide-react";
import { ScreenEnum } from "./vehicle-detail";
import { DialogClose } from "@ui/components/ui/dialog";
import { useToast } from "@ui/components/ui/use-toast";
import { useState } from "react";
import { deleteSparePartById } from "@/services/job-card/requests";
import AddSpare from "./add-spare";

export default function SparePartScreen({
  details,
  setScreen,
  currentUser,
  groupedProducts,
}: {
  details: JobCardDetailT;
  setScreen: (screen: ScreenEnum) => void;
  currentUser: any;
  groupedProducts: any;
}) {
  const { toast } = useToast();
  const [deleteLoading, setDeleteLoading] = useState(false);

  const handleDeleteSpare = async (spare) => {
    try {
      setDeleteLoading(true);

      const res = await deleteSparePartById(spare.id);
      setDeleteLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully deleted spare part!`,
          description: "",
        });
      }
    } catch (error: any) {
      console.log(
        "🚀 ~ file: spare-part-request.tsx:62 ~ handleReject ~ error:",
        error
      );
      setDeleteLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: JSON.stringify(error),
      });
    }
  };

  return (
    <div>
      <div className="flex items-center justify-between mb-5">
        <h2 className="text-xl font-bold text-primary">Spare parts</h2>
        <div className="flex items-center space-x-3">
          {(details?.jobcard?.status == "In-Progress" ||
            details?.jobcard?.status == "Pending") && (
            <Dialog
              isTriggerBtn
              title={`Add service`}
              content={
                <AddSpare
                  jobCardId={details?.jobcard?.id}
                  groupedProducts={groupedProducts}
                  user={currentUser}
                />
              }
              description=""
              footer={<></>}
            >
              <Button
                className="flex items-center space-x-3"
                onClick={() => {}}
              >
                <Plus className="w-4 h-4" /> <span>Add Spare parts</span>
              </Button>
            </Dialog>
          )}
          <Button
            className="flex items-center space-x-3"
            variant="secondary"
            onClick={() => setScreen(ScreenEnum.MAIN)}
          >
            <ArrowLeft className="w-4 h-4" /> <span>Back</span>
          </Button>
        </div>
      </div>
      <div className="grid grid-cols-4 gap-5">
        {(details?.jobcard?.spare || [])?.map((spare) => (
          <div
            key={spare?.id}
            className="bg-secondary rounded-lg p-6 flex flex-col items-center justify-center"
          >
            <Avatar
              imgSrc={`${
                process.env.NEXT_PUBLIC_API_ENDPOINT
              }/${spare?.product_image?.replace("uploads/", "")}`}
              imgAlt="service image"
              fallback="V"
            />
            <h2 className="text-primary font-bold mt-2">
              {spare?.product_name}
            </h2>
            <p>{spare?.quantity} Unit</p>
            {(details?.jobcard?.status == "In-Progress" ||
              details?.jobcard?.status == "Pending") && (
              <div className="flex items-center justify-center space-x-4 mt-4">
                <Dialog
                  title="Delete Spare part"
                  content={
                    <div className="px-6 my-5 text-center">
                      <h3 className="mb-2">
                        Are you sure you want to delete this spare part?
                      </h3>
                      <p className="text-primary font-bold text-2xl">{`${spare.product_name}`}</p>
                    </div>
                  }
                  description=""
                  footer={
                    <div className="flex items-center justify-end space-x-3">
                      <DialogClose>
                        <Button variant="outline">Cancel</Button>
                      </DialogClose>
                      <Button
                        loading={deleteLoading}
                        onClick={() => handleDeleteSpare(spare)}
                      >
                        Delete
                      </Button>
                    </div>
                  }
                >
                  <Tooltip title="Delete">
                    <Trash2 className="w-5 h-5 text-destructive" />
                  </Tooltip>
                </Dialog>
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
}
