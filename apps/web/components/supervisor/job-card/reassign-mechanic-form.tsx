"use client";

import { Button } from "@ui/components/common";
import { useForm } from "react-hook-form";
import { Form } from "@ui/components/ui/form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { Select, Textarea } from "@ui/components/form";
import { DialogClose } from "@ui/components/ui/dialog";
import { updatePaymentRequest } from "@/services/payment/requests";
import { mechanicReAssignTask } from "@/services/job-card/requests";

const formSchema = z.object({
  mechanic: z.string().min(1, "This field is required"),
});

export function ReassignMechanicForm({
  currentUser,
  mechanicId,
  serviceId,
  mechanicList,
}: {
  currentUser: any;
  serviceId: string;
  mechanicId: string;
  mechanicList: any;
}) {
  const { toast } = useToast();
  const [loading, setLoading] = useState(false);
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      mechanic: mechanicId || "",
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      setLoading(true);

      const res = await mechanicReAssignTask({
        id: serviceId,
        assigned_to: values.mechanic,
        updated_by: currentUser.id,
      });
      setLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "",
        });
      } else {
        toast({
          variant: "success",
          title: "Successfully reassigned mechanic!",
          description: "",
        });
      }
    } catch (error: any) {
      console.log("🚀 ~ file: select-bay.tsx:64 ~ onSubmit ~ error:", error);
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: JSON.stringify(error),
      });
    }
  }

  return (
    <div className="p-6">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-1 gap-4 py-4 px-6">
            <Select
              form={form}
              name="mechanic"
              placeholder="Select mechanic"
              options={(mechanicList || [])?.map((item) => ({
                label: item.name,
                value: item.id,
              }))}
            />
          </div>
          <div className="flex w-full justify-end mt-5 space-x-3">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button loading={loading} type="submit">
              Confirm
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
