"use-client";

import { JobCardDetailT } from "@/services/job-card/dto";
import { Avatar, Button, Tooltip } from "@ui/components/common";
import { ArrowLeft, ClipboardList, Plus, Repeat, Trash2 } from "lucide-react";
import { ScreenEnum } from "./vehicle-detail";

export default function ComplaintScreen({
  details,
  setScreen,
}: {
  details: JobCardDetailT;
  setScreen: (screen: ScreenEnum) => void;
}) {
  const complaintStatus = {
    Pending: "Pending",
    Start: "In Progress",
    StartAgain: "In Progress",
    Paused: "Paused",
    Completed: "Completed",
  };

  return (
    <div>
      <div className="flex items-center justify-between mb-5">
        <h2 className="text-xl font-bold text-primary">Complaints</h2>
        <div className="flex items-center space-x-3">
          <Button
            className="flex items-center space-x-3"
            variant="secondary"
            onClick={() => setScreen(ScreenEnum.MAIN)}
          >
            <ArrowLeft className="w-4 h-4" /> <span>Back</span>
          </Button>
        </div>
      </div>
      <div className="grid grid-cols-2 gap-5">
        {(details?.jobcard?.complaints || [])?.map((complaint) => (
          <Tooltip
            key={complaint?.id}
            title={complaintStatus[complaint?.task_status]}
          >
            <div className="bg-secondary rounded-lg p-6 flex items-center space-x-3">
              <ClipboardList className="w-8 h-8" />
              <h2 className="font-bold">{complaint?.complaints}</h2>
            </div>
          </Tooltip>
        ))}
      </div>
    </div>
  );
}
