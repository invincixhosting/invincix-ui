"use-client";

import { JobCardDetailT } from "@/services/job-card/dto";
import { Avatar, Button, Dialog, Tooltip } from "@ui/components/common";
import { ArrowLeft, Plus, Repeat, Trash2 } from "lucide-react";
import { ScreenEnum } from "./vehicle-detail";
import { ReassignMechanicForm } from "./reassign-mechanic-form";
import { DialogClose } from "@ui/components/ui/dialog";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { deleteJobcardServiceById } from "@/services/job-card/requests";
import AddService from "./add-service";

export default function ServiceScreen({
  details,
  setScreen,
  mechanicList,
  currentUser,
  serviceList,
}: {
  details: JobCardDetailT;
  setScreen: (screen: ScreenEnum) => void;
  mechanicList: any;
  currentUser: any;
  serviceList: any;
}) {
  const { toast } = useToast();
  const [deleteLoading, setDeleteLoading] = useState(false);
  const serviceStatus = {
    Pending: "Pending",
    Start: "In Progress",
    StartAgain: "In Progress",
    Paused: "Paused",
    Completed: "Completed",
  };

  const handleDeleteService = async (service) => {
    try {
      setDeleteLoading(true);

      const res = await deleteJobcardServiceById(service.id);
      setDeleteLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully deleted service!`,
          description: "",
        });
      }
    } catch (error: any) {
      console.log(
        "🚀 ~ file: spare-part-request.tsx:62 ~ handleReject ~ error:",
        error
      );
      setDeleteLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: JSON.stringify(error),
      });
    }
  };

  return (
    <div>
      <div className="flex items-center justify-between mb-5">
        <h2 className="text-xl font-bold text-primary">Services</h2>
        <div className="flex items-center space-x-3">
          {(details?.jobcard?.status == "In-Progress" ||
            details?.jobcard?.status == "Pending") && (
            <Dialog
              isTriggerBtn
              title={`Add service`}
              content={
                <AddService
                  jobCardId={details?.jobcard?.id}
                  mechanicList={mechanicList}
                  user={currentUser}
                  serviceList={serviceList}
                />
              }
              description=""
              footer={<></>}
            >
              <Button className="flex items-center space-x-3">
                <Plus className="w-4 h-4" /> <span>Add Service</span>
              </Button>
            </Dialog>
          )}
          <Button
            className="flex items-center space-x-3"
            variant="secondary"
            onClick={() => setScreen(ScreenEnum.MAIN)}
          >
            <ArrowLeft className="w-4 h-4" /> <span>Back</span>
          </Button>
        </div>
      </div>
      <div className="grid grid-cols-4 gap-5">
        {(details?.jobcard?.services || [])?.map((service) => (
          <Tooltip
            key={service?.id}
            title={serviceStatus[service?.task_status]}
          >
            <div className="bg-secondary rounded-lg p-6 flex flex-col items-center justify-center">
              <Avatar
                imgSrc={`${
                  process.env.NEXT_PUBLIC_API_ENDPOINT
                }/${service?.service_image?.replace("uploads/", "")}`}
                imgAlt="service image"
                fallback="V"
              />
              <h2 className="text-primary font-bold mt-2">
                {service?.service_name}
              </h2>
              <p>₹{service?.service_price}/-</p>
              {(details?.jobcard?.status == "In-Progress" ||
                details?.jobcard?.status == "Pending") && (
                <div className="flex items-center justify-center space-x-4 mt-4">
                  <Dialog
                    title={`Reassign Mechanic for ${service.service_name}`}
                    content={
                      <ReassignMechanicForm
                        currentUser={currentUser}
                        serviceId={service.id}
                        mechanicId={service.assigned_to}
                        mechanicList={mechanicList}
                      />
                    }
                    description=""
                    footer={<></>}
                  >
                    <Tooltip title="Reassign">
                      <Repeat className="w-5 h-5" />
                    </Tooltip>
                  </Dialog>
                  <Dialog
                    title="Delete Service"
                    content={
                      <div className="px-6 my-5 text-center">
                        <h3 className="mb-2">
                          Are you sure you want to delete this service?
                        </h3>
                        <p className="text-primary font-bold text-2xl">{`${service.service_name}`}</p>
                      </div>
                    }
                    description=""
                    footer={
                      <div className="flex items-center justify-end space-x-3">
                        <DialogClose>
                          <Button variant="outline">Cancel</Button>
                        </DialogClose>
                        <Button
                          loading={deleteLoading}
                          onClick={() => handleDeleteService(service)}
                        >
                          Delete
                        </Button>
                      </div>
                    }
                  >
                    <Tooltip title="Delete">
                      <Trash2 className="w-5 h-5 text-destructive" />
                    </Tooltip>
                  </Dialog>
                </div>
              )}
            </div>
          </Tooltip>
        ))}
      </div>
    </div>
  );
}
