"use client";

import { Avatar, Button, DropdownMenu } from "@ui/components/common";
import { DropdownMenuSeparator } from "@ui/components/ui/dropdown-menu";
import { ChevronDown, Frown, Minus, Plus, X } from "lucide-react";
import { Card, CardContent, CardFooter } from "@ui/components/ui/card";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { useParams, useRouter } from "next/navigation";
import { createJobCardServices } from "@/services/job-card/requests";
import * as PATH from "@/const/paths";

const ItemCard = ({
  data,
  handleAddItem,
}: {
  data: any;
  handleAddItem: (item: any) => void;
}) => {
  return (
    <Card className="border-transparent min-w-[200px]">
      <CardContent className="flex flex-col items-center justify-center space-y-3 mt-5">
        <Avatar
          imgSrc={`${
            process.env.NEXT_PUBLIC_API_ENDPOINT
          }/${data.image_path.replace("uploads/", "")}`}
          imgAlt="Wheel"
          fallback="V"
        />
        <h1 className="text-xl text-primary">{data.name}</h1>
        <p>Service Cost: ₹{data?.price}/-</p>
        <p>ETD: {data?.durationInMinute} min</p>
      </CardContent>
      <CardFooter className="flex justify-center items-center">
        <Button variant="secondary" onClick={() => handleAddItem(data)}>
          ADD
        </Button>
      </CardFooter>
    </Card>
  );
};

const SelectedItem = ({
  data,
  handleRemoveItem,
  mechanicList,
  handleChangeItemMechanic,
}: {
  data: any;
  handleRemoveItem: (item: any) => void;
  mechanicList: any;
  handleChangeItemMechanic: (item: any, mechanic: any) => void;
}) => {
  const [selectedMechanic, setSelectedMechanic] = useState<any>();

  return (
    <Card className="border-transparent min-w-[200px]">
      <CardContent className="flex flex-col items-center justify-center my-5 space-y-3 pb-0">
        <div className="flex flex-col items-center">
          <Avatar
            imgSrc={`${
              process.env.NEXT_PUBLIC_API_ENDPOINT
            }/${data.image_path.replace("uploads/", "")}`}
            imgAlt="Wheel"
            fallback="V"
          />
          <p className="text-center text-sm text-bold mt-2">{data.name}</p>
          <p className="text-sm text-center">₹{data?.price}/-</p>
        </div>
        <div className="my-2">
          <DropdownMenu
            title={
              <Button variant="secondary" className="flex space-x-3">
                <span>
                  {selectedMechanic
                    ? selectedMechanic?.name
                    : "Select Mechanic"}
                </span>
                <ChevronDown />
              </Button>
            }
            menuContent={[
              {
                type: "label",
                content: "Select Mechanic",
              },
              { type: "separator" },
              ...mechanicList?.map((mechanic) => ({
                type: "item",
                content: mechanic?.name,
                onClick: () => {
                  setSelectedMechanic(mechanic);
                  handleChangeItemMechanic(data, mechanic);
                },
              })),
            ]}
          />
        </div>
        <div className="flex space-x-2 items-center">
          <Button
            variant="destructive"
            className="rounded-full p-2 h-8"
            onClick={() => handleRemoveItem(data)}
          >
            <X className="w-4 h-4" />
          </Button>
        </div>
      </CardContent>
    </Card>
  );
};

const EmptyCart = () => {
  return (
    <div className="flex flex-col items-center justify-center text-muted min-h-[350px] max-h-[350px] min-w-[250px]">
      <Frown className="mb-4 w-8 h-8 text-muted" />
      No items selected
    </div>
  );
};

export default function AddService({
  serviceList,
  user,
  mechanicList,
  jobCardId,
}: {
  serviceList: any;
  user: any;
  mechanicList: any;
  jobCardId: string;
}) {
  const { toast } = useToast();
  const router = useRouter();

  const [loading, setLoading] = useState<boolean>(false);
  const [cartItems, setCartItems] = useState<any>([]);

  const handleAddItem = (item: any) => {
    const existedItem = cartItems.find((cartItem) => cartItem.id == item.id);
    if (existedItem) {
      toast({
        variant: "error",
        title: "Item is already added!",
        description:
          "This item has already been added, please select other service",
      });
    } else {
      setCartItems([...cartItems, { ...item }]);
    }
  };

  const handleChangeItemMechanic = (item: any, mechanic: any) => {
    const existedItem = cartItems.find((cartItem) => cartItem.id == item.id);
    if (existedItem) {
      let newCartArr = [...cartItems];
      let existedIndex = newCartArr.indexOf(existedItem);
      newCartArr[existedIndex]["currentMechanic"] = mechanic?.id;
      setCartItems(newCartArr);
    }
  };

  const handleRemoveItem = (item: any) => {
    const existedItem = cartItems.find((cartItem) => cartItem.id == item.id);
    if (existedItem) {
      let newCartArr = [...cartItems];
      let existedIndex = newCartArr.indexOf(existedItem);
      newCartArr.splice(existedIndex, 1);
      setCartItems(newCartArr);
    }
  };

  const handleAddService = async () => {
    if (cartItems.length) {
      try {
        setLoading(true);
        const data = cartItems.map((item) => {
          return {
            partner_id: user.partner_id,
            jobCard_id: jobCardId,
            service_id: item.id,
            is_estimated: "0",
            assigned_to: item.currentMechanic,
            created_by: user?.id,
          };
        });

        const res = await createJobCardServices({ jobcardService: data });
        setLoading(false);

        if (res?.statusCode >= 300) {
          toast({
            variant: "error",
            title: "Error occurred!",
            description: "",
          });
        } else {
          toast({
            variant: "success",
            title: `Successfully added service!`,
            description: "",
          });
          setCartItems([]);
          router.push(PATH.SUPERVISOR_JOB_CARD);
        }
      } catch (error) {
        console.log(
          "🚀 ~ file: request-spare-parts.tsx:199 ~ handleAddService ~ error:",
          error
        );
        toast({
          variant: "error",
          title: "Error occurred!",
          description: JSON.stringify(error),
        });
      }
    }
  };

  return (
    <div className="flex min-h-[500px] mx-6">
      <div className="mr-4 flex flex-col min-h-full justify-between border-r-1 border-secondary">
        <div>
          <h1 className="text-xl text-bold border-b-1 border-secondary">
            Selected items{" "}
            {cartItems?.length > 0 && <span>({cartItems?.length})</span>}
          </h1>
          <div className="bg-secondary rounded-lg my-2 min-h-[400px] max-h-[400px] overflow-scroll">
            {cartItems.length ? (
              <div className="flex-col items-center justify-center space-y-3 p-4">
                {cartItems.map((item) => (
                  <SelectedItem
                    key={item?.name}
                    handleChangeItemMechanic={handleChangeItemMechanic}
                    mechanicList={mechanicList}
                    data={item}
                    handleRemoveItem={handleRemoveItem}
                  />
                ))}
              </div>
            ) : (
              <EmptyCart />
            )}
          </div>
        </div>
        <div className="flex items-center justify-center w-full">
          <Button
            loading={loading}
            className="w-full"
            size="lg"
            disabled={!cartItems.length}
            onClick={handleAddService}
          >
            ADD SERVICE
          </Button>
        </div>
      </div>
      <div className="rounded-lg bg-secondary px-8 py-4 w-full max-h-[500px] overflow-y-scroll">
        <h1 className="text-xl text-primary">Services</h1>
        {serviceList?.map((category) => (
          <div key={category?.categoryName} className="mt-8">
            <h2 className="text-xl">{category?.categoryName}</h2>
            <DropdownMenuSeparator className="my-3" />
            <div className="flex space-x-3 overflow-x-scroll">
              {category.services?.map((item) => (
                <ItemCard
                  key={item?.name}
                  handleAddItem={handleAddItem}
                  data={item}
                />
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
