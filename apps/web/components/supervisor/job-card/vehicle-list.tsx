"use client";

import { ArchiveJobCardT } from "@/services/job-card/dto";
import { Button } from "@ui/components/common";
import { Input } from "@ui/components/ui/input";
import {
  Tabs,
  TabsContent,
  TabsList,
  TabsTrigger,
} from "@ui/components/ui/tabs";
import { Frown, PlusCircle, Search } from "lucide-react";
import "./styles.css";
import { SUPERVISOR_VEHICLE_REGISTRATION } from "@/const/paths";
import { useRouter } from "next/navigation";

const VehicleCard = ({
  selected,
  vehicleDetails,
  onSelect,
}: {
  selected?: boolean;
  vehicleDetails: ArchiveJobCardT;
  onSelect: (details: any) => void;
}) => {
  return (
    <div
      className={`bg-secondary ${
        selected && "border-2 border-primary"
      } p-4 rounded-lg mb-3 cursor-pointer`}
      onClick={() => onSelect(vehicleDetails)}
    >
      <h2 className="text-xl font-bold text-primary mb-3">
        {vehicleDetails.vehicle.vehicle_name}
      </h2>
      <p>{vehicleDetails.vehicle.vehicle_number}</p>
      <p>{vehicleDetails.customer.customer_name}</p>
      <p>(M) {vehicleDetails.vehicle.model_number}</p>
    </div>
  );
};

export default function VehicleList({
  vehicleList,
  currentVehicle,
  setCurrentVehicle,
}: {
  vehicleList: ArchiveJobCardT[];
  currentVehicle: ArchiveJobCardT;
  setCurrentVehicle: (vehicle: any) => void;
}) {
  const router = useRouter();
  const pendingList = vehicleList.filter((item) => item.status == "Pending");
  const inProgressList = vehicleList.filter(
    (item) => item.status == "In-Progress"
  );
  const readyList = vehicleList.filter((item) => item.status == "Ready");
  const deliveredList = vehicleList.filter(
    (item) => item.status == "Delivered"
  );

  const tabs = [
    {
      value: "Pending",
      title: "Pending",
      list: pendingList,
    },
    {
      value: "In-Progress",
      title: "In Progress",
      list: inProgressList,
    },
    {
      value: "Ready",
      title: "Ready",
      list: readyList,
    },
    {
      value: "Delivered",
      title: "Delivered",
      list: deliveredList,
    },
  ];

  return (
    <>
      <Input
        startIcon={<Search className="w-4 h-4" />}
        placeholder="Search Vehicle No"
      />
      <Button
        className="flex items-center justify-center space-x-4 my-4 w-full"
        onClick={() => router.push(SUPERVISOR_VEHICLE_REGISTRATION)}
      >
        <PlusCircle className="w-4 h-4" />
        <span>Add Vehicle</span>
      </Button>
      <Tabs defaultValue={currentVehicle?.status || "Pending"}>
        <TabsList className="h-fit justify-start max-w-full overflow-x-scroll">
          {tabs.map((item) => (
            <TabsTrigger
              key={item?.value}
              className={`m-1 p-4 ${item.value == "Pending" && "ml-0"}`}
              value={item.value}
            >
              {item.title} ({item.list?.length})
            </TabsTrigger>
          ))}
        </TabsList>
        {tabs.map((tab) => (
          <TabsContent
            key={tab?.value}
            value={tab.value}
            className="mt-0 bg-card rounded-lg p-4 min-h-[calc(100vh-450px)] max-h-[calc(100vh-450px)] overflow-y-scroll"
          >
            {tab.list?.length ? (
              tab.list.map((vehicle) => (
                <VehicleCard
                  key={vehicle?.id}
                  selected={vehicle.id == currentVehicle.id}
                  vehicleDetails={vehicle}
                  onSelect={(details) => setCurrentVehicle(details)}
                />
              ))
            ) : (
              <div className="flex items-center justify-center flex-col space-y-3 text-muted min-h-[250px]">
                <Frown className="w-10 h-10" />
                <div className="text-xl">No items</div>
              </div>
            )}
          </TabsContent>
        ))}
      </Tabs>
    </>
  );
}
