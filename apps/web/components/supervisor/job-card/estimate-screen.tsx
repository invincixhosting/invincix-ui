"use client";

import { Button } from "@ui/components/common";
import { ArrowLeft } from "lucide-react";
import { ScreenEnum } from "./vehicle-detail";
import {
  Table,
  TableBody,
  TableCaption,
  TableCell,
  TableFooter,
  TableHead,
  TableHeader,
  TableRow,
} from "@ui/components/ui/table";
import { getEstimate } from "@/services/job-card/requests";
import { useState, useEffect } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { useSession} from 'next-auth/react'

export default function EstimateScreen({
  setScreen,
  jobCardId,
  // currentUser,
}: {
  setScreen: (screen: ScreenEnum) => void;
  jobCardId: string;
  // currentUser: any;
}) {
  const currentUser = useSession()?.data?.user
  const { toast } = useToast();
  const [estimateDetail, setEstimateDetail] = useState<any>();
  const [detailLoading, setDetailLoading] = useState(false);

  useEffect(() => {
    const fetchEstimateDetail = async () => {
      try {
        setDetailLoading(true);
        const res = await getEstimate(jobCardId);
        setDetailLoading(false);

        if (res?.statusCode >= 300) {
          toast({
            variant: "error",
            title: "Error occurred!",
            description: "",
          });
        } else {
          setEstimateDetail(res);
        }
      } catch (error) {
        console.log(
          "🚀 ~ file: job-card.tsx:19 ~ fetchVehicleDetail ~ error:",
          error
        );
      }
    };

    fetchEstimateDetail();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div className="flex items-center justify-between mb-5">
        <h2 className="text-xl font-bold text-primary">Estimate</h2>
        <div className="flex items-center space-x-3">
          <Button
            className="flex items-center space-x-3"
            variant="secondary"
            onClick={() => setScreen(ScreenEnum.MAIN)}
          >
            <ArrowLeft className="w-4 h-4" /> <span>Back</span>
          </Button>
        </div>
      </div>
      <Table>
        <TableHeader>
          <TableRow>
            <TableHead className="w-[300px]">Description</TableHead>
            <TableHead>Quantity</TableHead>
            <TableHead>Unit Price</TableHead>
            <TableHead className="text-right">Total Price</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody>
          {estimateDetail?.services.map((service) => (
            <TableRow key={service.service_name}>
              <TableCell className="font-medium">
                {service.service_name}
              </TableCell>
              <TableCell>-</TableCell>
              <TableCell>{service.price?.toFixed(2)}</TableCell>
              <TableCell className="text-right">
                {service.price?.toFixed(2)}
              </TableCell>
            </TableRow>
          ))}
          {estimateDetail?.spareParts.map((sparePart) => (
            <TableRow key={sparePart.spare_part_name}>
              <TableCell className="font-medium">
                {sparePart.spare_part_name}
              </TableCell>
              <TableCell>{sparePart.quantity}</TableCell>
              <TableCell>{sparePart.price?.toFixed(2)}</TableCell>
              <TableCell className="text-right">
                {sparePart.total_price?.toFixed(2)}
              </TableCell>
            </TableRow>
          ))}
          <TableRow>
            <TableCell className="font-medium">
              {estimateDetail?.extraService.service_name}
            </TableCell>
            <TableCell>1</TableCell>
            <TableCell>
              {estimateDetail?.extraService.price?.toFixed(2)}
            </TableCell>
            <TableCell className="text-right">
              {estimateDetail?.extraService.price?.toFixed(2)}
            </TableCell>
          </TableRow>

          <TableRow>
            <TableCell colSpan={3} className="font-medium">
              Sub Total
            </TableCell>
            <TableCell className="text-right">
              {estimateDetail?.totalPrice?.toFixed(2)}
            </TableCell>
          </TableRow>

          <TableRow>
            <TableCell colSpan={3}>
              Tax {currentUser?.tax_percentage}%
            </TableCell>
            <TableCell className="text-right">
              {(
                (Number(estimateDetail?.totalPrice) *
                  currentUser.tax_percentage) /
                100
              )?.toFixed(2)}
            </TableCell>
          </TableRow>
        </TableBody>
        <TableFooter>
          <TableRow>
            <TableCell colSpan={3}>Total</TableCell>
            <TableCell className="text-right">
              {Math.round(
                Math.floor(Number(estimateDetail?.totalPrice)) +
                  Math.floor(
                    (Number(estimateDetail?.totalPrice) *
                      currentUser.tax_percentage) /
                      100
                  )
              )?.toFixed(2)}
            </TableCell>
          </TableRow>
        </TableFooter>
      </Table>
    </>
  );
}
