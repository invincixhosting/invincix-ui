"use client";

import VehicleList from "./vehicle-list";
import VehicleDetail from "./vehicle-detail";
import { useEffect, useState } from "react";
import { first } from "lodash";
import { ArchiveJobCardT, JobCardDetailT } from "@/services/job-card/dto";
import Spin from "@/components/spin";
import { getJobCardDetails } from "@/services/job-card/requests";
import { useToast } from "@ui/components/ui/use-toast";
import { BayT } from "@/services/bay/dto";

export function JobCard({
  data,
  bayList,
  mechanicList,
  currentUser,
  serviceList,
  groupedProducts,
}: {
  data: ArchiveJobCardT[];
  bayList: BayT[];
  mechanicList: any;
  currentUser: any;
  serviceList: any;
  groupedProducts: any;
}) {
  const { toast } = useToast();
  const [selectedVehicle, setSelectedVehicle] = useState<ArchiveJobCardT>(
    first(data)
  );
  const [vehicleDetail, setVehicleDetail] = useState<JobCardDetailT>();
  const [detailLoading, setDetailLoading] = useState(false);
  const [needReloadDetail, setNeedReloadDetail] = useState(false);

  const fetchVehicleDetail = async (id: string) => {
    try {
      setDetailLoading(true);
      const res = await getJobCardDetails(id);
      setDetailLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "",
        });
      } else {
        setVehicleDetail(res);
      }
    } catch (error) {
      console.log(
        "🚀 ~ file: job-card.tsx:19 ~ fetchVehicleDetail ~ error:",
        error
      );
    }
  };

  useEffect(() => {
    fetchVehicleDetail(selectedVehicle?.id || "");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedVehicle]);

  useEffect(() => {
    if (needReloadDetail) {
      fetchVehicleDetail(selectedVehicle?.id || "");
      setNeedReloadDetail(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [needReloadDetail]);

  return (
    <div className="grid grid-cols-3 gap-4">
      <div className="mb-4 col-span-3 md:col-span-1">
        <VehicleList
          vehicleList={data}
          currentVehicle={selectedVehicle}
          setCurrentVehicle={setSelectedVehicle}
        />
      </div>
      <div className="col-span-2">
        {detailLoading ? (
          <Spin />
        ) : (
          <VehicleDetail
            groupedProducts={groupedProducts}
            serviceList={serviceList}
            setNeedReloadDetail={setNeedReloadDetail}
            currentUser={currentUser}
            mechanicList={mechanicList}
            details={vehicleDetail}
            bayList={bayList}
          />
        )}
      </div>
    </div>
  );
}
