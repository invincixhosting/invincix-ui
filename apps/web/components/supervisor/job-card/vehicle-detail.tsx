"use client";

import { JobCardDetailT } from "@/services/job-card/dto";
import { Button, DropdownMenu } from "@ui/components/common";
import Chip from "@ui/components/common/chip";
import Mail from "@ui/icons/Mail";
import { Car, ChevronDown, Gauge, Phone, User } from "lucide-react";
import moment from "moment";
import { first } from "lodash";
import { BayT } from "@/services/bay/dto";
import { useState } from "react";
import MainScreen from "./main-screen";
import ServiceScreen from "./service-screen";
import ComplaintScreen from "./complaint-screen";
import SparePartScreen from "./spare-part-screen";
import { useToast } from "@ui/components/ui/use-toast";
import { updateBayByMechanic } from "@/services/job-card/requests";
import EstimateScreen from "./estimate-screen";

export enum ScreenEnum {
  MAIN = "main",
  SERVICE = "service",
  COMPLAINTS = "complaints",
  SPARE_PART = "spare part",
  INVOICE = "invoice",
  ESTIMATE = "estimate",
}

export default function VehicleDetail({
  details,
  bayList,
  mechanicList,
  currentUser,
  setNeedReloadDetail,
  serviceList,
  groupedProducts,
}: {
  details: JobCardDetailT;
  bayList: BayT[];
  mechanicList: any;
  currentUser: any;
  serviceList: any;
  setNeedReloadDetail: (needReload: boolean) => void;
  groupedProducts: any;
}) {
  const { toast } = useToast();
  const [screen, setScreen] = useState<ScreenEnum>(ScreenEnum.MAIN);

  const handleSelectBay = async (bayId: string) => {
    try {
      const res = await updateBayByMechanic({
        jobcardTask: {
          jobcard_id: details.jobcard.id,
          bay: bayId,
        },
      });
      setNeedReloadDetail(true);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully updated bay!`,
          description: "",
        });
      }
    } catch (error: any) {
      console.log(
        "🚀 ~ file: spare-part-request.tsx:62 ~ handleReject ~ error:",
        error
      );
      toast({
        variant: "error",
        title: "Error occurred!",
        description: JSON.stringify(error),
      });
    }
  };

  return (
    <>
      <div className="rounded-lg bg-card p-4">
        <div className="flex items-center justify-between mb-3">
          <div className="flex items-center space-x-5">
            <span className="rounded-sm bg-secondary border border-primary p-2 font-bold text-xl">
              {details?.jobcard?.vehicle_number || ""}
            </span>
            <span className="flex items-center space-x-2">
              <Car className="w-4 h-4" />
              <p>{details?.jobcard?.model_number || ""}</p>
            </span>
            <span className="flex items-center space-x-2">
              <span
                className={`w-4 h-4 rounded-sm border-[0.5px] border-[yellow]`}
                style={{ backgroundColor: details?.jobcard?.color || "#fff" }}
              ></span>
              <p>{details?.jobcard?.color || ""}</p>
            </span>
            <span className="flex items-center space-x-2">
              <Gauge className="w-4 h-4" />
              <p>{details?.jobcard?.kilometer_driven || ""} KM</p>
            </span>
          </div>
          <div>
            <Chip
              title={details?.jobcard?.status}
              color={
                details?.jobcard?.status == "Pending"
                  ? "destructive"
                  : details?.jobcard?.status == "In-Progress"
                  ? "danger"
                  : "primary"
              }
            />
          </div>
        </div>
        <div className="flex items-center justify-between">
          <div>
            <p>
              <span className="text-muted">Started Date:</span>{" "}
              {details?.jobcard?.created_at
                ? moment(details?.jobcard?.created_at).format("MMMM Do YYYY")
                : "N/A"}
            </p>
            <p>
              <span className="text-muted">Estimated Date:</span>{" "}
              {first(details?.jobcard?.extraService)
                ? first(details?.jobcard?.extraService)?.estimated
                : "N/A"}
            </p>
          </div>
          {(details?.jobcard?.status == "Pending" ||
            details?.jobcard?.status == "In-Progress") && (
            <DropdownMenu
              title={
                <Button variant="outline" className="flex space-x-3">
                  <span>
                    {bayList?.find((bay) => bay?.id == details?.jobcard?.bay)
                      ?.name || "Select Bay"}
                  </span>
                  <ChevronDown />
                </Button>
              }
              menuContent={[
                {
                  type: "label",
                  content: "Select Bay",
                },
                { type: "separator" },
                ...bayList?.map((bay) => ({
                  type: "item",
                  content: bay?.name,
                  onClick: async () => await handleSelectBay(bay.id),
                })),
              ]}
            />
          )}
        </div>
      </div>
      <div className="rounded-lg bg-card p-4 flex items-center justify-between my-4">
        <span className="flex items-center space-x-2">
          <User className="w-4 h-4" />
          <p>
            <span className="text-muted">Customer Name:</span>{" "}
            {details?.jobcard?.customer_name || ""}
          </p>
        </span>
        <span className="flex items-center space-x-2">
          <Phone className="w-4 h-4" />
          <p>{details?.jobcard?.phone_number || ""}</p>
        </span>
        <span className="flex items-center space-x-2">
          <Mail className="w-4 h-4" />
          <p>{details?.jobcard?.email || ""}</p>
        </span>
      </div>
      <div className="rounded-lg bg-card p-4 min-h-[calc(100vh-450px)] max-h-[calc(100vh-450px)] overflow-y-scroll">
        {screen == ScreenEnum.MAIN ? (
          <MainScreen
            mechanicList={mechanicList}
            details={details}
            setScreen={setScreen}
          />
        ) : screen == ScreenEnum.SERVICE ? (
          <ServiceScreen
            serviceList={serviceList}
            currentUser={currentUser}
            mechanicList={mechanicList}
            details={details}
            setScreen={setScreen}
          />
        ) : screen == ScreenEnum.COMPLAINTS ? (
          <ComplaintScreen details={details} setScreen={setScreen} />
        ) : screen == ScreenEnum.SPARE_PART ? (
          <SparePartScreen
            currentUser={currentUser}
            groupedProducts={groupedProducts}
            details={details}
            setScreen={setScreen}
          />
        ) : screen == ScreenEnum.ESTIMATE ? (
          <EstimateScreen
            // currentUser={currentUser}
            jobCardId={details?.jobcard?.id}
            setScreen={setScreen}
          />
        ) : screen == ScreenEnum.INVOICE ? (
          <EstimateScreen
            // currentUser={currentUser}
            jobCardId={details?.jobcard?.id}
            setScreen={setScreen}
          />
        ) : (
          <></>
        )}
      </div>
    </>
  );
}
