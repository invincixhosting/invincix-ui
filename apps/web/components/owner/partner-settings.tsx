"use client";

import { Dialog, Tooltip } from "@ui/components/common";
import Chip from "@ui/components/common/chip";
import { DataTable } from "@ui/components/data-table";
import { PenLine, Plus } from "lucide-react";
import PartnerForm from "./partner-form";

export default function PartnerSettings({
  data,
  currentUser,
  currencyList,
  countryList,
}: {
  data: any;
  currentUser: any;
  currencyList: any;
  countryList: any;
}) {
  const columns = [
    {
      field: "name",
      title: "Name",
      sort: true,
      cellStyle: "text-primary font-bold",
    },
    {
      field: "email",
      title: "Email",
      sort: true,
    },
    {
      field: "phone_number",
      title: "Phone No",
      sort: true,
    },
    {
      field: "garage_name",
      title: "Garage Name",
      sort: true,
    },
    {
      field: "is_active",
      title: "Status",
      headerStyle: "text-center",
      renderCell: ({ row }) => {
        const isActive = row.original.is_active;
        return (
          <div className="flex justify-center items-center">
            <Chip
              title={isActive ? "Active" : "Inactive"}
              color={isActive ? "primary" : "destructive"}
            />
          </div>
        );
      },
    },
    {
      field: "actions",
      title: "Actions",
      headerStyle: "text-right",
      renderCell: ({ row }) => {
        return (
          <div className="flex justify-end items-center">
            <Dialog
              title="Edit partner"
              content={
                <PartnerForm
                  currencyList={currencyList}
                  countryList={countryList}
                  type="edit"
                  currentUser={currentUser}
                />
              }
              description="Edit partner information"
              footer={<></>}
            >
              <Tooltip title="Edit partner">
                <div className="rounded-full bg-primary p-2">
                  <PenLine className="text-primary-foreground w-4 h-4" />
                </div>
              </Tooltip>
            </Dialog>
          </div>
        );
      },
    },
  ];

  return (
    <div>
      <div className="flex justify-start items-center w-full mb-5 space-x-5">
        <h1 className="text-2xl">Partners</h1>
        <Dialog
          title="Add Partner"
          content={
            <PartnerForm
              currencyList={currencyList}
              countryList={countryList}
              type="add"
              currentUser={currentUser}
            />
          }
          description="Add a partner to your garage"
          footer={<></>}
        >
          <Tooltip title="Add Partner">
            <div className="rounded-full bg-primary p-2">
              <Plus className="text-primary-foreground w-6 h-6" />
            </div>
          </Tooltip>
        </Dialog>
      </div>
      <DataTable columns={columns} data={data} />
    </div>
  );
}
