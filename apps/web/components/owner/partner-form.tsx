"use client";

import {
  Checkbox,
  ImageUpload,
  Input,
  PasswordInput,
  Select,
  Switch,
} from "@ui/components/form";
import { Form } from "@ui/components/ui/form";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { DialogClose } from "@ui/components/ui/dialog";
import { Button } from "@ui/components/common";
import Mail from "@ui/icons/Mail";
import { createPartner } from "@/services/partner/requests";

interface InitDataT {
  id: string;
  functions: string[];
}

const formSchema = z.object({
  name: z.string().min(1, "This field is required"),
  garage_name: z.string(),
  email: z.string().min(1, "This field is required"),
  password: z.string().min(1, "This field is required"),
  currency: z.string().min(1, "This field is required"),
  country: z.string().min(1, "This field is required"),
  tax_number: z.string(),
  tax_percentage: z.string(),
  phone_number: z.string(),
  website: z.string(),
  address: z.string(),
  color_code: z.string(),
  contact_person: z.string(),
  logo: z.any(),
});

export default function PartnerForm({
  type,
  currentUser,
  initData,
  currencyList,
  countryList,
}: {
  type: "add" | "edit";
  currentUser: any;
  currencyList: any;
  countryList: any;
  initData?: InitDataT;
}) {
  const [loading, setLoading] = useState<boolean>(false);
  const { toast } = useToast();
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: "",
      garage_name: "",
      email: "",
      password: "",
      currency: "",
      country: "",
      tax_number: "",
      tax_percentage: "",
      phone_number: "",
      website: "",
      address: "",
      color_code: "",
      contact_person: "",
      logo: "",
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      let data = {
        ...values,
        logo: values.logo.base64,
        slug: "test",
        user_type: "Partner",
      };
      setLoading(true);

      let res;
      if (type == "add") {
        res = await createPartner({
          ...data,
          created_by: currentUser.id,
        });
      } else {
        // res = await updateCurrency({
        //   ...data,
        //   currency_id: initData.id,
        //   updated_by: currentUser.id,
        // });
      }

      setLoading(false);

      if (!res || res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: res ? "Please try again later!" : "API not available!",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully ${type == "add" ? "added" : "edited"} role!`,
          description: "",
        });
      }
    } catch (error: any) {
      console.log("🚀 ~ file: role-form.tsx:153 ~ onSubmit ~ error:", error);
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  }

  return (
    <div className="pt-8 px-8">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-5 bg-card p-8 rounded-lg">
            <ImageUpload
              className="col-span-2"
              form={form}
              label="Logo"
              name="logo"
            />
            <Input name="name" placeholder="Partner Name" form={form} />
            <Input name="garage_name" placeholder="Garage Name" form={form} />
            <Input
              name="email"
              placeholder="email"
              form={form}
              endIcon={<Mail />}
            />
            <PasswordInput form={form} name="password" />
            <Select
              name="currency"
              placeholder="Currency"
              options={currencyList.map((item) => ({
                label: item.name,
                value: item.id,
              }))}
              form={form}
            />
            <Select
              name="country"
              placeholder="Country"
              options={countryList.map((item) => ({
                label: item?.name?.common,
                value: item?.name?.common,
              }))}
              form={form}
            />
            <Input name="tax_number" placeholder="Tax No" form={form} />
            <Input name="tax_percentage" placeholder="Website" form={form} />
            <Input name="phone_number" placeholder="Phone No" form={form} />
            <Input name="website" placeholder="Website" form={form} />
            <Input name="address" placeholder="Address" form={form} />
            <Input name="color_code" placeholder="Color Code" form={form} />
            <Input
              className="col-span-2"
              name="contact_person"
              placeholder="Contact Person"
              form={form}
            />
            <Switch name="status" label="Status" form={form} />
          </div>
          <div className="flex items-center justify-end space-x-3 mt-8">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button type="submit" loading={loading}>
              Submit
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
