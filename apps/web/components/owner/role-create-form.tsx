"use client";

import { Input, Textarea } from "@ui/components/form";
import { Form } from "@ui/components/ui/form";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { DialogClose } from "@ui/components/ui/dialog";
import { Button } from "@ui/components/common";
import { createRoleCreation } from "@/services/role/requests";

const formSchema = z.object({
  name: z.string().min(2).max(50),
  description: z.string().max(256),
});

export default function RoleCreateForm({ currentUser }: { currentUser: any }) {
  const [loading, setLoading] = useState<boolean>(false);

  const { toast } = useToast();
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: "",
      description: "",
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      setLoading(true);
      const data = {
        name: values.name,
        description: values.description,
      };

      let res;
      res = await createRoleCreation({
        ...data,
        created_by: currentUser.id,
        slug: "user",
      });

      setLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "Please try again later!",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully added role creation!`,
          description: "",
        });
      }
    } catch (error: any) {
      console.log(
        "🚀 ~ file: currency-form.tsx:122 ~ onSubmit ~ error:",
        error
      );
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  }

  return (
    <div className="pt-8 px-8">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-1 gap-4 bg-card p-8 rounded-lg">
            <Input form={form} name="name" placeholder="Name" />
            <Textarea
              form={form}
              name="description"
              placeholder="Description"
            />
          </div>
          <div className="flex items-center justify-end space-x-3 mt-8">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button type="submit" loading={loading}>
              Submit
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
