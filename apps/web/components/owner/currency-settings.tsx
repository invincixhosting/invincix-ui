"use client";

import { Dialog, Tooltip } from "@ui/components/common";
import { DataTable } from "@ui/components/data-table";
import { PenLine, Plus } from "lucide-react";
import CurrencyForm from "./currency-form";

export default function CurrencySettings({
  data,
  currentUser,
}: {
  data: any;
  currentUser: any;
}) {
  const columns = [
    {
      field: "name",
      title: "Name",
      cellStyle: "text-primary font-bold",
      sort: true,
    },
    {
      field: "code",
      title: "Code",
      sort: true,
    },
    {
      field: "exchange_rate",
      title: "Exchange Rate",
      sort: true,
    },
    {
      field: "symbol",
      title: "Currency Symbol",
    },
    {
      field: "actions",
      title: "Actions",
      headerStyle: "text-right",
      renderCell: ({ row }) => {
        return (
          <div className="flex justify-end items-center">
            <Dialog
              title="Edit currency"
              content={
                <CurrencyForm
                  type="edit"
                  currentUser={currentUser}
                  initData={{
                    id: row.original.id,
                    name: row.original.name,
                    code: row.original.code,
                    exchange_rate: row.original.exchange_rate,
                    symbol: row.original.symbol,
                  }}
                />
              }
              description="Edit currency information"
              footer={<></>}
            >
              <Tooltip title="Edit currency">
                <div className="rounded-full bg-primary p-2">
                  <PenLine className="text-primary-foreground w-4 h-4" />
                </div>
              </Tooltip>
            </Dialog>
          </div>
        );
      },
    },
  ];

  return (
    <div>
      <div className="flex justify-start items-center w-full mb-5 space-x-5">
        <h1 className="text-2xl">Currency</h1>
        <Dialog
          title="Add Currency"
          content={<CurrencyForm type="add" currentUser={currentUser} />}
          description="Add a currency to your garage"
          footer={<></>}
        >
          <Tooltip title="Add Currency">
            <div className="rounded-full bg-primary p-2">
              <Plus className="text-primary-foreground w-6 h-6" />
            </div>
          </Tooltip>
        </Dialog>
      </div>
      <DataTable columns={columns} data={data} />
    </div>
  );
}
