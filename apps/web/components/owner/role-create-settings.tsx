"use client";

import { Dialog, Tooltip } from "@ui/components/common";
import Chip from "@ui/components/common/chip";
import { DataTable } from "@ui/components/data-table";
import { Plus } from "lucide-react";
import RoleCreateForm from "./role-create-form";

export default function RoleCreateSettings({
  roleList,
  currentUser,
}: {
  roleList: any;
  currentUser: any;
}) {
  const columns = [
    {
      field: "name",
      title: "Name",
      cellStyle: "text-primary font-bold",
      sorted: true,
    },
    {
      field: "description",
      title: "Description",
    },
    {
      field: "is_active",
      title: "Status",
      headerStyle: "text-center",
      renderCell: ({ row }) => {
        const isActive = row.original.is_active;
        return (
          <div className="flex justify-center items-center">
            <Chip
              title={isActive ? "Active" : "Inactive"}
              color={isActive ? "primary" : "destructive"}
            />
          </div>
        );
      },
    },
  ];

  return (
    <div>
      <div className="flex justify-start items-center w-full mb-5 space-x-5">
        <h1 className="text-2xl">Role Creation</h1>
        <Dialog
          title="Add Role Creation"
          content={<RoleCreateForm currentUser={currentUser} />}
          description="Add a role to your garage"
          footer={<></>}
        >
          <Tooltip title="Add Role Creation">
            <div className="rounded-full bg-primary p-2">
              <Plus className="text-primary-foreground w-6 h-6" />
            </div>
          </Tooltip>
        </Dialog>
      </div>
      <DataTable columns={columns} data={roleList} />
    </div>
  );
}
