"use client";

import { Input } from "@ui/components/form";
import { Form } from "@ui/components/ui/form";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { DialogClose } from "@ui/components/ui/dialog";
import { Button } from "@ui/components/common";
import { createCurrency, updateCurrency } from "@/services/currency/requests";

interface InitDataT {
  name: string;
  id: string;
  code: string;
  exchange_rate: string;
  symbol: string;
}

const addFormSchema = z.object({
  name: z.string().min(2).max(50),
  code: z.string().min(1).max(30),
  exchange_rate: z.string(),
  symbol: z.string().min(1).max(16),
});

const editFormSchema = z.object({
  name: z.string().min(2).max(50),
  code: z.string().min(1).max(30),
  exchange_rate: z.string(),
  symbol: z.string().min(1).max(16),
});

export default function CurrencyForm({
  type,
  currentUser,
  initData,
}: {
  type: "add" | "edit";
  currentUser: any;
  initData?: InitDataT;
}) {
  const [loading, setLoading] = useState<boolean>(false);

  const { toast } = useToast();
  const addForm = useForm<z.infer<typeof addFormSchema>>({
    resolver: zodResolver(addFormSchema),
    defaultValues: {
      name: "",
      code: "",
      exchange_rate: "",
      symbol: "",
    },
  });

  const editForm = useForm<z.infer<typeof editFormSchema>>({
    resolver: zodResolver(editFormSchema),
    defaultValues: {
      name: initData?.name,
      code: initData?.code,
      exchange_rate: initData?.exchange_rate,
      symbol: initData?.symbol,
    },
  });

  const handleAddCurrency = async (values: z.infer<typeof addFormSchema>) => {
    const data = {
      name: values.name,
      code: values.code,
      exchange_rate: values.exchange_rate,
      symbol: values.symbol,
    };
    await onSubmit(data);
  };

  const handleEditCurrency = async (values: z.infer<typeof editFormSchema>) => {
    const data = {
      name: values.name,
      code: values.code,
      exchange_rate: values.exchange_rate,
      symbol: values.symbol,
    };
    await onSubmit(data);
  };

  async function onSubmit(data: any) {
    try {
      setLoading(true);

      let res;
      if (type == "add") {
        res = await createCurrency({
          ...data,
          created_by: currentUser.id,
        });
      } else {
        res = await updateCurrency({
          ...data,
          currency_id: initData.id,
          updated_by: currentUser.id,
        });
      }

      setLoading(false);

      if (res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: "Please try again later!",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully ${type == "add" ? "added" : "edited"} currency!`,
          description: "",
        });
      }
    } catch (error: any) {
      console.log("🚀 ~ file: currency-form.tsx:122 ~ onSubmit ~ error:", error)
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  }

  return (
    <div className="pt-8 px-8">
      <Form {...(type == "add" ? addForm : editForm)}>
        <form
          onSubmit={(type == "add" ? addForm : editForm).handleSubmit(
            type == "add" ? handleAddCurrency : handleEditCurrency
          )}
        >
          <div className="grid grid-cols-2 gap-4 bg-card p-8 rounded-lg">
            <Input
              form={type == "add" ? addForm : editForm}
              name="name"
              placeholder="Name"
            />
            <Input
              form={type == "add" ? addForm : editForm}
              name="code"
              placeholder="Code"
            />
            <Input
              form={type == "add" ? addForm : editForm}
              name="exchange_rate"
              placeholder="Exchange Rate"
            />
            <Input
              form={type == "add" ? addForm : editForm}
              name="symbol"
              placeholder="Symbol"
            />
          </div>
          <div className="flex items-center justify-end space-x-3 mt-8">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button type="submit" loading={loading}>
              Submit
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
