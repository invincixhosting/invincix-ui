"use client";

import { Button, Dialog, Tooltip } from "@ui/components/common";
import { DataTable } from "@ui/components/data-table";
import { PenLine, Plus } from "lucide-react";
import RoleForm from "./role-form";
import { DialogClose } from "@ui/components/ui/dialog";

export default function RoleSettings({
  roleFunctionList,
  roleList,
  currentUser,
  functionList,
}: {
  roleFunctionList: any;
  roleList: any;
  currentUser: any;
  functionList: any;
}) {
  const columns = [
    {
      field: "rolename",
      title: "Role Name",
      headerStyle: "min-w-[150px]",
      cellStyle: "text-primary font-bold min-w-[150px]",
      sort: true,
    },
    {
      field: "functionName",
      title: "Function Access",
      renderCell: ({ row }) => {
        const functionName: string[] = row.original.functionName;
        return (
          <div className="flex items-center flex-wrap">
            {functionName.map((item) => (
              <span
                key={`function_${item}`}
                className="m-2 py-2 px-4 rounded-lg bg-secondary flex items-center justify-center"
              >
                {item}
              </span>
            ))}
          </div>
        );
      },
    },
    {
      field: "actions",
      title: "Actions",
      headerStyle: "text-right",
      renderCell: ({ row }) => {
        return (
          <div className="flex justify-end items-center">
            <Dialog
              title="Edit role"
              content={
                <RoleForm
                  roleList={roleList}
                  functionList={functionList}
                  type="edit"
                  initData={{
                    id: row.original.roleid,
                    functions: row.original.functionName,
                  }}
                  currentUser={currentUser}
                />
              }
              description="Edit role information"
              footer={<></>}
            >
              <Tooltip title="Edit role">
                <div className="rounded-full bg-primary p-2">
                  <PenLine className="text-primary-foreground w-4 h-4" />
                </div>
              </Tooltip>
            </Dialog>
          </div>
        );
      },
    },
  ];

  return (
    <div>
      <div className="flex justify-start items-center w-full mb-5 space-x-5">
        <h1 className="text-2xl">Roles</h1>
        <Dialog
          title="Add Role"
          content={
            <RoleForm
              roleList={roleList}
              functionList={functionList}
              type="add"
              currentUser={currentUser}
            />
          }
          description="Add a role to your garage"
          footer={<></>}
        >
          <Tooltip title="Add Role">
            <div className="rounded-full bg-primary p-2">
              <Plus className="text-primary-foreground w-6 h-6" />
            </div>
          </Tooltip>
        </Dialog>
      </div>
      <DataTable columns={columns} data={roleFunctionList} />
    </div>
  );
}
