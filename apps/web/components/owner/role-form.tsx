"use client";

import { Checkbox, Select, Switch } from "@ui/components/form";
import { Form } from "@ui/components/ui/form";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useState } from "react";
import { useToast } from "@ui/components/ui/use-toast";
import { DialogClose } from "@ui/components/ui/dialog";
import { Button } from "@ui/components/common";
import { createRole } from "@/services/role/requests";

interface InitDataT {
  id: string;
  functions: string[];
}

const formSchema = z.object({
  role: z.string().min(1, "Please select one role"),
  functions: z.array(z.string()),
  allAccess: z.array(z.string()),
  status: z.boolean(),
});

export default function RoleForm({
  type,
  currentUser,
  initData,
  functionList,
  roleList,
}: {
  type: "add" | "edit";
  currentUser: any;
  initData?: InitDataT;
  functionList: any;
  roleList: any;
}) {
  const [loading, setLoading] = useState<boolean>(false);
  const { toast } = useToast();
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      role: type == "edit" ? initData.id : "",
      allAccess: [],
      functions: type == "edit" ? getInitFunctionIds(initData.functions) : [],
      status: true,
    },
  });

  function getInitFunctionIds(initFunctions: string[]) {
    let fullFunctionList = [];
    Object.values(functionList).forEach(
      (item: any) =>
        (fullFunctionList = [...fullFunctionList, ...item.functions])
    );

    let initFunctionIds = [];
    initFunctions.forEach((name) => {
      const id = fullFunctionList.find((item) => item.name == name).id;
      initFunctionIds = [...initFunctionIds, id];
    });
    return initFunctionIds;
  }

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      let data: any = {
        roleid: values.role,
        functionid: values.functions,
      };
      setLoading(true);

      let res;
      if (type == "add") {
        res = await createRole({
          ...data,
          created_by: currentUser.id,
        });
      } else {
        // res = await updateCurrency({
        //   ...data,
        //   currency_id: initData.id,
        //   updated_by: currentUser.id,
        // });
      }

      setLoading(false);

      if (!res || res?.statusCode >= 300) {
        toast({
          variant: "error",
          title: "Error occurred!",
          description: res ? "Please try again later!" : "API not available!",
        });
      } else {
        toast({
          variant: "success",
          title: `Successfully ${type == "add" ? "added" : "edited"} role!`,
          description: "",
        });
      }
    } catch (error: any) {
      console.log("🚀 ~ file: role-form.tsx:153 ~ onSubmit ~ error:", error);
      setLoading(false);
      toast({
        variant: "error",
        title: "Error occurred!",
        description: "Please try again later!",
      });
    }
  }

  function handleSelectAll(e: string[]) {
    if (e?.length) {
      let allFunctionIds = [];
      for (let func of functionList) {
        allFunctionIds = [
          ...allFunctionIds,
          ...func.functions.map((e) => e.id),
        ];
      }
      form.setValue("functions", allFunctionIds);
    } else {
      form.setValue("functions", []);
    }
  }

  return (
    <div className="pt-8 px-8">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="grid grid-cols-1 gap-4 bg-card p-8 rounded-lg">
            <Select
              placeholder="Select Role"
              form={form}
              name="role"
              options={roleList.map((role) => ({
                label: role?.name,
                value: role?.id,
              }))}
            />
            {type == "edit" && (
              <Switch form={form} name="status" label="Status" />
            )}
            <h1 className="text-xl text-primary">Functions Access</h1>
            <Checkbox
              className="mb-5"
              onChange={handleSelectAll}
              form={form}
              name="allAccess"
              options={[
                {
                  label: "All Access",
                  value: "allAccess",
                },
              ]}
            />
            {functionList.map((item) => {
              return (
                <Checkbox
                  key={item.category?.name}
                  className="mb-5"
                  form={form}
                  name="functions"
                  label={item.category?.name}
                  options={item.functions.map((e) => ({
                    label: e.name,
                    value: e.id,
                  }))}
                />
              );
            })}
          </div>
          <div className="flex items-center justify-end space-x-3 mt-8">
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button type="submit" loading={loading}>
              Submit
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}
