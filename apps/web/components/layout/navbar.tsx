"use client";

import { BellDot, ChevronDown, Menu, X } from "lucide-react";
import { DropdownMenu, MenuContent } from "@ui/components/common";
import { Avatar } from "@ui/components/common";
import { Button } from "@ui/components/common";
import "./layout.css";
import { useState } from "react";
import { Sidebar, SidebarContent } from "./sidebar";
import { first } from "lodash";
import { useTranslation } from "@/i18n/client";
import { LocaleTypes } from "@/i18n/settings";
import ThemeButton from "../theme-button";
import LanguageButton from "../language-button";
import Bell from "@ui/icons/Bell";
import {
  Sheet,
  SheetContent,
  SheetDescription,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
  SheetClose,
} from "@ui/components/ui/sheet";
import MobileSideBar from "../mobile-sidebar";

interface MenuT {
  icon: React.ReactNode;
  link: string;
  title: string;
}

interface PropsT {
  brandName: string;
  brandLink: string;
  brandLogo: React.ReactNode;
  navBarMenus?: MenuContent[];
  menus: MenuT[];
  pathName: string;
  sidebarLogo: React.ReactNode;
  user: any;
  locale: LocaleTypes;
}

export function NavBar(props: PropsT): JSX.Element {
  const { t } = useTranslation(props.locale, "common");

  return (
    <div className="w-full flex">
      <div className="z-50 rounded-tl-lg md:rounded-t-lg h-30 w-9/12 bg-section header">
        <div className="p-5 flex item-center justify-between">
          <MobileSideBar
            brandLogo={props.brandLogo}
            menus={props.menus}
            pathName={props.pathName}
            sidebarLogo={props.sidebarLogo}
            user={props.user}
            locale={props.locale}
          />
          <div className="flex items-center md:items-start space-x-7">
            {props.brandLogo}
            <div className="hidden md:flex flex-col">
              <h1 className="text-xl text-muted">{t("hello")},</h1>
              <h1 className="text-xl">{props.user?.name || ""}</h1>
            </div>
          </div>
          <div className="flex space-x-10">
            <div className="flex space-x-3 items-center">
              <Button className="hidden md:block w-fit whitespace-nowrap">
                {t("check_in")}
              </Button>
              <Button className="hidden md:block" variant="secondary">
                <Bell />
              </Button>
              <ThemeButton />
              <LanguageButton locale={props.locale} />
            </div>
          </div>
        </div>
      </div>
      <div className="ml-0 md:mx-5 w-[200px] md:w-[300px] h-30 md:h-20 bg-section rounded-none md:rounded-lg flex items-center justify-center">
        <DropdownMenu
          title={
            <Button variant="ghost">
              <div className="flex items-center space-x-3">
                <Avatar
                  imgSrc={props.user?.image_path || ""}
                  imgAlt="Avatar"
                  fallback={first(props.user?.name) || ""}
                />
                <div className="text-left">
                  <p className="hidden md:inline-block">
                    {props.user?.name || ""}
                  </p>
                  <p className="text-muted">
                    {first(props.user?.transformedData)?.rolename ||
                      props.user?.user_type ||
                      ""}
                  </p>
                </div>
                <ChevronDown />
              </div>
            </Button>
          }
          menuContent={props.navBarMenus || []}
        ></DropdownMenu>
      </div>
    </div>
  );
}
