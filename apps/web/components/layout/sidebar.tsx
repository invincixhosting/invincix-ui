"use client";

import { Button, Tooltip } from "@ui/components/common";
import { useRouter } from "next/navigation";

interface MenuT {
  icon: React.ReactNode;
  link: string;
  title: string;
}

export function SidebarContent({
  menus,
  logo,
  pathName,
}: {
  menus: MenuT[];
  logo: React.ReactNode;
  pathName: string;
}): JSX.Element {
  const router = useRouter();

  const getActiveState = (
    path: string,
    link: string,
    type: "background" | "icon"
  ) => {
    const getState = (
      contentType: "background" | "icon",
      stateType: "active" | "normal"
    ) => {
      if (contentType == "background") {
        return stateType == "active"
          ? "dark:bg-[#CAFF32] bg-[#000000]"
          : "dark:bg-[#1F2904]";
      } else if (contentType == "icon") {
        return stateType == "active"
          ? "dark:text-black text-white"
          : "dark:text-[#CAFF32]";
      }
    };

    if (link === "/") {
      if (path === "/") {
        return getState(type, "active");
      } else {
        return getState(type, "normal");
      }
    } else if (path.startsWith(link)) {
      return getState(type, "active");
    } else {
      return getState(type, "normal");
    }
  };

  return (
    <div className="h-full px-3 pb-4 flex flex-col items-center justify-between">
      <ul className="space-y-4 font-medium max-h-[300px] overflow-y-scroll">
        {menus.map((item, i) => (
          <li key={item.title.split(" ").join("") + i}>
            <Button
              variant="ghost"
              className={`flex items-center justify-center text-[#000000] bg-[#EBEBEB] rounded-[18px] hover:bg-[#000000] hover:text-[#FFFFFF] dark:text-white dark:hover:bg-[#324207] group w-[50px] h-[50px] ${getActiveState(
                pathName,
                item.link,
                "background"
              )}`}
              onClick={() => router.push(item.link)}
            >
              <Tooltip side="right" title={item.title}>
                <span
                  className={`${getActiveState(
                    pathName,
                    item.link,
                    "icon"
                  )} transition duration-75`}
                >
                  {item.icon}
                </span>
              </Tooltip>
            </Button>
          </li>
        ))}
      </ul>
      {Boolean(logo) ? <div className="mt-20">{logo}</div> : <></>}
    </div>
  );
}

export function Sidebar({
  menus,
  logo,
  pathName,
}: {
  menus: MenuT[];
  logo: React.ReactNode;
  pathName: string;
}): JSX.Element {
  return (
    <aside
      className="fixed top-32 md:top-48 left-0 z-40 pt-5 transition-transform -translate-x-[140%] md:translate-x-0 bg-gradient-to-b from-[#FFFFFF] to-[#FFFFFF] dark:from-[#000000] dark:to-[#3A4E01] w-[80px] my-0 md:my-5 sm:mx-5 mx-0 rounded-lg"
      aria-label="Sidebar"
    >
      <SidebarContent menus={menus} logo={logo} pathName={pathName} />
    </aside>
  );
}
