"use client";

import "./layout.css";
import * as CONFIGS from "@/const/configs";
import Image from "next/image";
import LogOutDialog from "../log-out-dialog";
import brandLogo from "@/media/logos/TeltoLogo.svg";
import moment from "moment";
import sidebarLogo from "@/media/logos/greeting-robot.svg";
import { ArrowLeft, LogOut } from "lucide-react";
import { Button } from "@ui/components/common";
import { LocaleTypes } from "@/i18n/settings";
import { NavBar } from "./navbar";
import { Sidebar } from "./sidebar";
import { first, last } from "lodash";
import { usePathname } from "next/navigation";
import { useRouter } from "next/navigation";
import { useTranslation } from "@/i18n/client";
import { useState } from "react";
import { getUserMenu } from "@/const/menus";

interface PropsT {
  children: React.ReactNode;
  user?: any;
  locale: LocaleTypes;
}

export function Layout(props: PropsT): JSX.Element {
  const { t } = useTranslation(props.locale, "common");
  const [logoutOpen, setLogoutOpen] = useState<boolean>(false);
  const pathname = usePathname();
  const userRole =
    first(props.user?.transformedData)?.rolename || props.user?.user_type;
  const router = useRouter();

  if (!props.user) {
    router.push("/login");
  }

  return (
    <div className="bg-background min-h-screen md:p-4 p-0">
      <div className="fixed top-0 z-50 md:pt-4 w-full">
        <NavBar
          locale={props.locale}
          brandName={CONFIGS.BRAND_NAME}
          brandLink={CONFIGS.BRAND_LINK}
          brandLogo={
            <Image
              src={brandLogo}
              width={100}
              height={65}
              alt="Brand Logo"
              className="h-10 mr-3"
            />
          }
          navBarMenus={[
            {
              type: "label",
              content: (
                <div className="">
                  <h1 className="font-bold">{props.user.name}</h1>
                  <h2 className="text-muted">
                    {first(props.user?.transformedData)?.rolename ||
                      props.user?.user_type ||
                      ""}
                  </h2>
                </div>
              ),
            },
            { type: "separator" },
            {
              type: "item",
              onClick: () => {
                setLogoutOpen(true);
              },
              content: (
                <div className="flex items-center space-x-2">
                  <LogOut className="w-4 h-4" />
                  <span>{t("log_out")}</span>
                </div>
              ),
            },
          ]}
          sidebarLogo={
            <Image
              src={sidebarLogo}
              width={50}
              height={90}
              alt="Sidebar Logo"
            />
          }
          menus={getUserMenu(props.locale, userRole, t)}
          pathName={pathname}
          user={props.user}
        />
      </div>
      <Sidebar
        menus={getUserMenu(props.locale, userRole, t)}
        logo={
          <Image src={sidebarLogo} width={50} height={90} alt="Sidebar Logo" />
        }
        pathName={pathname}
      />

      <LogOutDialog open={logoutOpen} setOpen={setLogoutOpen} />

      <div className="mt-[5rem] md:mt-24 flex space-x-0">
        <div className="hidden md:flex h-[80px] rounded-bl-lg bg-section w-28 items-start justify-center pre-header relative">
          <div
            className={`p-2 rounded-lg text-white bg-gradient-to-b from-[#1B1B29] to-[#267B67] flex flex-col items-center justify-center transition-all ${
              last(pathname.split("/")) == "home"
                ? "scale-100 rotate-0"
                : "scale-0 -rotate-90"
            }`}
          >
            <span className="text-md font-bold">{moment().format("DD")}</span>
            <span className="text-[10px]">{moment().format("MMMM")}</span>
          </div>
          <Button
            variant="secondary"
            className={`w-[50px] h-[50px] transition-all absolute ${
              last(pathname.split("/")) == "home"
                ? "scale-0 -rotate-90"
                : "scale-100 rotate-0"
            }`}
            onClick={() => router.back()}
          >
            <ArrowLeft className="w-5 h-5" />
          </Button>
        </div>
        <div className="p-8 md:p-14 min-h-[calc(100vh)] md:min-h-[calc(100vh-9rem)] max-h-[calc(100vh-9rem)] py-8 bg-section rounded-lg rounded-tl-none w-full layout">
          {props.children}
        </div>
      </div>
    </div>
  );
}
