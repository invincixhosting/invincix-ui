"use client";

import { Button } from "@ui/components/common";
import { useTheme } from "next-themes";
import Sun from "@ui/icons/Sun";
import Moon from "@ui/icons/Moon";

export default function ThemeButton() {
  const { theme, setTheme } = useTheme();

  return (
    <Button
      variant="secondary"
      onClick={() => setTheme(theme == "dark" ? "light" : "dark")}
    >
      <Sun
        color="black"
        className="rotate-0 scale-100 transition-all dark:-rotate-90 dark:scale-0"
      />
      <Moon className="absolute rotate-90 scale-0 transition-all dark:rotate-0 dark:scale-100" />
    </Button>
  );
}
