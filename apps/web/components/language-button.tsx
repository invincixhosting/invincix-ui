"use client";

import { useTranslation } from "@/i18n/client";
import { LocaleTypes } from "@/i18n/settings";
import { Button, DropdownMenu } from "@ui/components/common";
import { ChevronDown } from "lucide-react";
import { Globe } from "lucide-react";
import { usePathname, useRouter } from "next/navigation";

export default function LanguageButton({ locale }: { locale: LocaleTypes }) {
  const { t } = useTranslation(locale, "common");
  const router = useRouter();
  const pathname = usePathname();

  let pathnameWithoutLocale = pathname.split("/");
  pathnameWithoutLocale.splice(0, 2);

  return (
    <DropdownMenu
      title={
        <Button variant="secondary" className="flex space-x-3">
          <Globe /> <span>{locale == "en" ? "ENG" : "VIE"}</span>{" "}
          <ChevronDown />
        </Button>
      }
      menuContent={[
        {
          content: t("choose_language"),
          type: "label",
        },
        {
          type: "separator",
        },
        {
          content: "English",
          type: "item",
          onClick: () => {
            router.push(`/en/${pathnameWithoutLocale.join("/")}`);
          },
        },
        {
          content: "Tiếng Việt",
          type: "item",
          onClick: () => {
            router.push(`/vi/${pathnameWithoutLocale.join("/")}`);
          },
        },
      ]}
    />
  );
}
