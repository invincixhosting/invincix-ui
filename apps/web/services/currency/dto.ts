export interface CreateCurrencyDTO {
  name: string;
  exchange_rate: string;
  code: string;
  symbol: string;
  created_by: string;
}

export interface UpdateCurrencyDTO {
  currency_id: string;
  name: string;
  exchange_rate: string;
  code: string;
  symbol: string;
  updated_by: string;
}
