import { callApi } from "@/lib";
import { CreateCurrencyDTO, UpdateCurrencyDTO } from "./dto";

export const getAllCurrency = () => {
  return callApi(`/currency`, {}, "GET");
};

export const createCurrency = (body: CreateCurrencyDTO) => {
  return callApi(
    `/currency`,
    {
      currency: {
        name: body.name,
        exchange_rate: body.exchange_rate,
        code: body.code,
        symbol: body.symbol,
        created_by: body.created_by,
      },
    },
    "POST",
    false
  );
};

export const updateCurrency = (body: UpdateCurrencyDTO) => {
  return callApi(
    `/currency/${body.currency_id}`,
    {
      currency: {
        name: body.name,
        exchange_rate: body.exchange_rate,
        code: body.code,
        symbol: body.symbol,
        updated_by: body.updated_by,
      },
    },
    "PUT",
    false
  );
};
