export interface CreateServiceCategoryDTO {
  name: string;
  description: string;
  slug: string;
  partner_id: string;
  services: any[];
  created_by: string;
}

export interface UpdateServiceCategoryDTO {
  name: string;
  description: string;
  slug: string;
  partner_id: string;
  services: any[];
  service_category_id: string;
  IsActive?: boolean;
  updated_by: string;
}

export interface CreateServiceDTO {
  name: string;
  description: string;
  price: number;
  durationInMinute: string;
  image_path: string;
  service_category_id: string;
  partner_id: string;
  slug: string;
  created_by: string;
}

export interface UpdateServiceDTO {
  name: string;
  description: string;
  price: number;
  durationInMinute: string;
  image_path: string;
  service_category_id: string;
  service_id: string;
  partner_id: string;
  slug: string;
  is_active: boolean;
  updated_by: string;
}

export interface CreateStockDTO {
  product_category: string;
  product: string;
  quantity: number;
  unit_price: number;
  receipt_number: string;
  receipt_date: string;
  po_reference_number: string;
  po_date: string;
  supplier: string;
  partner_id: string;
  slug: string;
  created_by: string;
  base_price: number;
}

export interface UpdateStockDTO {
  product_category: string;
  product: string;
  quantity: number;
  unit_price: number;
  receipt_number: string;
  receipt_date: string;
  po_reference_number: string;
  po_date: string;
  supplier: string;
  partner_id: string;
  stock_id: string;
  slug: string;
  IsActive: boolean;
  updated_by: string;
  base_price: number;
}
