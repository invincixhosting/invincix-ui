import { callApi } from "@/lib";
import {
  CreateServiceCategoryDTO,
  CreateServiceDTO,
  CreateStockDTO,
  UpdateServiceCategoryDTO,
  UpdateServiceDTO,
  UpdateStockDTO,
} from "./dto";

export const getAllService = (id: string, queryParams: any) => {
  const queryString = new URLSearchParams(queryParams).toString();
  return callApi(`/service/${id}?${queryString}`, {}, "GET");
};

export const createServiceCategory = (data: CreateServiceCategoryDTO) => {
  return callApi(`/service-category`, { serviceCategory: data }, "POST", false);
};

export const updateServiceCategory = (data: UpdateServiceCategoryDTO) => {
  return callApi(
    `/service-category/${data.service_category_id}`,
    {
      serviceCategory: {
        name: data.name,
        description: data.description,
        slug: data.slug,
        partner_id: data.partner_id,
        services: data.services,
        IsActive: data.IsActive,
        updated_by: data.updated_by,
      },
    },
    "PUT",
    false
  );
};

export const getAllActiveServiceCategory = (id: string) => {
  return callApi(`/service-category/active/${id}`, {}, "GET");
};

export const createService = (body: CreateServiceDTO) => {
  return callApi(`/service`, { service: body }, "POST", false);
};

export const updateService = (body: UpdateServiceDTO) => {
  return callApi(
    `/service/${body.service_id}`,
    {
      service: {
        name: body.name,
        description: body.description,
        price: body.price,
        durationInMinute: body.durationInMinute,
        image_path: body.image_path,
        service_category_id: body.service_category_id,
        partner_id: body.partner_id,
        slug: body.slug,
        is_active: body.is_active,
        updated_by: body.updated_by,
      },
    },
    "PUT",
    false
  );
};

export const getAllStock = (id: string) => {
  return callApi(`/product-inventory/${id}`, {}, "GET");
};

export const stockOut = (id: string) => {
  return callApi(`/product-inventory/stockOut/${id}`, {}, "PUT", false);
};

export const createStock = (body: CreateStockDTO) => {
  return callApi(
    `/product-inventory`,
    { productInventory: body },
    "POST",
    false
  );
};

export const updateStock = (body: UpdateStockDTO) => {
  return callApi(
    `/product-inventory/${body.stock_id}`,
    {
      productInventory: {
        product_category: body.product_category,
        product: body.product,
        quantity: body.quantity,
        unit_price: body.unit_price,
        receipt_number: body.receipt_number,
        receipt_date: body.receipt_date,
        po_reference_number: body.po_reference_number,
        po_date: body.po_date,
        supplier: body.supplier,
        partner_id: body.partner_id,
        stock_id: body.stock_id,
        slug: body.slug,
        IsActive: body.IsActive,
        updated_by: body.updated_by,
        base_price: body.base_price,
      },
    },
    "PUT",
    false
  );
};
