export const getAllCountries = async () => {
  const res = await fetch("https://restcountries.com/v3.1/all", {
    headers: {
      "Content-Type": "application/json",
      Accept: "*",
    },
  });
  const data = await res.json();
  if (data) {
    return data;
  }
  return null;
};

export const productUOM: string[] = [
  "piece",
  "dozen",
  "pack",
  "box",
  "case",
  "carton",
  "set",
  "pair",
  "meter",
  "centimeter",
  "inch",
  "pound",
  "kilogram",
  "gram",
  "liter",
  "milliliter",
  "gallon",
  "quart",
  "pint",
  "cup",
  "fluid ounce",
];
