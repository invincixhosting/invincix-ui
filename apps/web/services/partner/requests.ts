import { callApi } from "@/lib";
import { CreatePartnerDTO } from "./dto";

export const getAllPartner = () => {
  return callApi(`/partners`, {}, "GET");
};

export const createPartner = (body: CreatePartnerDTO) => {
  return callApi(`/partners`, { partner: body }, "POST", false);
};
