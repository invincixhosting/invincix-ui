export interface CreatePartnerDTO {
  name?: string;
  contact_person?: string;
  garage_name?: string;
  email?: string;
  password?: string;
  currency?: string;
  country?: string;
  user_type?: string;
  tax_number?: string;
  phone_number?: string;
  website?: string;
  tax_percentage?: string;
  address?: string;
  color_code?: string;
  logo?: string;
  slug?: string;
  created_by?: string;
}
