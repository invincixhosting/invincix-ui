export interface GetReportDTO {
  id: string;
  startDate: string;
  endDate: string;
}

export interface GetAllMechanicJobsDTO {
  id: string;
  partner_id: string;
}

export interface JobcardTask {
  bay: string | undefined;
  jobcard_id: string;
}

export interface UpdateBayByMechanicDTO {
  jobcardTask: JobcardTask;
}

export interface RequestSparePartDTO {
  jobCard_id: string;
  product_id: string;
  quantity: number;
  is_estimated: number;
  created_by: string;
  partner_id: string;
}

export interface JobCardSpares {
  id: string;
  partner_id: string;
  jobCard_id: string | number | undefined;
  product_id: string | number;
  quantity: string | number;
  is_estimated: string;
  created_by: string;
}

export interface ReturnSparePartDTO {
  jobcardSpareParts: JobCardSpares[];
}

export interface UpdateJobCardStatusDTO {
  id: string;
  status: string;
  starttime: string | null;
  endtime: string | null;
  updatedBy: string;
  Jobcard_id: string;
}

export interface JobCardT {
  id: string;
  jobCard_id: string;
  partner_id: string;
  task: string;
  duration: string;
  assigned_to: string;
  status: string;
  start_time: string;
  record_time: string;
  hour_log_time: string;
  end_time: string;
  module: string;
  module_id: string;
  is_active: boolean;
  is_deleted: boolean;
  jobcard_serviceId: string;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
  details: string[];
  bay_name: string;
  assigned_by: string;
}

export interface ArchiveJobCardT {
  id: string;
  status: string;
  is_active: boolean;
  is_deleted: boolean;
  bay: string;
  vehicle: {
    id: string;
    vehicle_number: string;
    vehicle_name: string;
    vehicle_id: string;
    color: string;
    model_number: string;
  };
  customer: {
    id: string;
    customer_name: string;
    email: string;
    customer_id: string;
    phone_number: string;
  };
}

export interface MechanicFeedBackDTO {
  mechanic_id: string;
  partner_id: string;
  rating: string;
  review: string;
}

interface JobCardDetailSpare {
  id: string;
  jobCard_id: string;
  partner_id: string;
  product_id: string;
  quantity: string;
  price: string | number | null;
  is_estimated: string;
  is_final: string | boolean | null;
  is_active: boolean;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
  product_name: string;
  product_image: string;
}

interface JobCardDetailService {
  id: string;
  jobCard_id: string;
  partner_id: string;
  service_id: string;
  service_price: string;
  is_estimated: string;
  assigned_to: string;
  jobcard_serviceId: string;
  is_final: string | boolean | null;
  is_active: boolean;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
  service_name: string;
  service_image: string;
  task_status: string;
}

interface JobCardDetailTask {
  id: string;
  jobCard_id: string;
  partner_id: string;
  task: string;
  duration: string;
  assigned_to: string;
  status: string;
  start_time: string | null;
  record_time: string | null;
  hour_log_time: string | null;
  end_time: string | null;
  module: string;
  module_id: string;
  is_active: boolean;
  is_deleted: boolean;
  jobcard_serviceId: string;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
}

interface JobCardDetailExtraService {
  id: string;
  jobCard_id: string;
  partner_id: string;
  description: string;
  price: string;
  is_estimated: boolean | string | null;
  is_final: boolean | string | null;
  is_active: boolean;
  estimated: string;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
}

interface JobCardDetailComplaint {
  id: string;
  jobCard_id: string;
  partner_id: string;
  complaints: string;
  assigned_to: string;
  duration: string;
  jobcard_serviceId: string;
  is_active: boolean;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
  task_status: string;
}

export interface JobCardDetailT {
  jobcard: {
    id: string;
    vehicle_id: string;
    customer_id: string;
    kilometer_driven: number;
    status: string;
    bay: string;
    partner_id: string;
    is_active: boolean;
    is_deleted: boolean;
    slug: string;
    estimation_status: string | null;
    estimation_data: string | null;
    image_paths: string[];
    created_at: string;
    created_by: string;
    modified_at: string;
    modified_by: string;
    vehicle_number: string;
    color: string;
    model_number: string;
    customer_name: string;
    email: string;
    phone_number: string;
    address: string;
    spare: JobCardDetailSpare[];
    services: JobCardDetailService[];
    tasks: JobCardDetailTask[];
    complaints: JobCardDetailComplaint[];
    extraService: JobCardDetailExtraService[];
  };
}

export interface CreateJobCardServiceDTO {
  jobcardService: JobcardService[];
}

export interface JobcardService {
  partner_id: string;
  jobCard_id: string | number | undefined;
  service_id: string;
  is_estimated: string;
  assigned_to: string;
  created_by: string;
}

export interface CreateJobCardSparesDTO {
  jobcardSpareParts: {
    partner_id: string;
    jobCard_id: string | number | undefined;
    product_id: string | number;
    quantity: string | number;
    is_estimated: string;
    created_by: string;
  }[];
}

export interface CreateJobCardDTO {
  vehicle_number: string;
  vehicle_type: string;
  color: string;
  model_number: string;
  engine_number: string;
  chasis_number: string;
  vehicle_category: string;
  partner_id: string;
  kilometer_driven: number | string;
  image_paths?: string[];
  phone_number: string;
  name: string;
  email: string;
  address: string;
  slug: string;
  created_by: string;
}

export interface JobCardCreateFinalData_Complaints {
  jobCard_id: string;
  complaints: string;
  duration: string;
  assigned_to: string;
  created_by: string;
  slug: string;
  partner_id: string;
}

export interface JobCardCreateFinalData_Service {
  jobCard_id: string;
  service_id: string;
  is_estimated: string;
  assigned_to: string;
  created_by: string;
  partner_id: string;
  price: string;
}

export interface JobCardCreateFinalData_Spares {
  jobCard_id: string;
  product_id: string;
  quantity: string;
  is_estimated: string;
  created_by: string;
  partner_id: string;
  price: string;
}

export interface JobCardCreateFinalData_extraService {
  jobCard_id: string;
  description: string;
  price: string;
  estimated: string;
  created_by: string;
  partner_id: string;
}

export interface JobCardCreateFinalDataDTO {
  jobcardComplain: JobCardCreateFinalData_Complaints[];
  jobcardService: JobCardCreateFinalData_Service[];
  jobcardSpareParts: JobCardCreateFinalData_Spares[];
  jobcardExtraService: JobCardCreateFinalData_extraService[];
}
