import { callApi } from "@/lib";
import {
  CreateJobCardDTO,
  CreateJobCardServiceDTO,
  CreateJobCardSparesDTO,
  GetAllMechanicJobsDTO,
  GetReportDTO,
  JobCardCreateFinalDataDTO,
  MechanicFeedBackDTO,
  ReturnSparePartDTO,
  UpdateBayByMechanicDTO,
  UpdateJobCardStatusDTO,
} from "./dto";

export const getAllMechanics = (partnerId: string) => {
  return callApi(`/jobcard-tasks/allmechanic/${partnerId}`, {}, "GET");
};

export const getGarageReport = (body: GetReportDTO) => {
  return callApi(
    `/jobcard/garageReport/${body.id}?startDate=${body.startDate}&endDate=${body.endDate}`,
    {},
    "GET",
    false
  );
};

export const getTaskReport = (body: GetReportDTO) => {
  return callApi(
    `/jobcard/taskReport/${body.id}?startDate=${body.startDate}&endDate=${body.endDate}`,
    {},
    "GET",
    false
  );
};

export const getRevenueReport = (body: GetReportDTO) => {
  return callApi(
    `/jobcard/revenueReport/${body.id}?startDate=${body.startDate}&endDate=${body.endDate}`,
    {},
    "GET",
    false
  );
};

export const getSparePartReport = (body: GetReportDTO) => {
  return callApi(
    `/jobcard/sparePartReport/${body.id}?startDate=${body.startDate}&endDate=${body.endDate}`,
    {},
    "GET",
    false
  );
};

export const getMechanicReport = (body: GetReportDTO) => {
  return callApi(
    `/jobcard/mechanicReport/${body.id}?startDate=${body.startDate}&endDate=${body.endDate}`,
    {},
    "GET",
    false
  );
};

export const getJobDurationReport = (body: GetReportDTO) => {
  return callApi(
    `/jobcard/jobDurationReport/${body.id}?startDate=${body.startDate}&endDate=${body.endDate}`,
    {},
    "GET",
    false
  );
};

export const getAllMechanicJobs = (body: GetAllMechanicJobsDTO) => {
  return callApi(
    `/jobcard-tasks/mechanic/${body.id}?partner_id=${body.partner_id}`,
    {},
    "GET"
  );
};

export const updateBayByMechanic = (body: UpdateBayByMechanicDTO) => {
  return callApi(
    `/jobcard-tasks/updateBay/${body.jobcardTask.jobcard_id}`,
    {
      jobcardTask: {
        bay: body.jobcardTask.bay,
      },
    },
    "PUT",
    false
  );
};

export const returnSparePart = (data: ReturnSparePartDTO) => {
  return callApi(`/jobcard-spare-parts/add-inventory`, data, "POST", false);
};

export const updateJobCardStatus = (body: UpdateJobCardStatusDTO) => {
  return callApi(
    `/jobcard-tasks/${body.id}`,
    {
      jobcardTask: {
        status: body.status,
        start_time: body.starttime,
        end_time: body.endtime,
        updated_by: body.updatedBy,
        Jobcard_id: body.Jobcard_id,
      },
    },
    "PUT",
    false
  );
};

export const mechanicReAssignTask = (data: any) => {
  return callApi(
    `/jobcard-tasks/reassign/${data.id}`,
    {
      jobcardTask: [
        {
          assigned_to: data.assigned_to,
          createdBy: data.updated_by,
        },
      ],
    },
    "POST",
    false
  );
};

export const deleteJobcardServiceById = (id: string) => {
  return callApi(`/jobcard-service/${id}`, {}, "DELETE", false);
};

export const deleteSparePartById = (id: string) => {
  return callApi(`/jobcard-spare-parts/${id}`, {}, "DELETE", false);
};

export const getAllArchiveJobCard = (id: string) => {
  return callApi(`/jobcard/archiveJobCardData/${id}`, {}, "GET");
};

export const createFeedbackMechanics = (body: MechanicFeedBackDTO) => {
  return callApi(`/mechanic-feedback/`, body, "POST", false);
};

export const getAllJobCard = (id: string) => {
  return callApi(`/jobcard/${id}`, {}, "GET");
};

export const getJobCardDetails = (id: string) => {
  return callApi(`/jobcard/jobcardDetails/${id}`, {}, "GET", false);
};

export const getEstimate = (id: string) => {
  return callApi(`/jobcard/estimation/${id}`, {}, "GET", false);
};

export const getAllActiveService = (id: string) => {
  return callApi(`/service/active/${id}`, {}, "GET");
};

export const createJobCardServices = (data: CreateJobCardServiceDTO) => {
  return callApi(`/jobcard-service/`, data, "POST", false);
};

export const createJobCardSpareParts = (data: CreateJobCardSparesDTO) => {
  return callApi(`/jobcard-spare-parts/remove-inventory`, data, "POST", false);
};

export const createJobCard = (data: CreateJobCardDTO) => {
  return callApi(`/jobcard`, { jobcard: data }, "POST", false);
};

export const createJobCardFinal = (data: JobCardCreateFinalDataDTO) => {
  return callApi(`/jobcard/update`, data, "POST", false);
};
