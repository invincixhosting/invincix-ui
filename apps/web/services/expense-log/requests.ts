import { callApi } from "@/lib";
import { CreateExpensesLogDTO } from "./dto";

export const getAllExpenseLog = (partnerId: string) => {
  return callApi(`/expense-log/${partnerId}`, {}, "GET");
};

export const createExpenseLog = (data: CreateExpensesLogDTO) => {
  return callApi(`/expense-log`, data, "POST", false);
};
