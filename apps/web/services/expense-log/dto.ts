export interface CreateExpensesLogDTO {
  title: string;
  description: string;
  amount: string;
  partner_id: string;
  category: string;
  isExpenses: boolean;
}
