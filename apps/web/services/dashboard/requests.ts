import { callApi } from "@/lib";

export const getMechanicDashboard = (id: string) => {
  return callApi(`/jobcard/mechanicstatus/${id}`, {}, "GET");
};

export const getPercentageStatus = (id: string) => {
  return callApi(`/jobcard-tasks/dashboard/${id}`, {}, "GET");
};

export const getOperationDashboard = (id: string, params?: any) => {
  let queryString = new URLSearchParams(params);
  return callApi(`/jobcard/dashboard/${id}?${queryString}`, {}, "GET");
};

export const getRevenueDashboard = (id: string, params?: any) => {
  let queryString = new URLSearchParams(params);
  return callApi(`/jobcard/dashboard/revenue/${id}`, {}, "GET");
};

export const getReviewDashboard = (id: string, params?: any) => {
  let queryString = new URLSearchParams(params);
  return callApi(`/jobcard/dashboard/review/${id}`, {}, "GET");
};

export const getCustomerFeedback = (id: string) => {
  return callApi(`/customer-feedback/review/${id}`, {}, "GET");
};

export const getMechanicFeedback = (id: string) => {
  return callApi(`/mechanic-feedback/${id}`, {}, "GET");
};
