import { callApi } from "@/lib";
import { CreateRoleDTO, RoleCreationDTO } from "./dto";

export const getAllRoles = () => {
  return callApi(`/role`, {}, "GET");
};

export const getAllRoleFunctions = () => {
  return callApi(`/role-function`, {}, "GET");
};

export const getAllRole = () => {
  return callApi(`/role/GetAllRole`, {}, "GET");
};

export const createRoleCreation = (data: RoleCreationDTO) => {
  return callApi(
    `/role`,
    {
      role: {
        name: data.name,
        description: data.description,
        slug: data.slug,
        created_by: data.created_by,
      },
    },
    "POST",
    false
  );
};

export const getAllFunction = () => {
  return callApi(`/function`, {}, "GET");
};

export const createRole = (data: CreateRoleDTO) => {
  return callApi(
    `/role-function`,
    {
      roleFunction: {
        roleid: data.roleid,
        functionid: data.functionid,
        created_by: data.created_by,
      },
    },
    "POST",
    false
  );
};
