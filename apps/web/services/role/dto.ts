export interface RoleCreationDTO {
  name: string;
  description: string;
  slug: string;
  created_by: string;
}

export interface CreateRoleDTO {
  roleid: string;
  functionid: string[];
  created_by: string;
}
