export interface CreateUserDTO {
  name: string;
  email: string;
  user_type: string;
  password: string;
  partner_id: string;
  created_by: string;
  slug: string;
}

export interface UpdateUserDTO {
  user_id: string;
  name: string;
  email: string;
  user_type?: string;
  password?: string;
  partner_id: string;
  slug: string;
  is_active?: boolean;
  updated_by: string;
  specialities?: string[];
}

export interface CreateSupplierDTO {
  name: string;
  address: string;
  registration_number: string;
  phone_number: string;
  slug: string;
  created_by: string;
  partner_id: string;
}

export interface UpdateSupplierDTO {
  partner_id: string;
  name: string;
  address: string;
  registration_number: string;
  phone_number: string;
  slug: string;
  updated_by: string;
  IsActive: boolean;
  supplier_id: string;
}
