import { callApi } from "@/lib";
import { CreateSupplierDTO, CreateUserDTO, UpdateSupplierDTO, UpdateUserDTO } from "./dto";

export const getAllUsers = (partnerId: string) => {
  return callApi(`/user/${partnerId}`, {}, "GET");
};

export const createUser = (data: CreateUserDTO) => {
  return callApi(`/user/`, { user: data }, "POST", false);
};

export const updateUser = (data: UpdateUserDTO) => {
  return callApi(
    `/user/${data.user_id}`,
    {
      user: {
        name: data.name,
        email: data.email,
        user_type: data.user_type,
        password: data.password,
        specialities: data.specialities,
        partner_id: data.partner_id,
        slug: data.slug,
        is_active: data.is_active,
        updated_by: data.updated_by,
      },
    },
    "PUT",
    false
  );
};

export const getAllSupervisors = (partnerId: string) => {
  return callApi(`/user/allSupervisor/${partnerId}`, {}, "GET");
};

export const getAllSupplier = (id: string, queryParams?: any) => {
  const queryString = new URLSearchParams(queryParams).toString();
  return callApi(`/supplier/${id}?${queryString}`, {}, "GET");
};

export const createSupplier = (body: CreateSupplierDTO) => {
  return callApi(`/supplier`, { supplier: body }, "POST", false);
};

export const updateSupplier = (body: UpdateSupplierDTO) => {
  return callApi(
    `/supplier/${body.supplier_id}`,
    {
      supplier: {
        name: body.name,
        address: body.address,
        registration_number: body.registration_number,
        phone_number: body.phone_number,
        slug: body.slug,
        updated_by: body.updated_by,
        IsActive: body.IsActive,
      },
    },
    "PUT",
    false
  );
};
