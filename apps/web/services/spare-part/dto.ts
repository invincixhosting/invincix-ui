export interface Stock {
  inventoryid: string;
  quantity: string | number;
}

export interface UpdateReqSparePartDTO {
  request_id: string;
  status: string;
  product_id: string;
  stock?: Stock[];
  jobcard_id: string;
  partnerid: string;
  createdby: string;
}
