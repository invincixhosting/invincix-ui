import { callApi } from "@/lib";
import { RequestSparePartDTO } from "../job-card/dto";
import { UpdateReqSparePartDTO } from "./dto";

export const requestSparePart = (body: RequestSparePartDTO[]) => {
  return callApi(`/sparepart-request`, body, "POST", false);
};

export const getAllSpareRequest = (id: string) => {
  return callApi(`/sparepart-details/${id}`, {}, "GET");
};

export const updateSparePartRequest = (body: UpdateReqSparePartDTO) => {
  return callApi(
    `/sparepart-details/${body.product_id}`,
    {
      request_id: body.request_id,
      status: body.status,
      stock: body.stock,
      jobcard_id: body.jobcard_id,
      partner_id: body.partnerid,
      created_by: body.createdby,
    },
    "PUT",
    false
  );
};
