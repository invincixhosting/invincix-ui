export interface PaymentT {
  id: string;
  jobCard_id: string;
  customer_name: string;
  vehicle_number: string;
  contact: string;
  amount: string;
  status: string;
  payment_method: string;
  remarks: string;
  partner_id: string;
  slug: string;
  is_active: boolean;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string | null;
}

export interface UpdatePaymentDTO {
  status: string;
  payment_id: string;
  payment_method: string;
  remarks: string;
}
