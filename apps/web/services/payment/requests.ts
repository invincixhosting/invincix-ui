import { callApi } from "@/lib";
import { UpdatePaymentDTO } from "./dto";

export const getAllPaymentRequest = (id: string) => {
  return callApi(`/payment/${id}`, {}, "GET");
};

export const updatePaymentRequest = (body: UpdatePaymentDTO) => {
  return callApi(
    `/payment/${body.payment_id}`,
    {
      status: body.status,
      payment_method: body.payment_method,
      remarks: body.remarks,
    },
    "PUT",
    false
  );
};
