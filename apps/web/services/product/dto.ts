export interface CreateProductCategoryDTO {
  name: string;
  description: string;
  partner_id: string;
  slug: string;
  products: any[];
  created_by: string;
}

export interface UpdateProductCategoryDTO {
  name: string;
  description: string;
  partner_id: string;
  slug: string;
  products: any[];
  product_category_id: string;
  IsActive: boolean;
  updated_by: string;
}

export interface CreateProductDTO {
  name: string;
  description: string;
  product_code: string;
  uom: string;
  min_quantity: number;
  max_quantity: number;
  partner_id: string;
  product_category_id: string;
  image_path: string;
  slug: string;
  created_by: string;
  ProductInventory: any[];
}

//Update Product inventry API request DTO
export interface UpdateProductDTO {
  name?: string;
  description?: string;
  product_code?: string;
  uom?: string;
  min_quantity?: number;
  max_quantity?: number;
  partner_id?: string;
  product_category_id?: string;
  image_path?: string;
  slug?: string;
  updated_by?: string;
  IsActive?: boolean | string;
  product_id: string;
  ProductInventory: any[];
}
