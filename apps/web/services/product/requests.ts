import { callApi } from "@/lib";
import {
  CreateProductCategoryDTO,
  CreateProductDTO,
  UpdateProductCategoryDTO,
  UpdateProductDTO,
} from "./dto";

export const getAllActiveProduct = (id: string) => {
  return callApi(`/product/active/${id}`, {}, "GET");
};

export const getAllActiveProductCategory = (id: string) => {
  return callApi(`/product-category/active/${id}`, {}, "GET");
};

export const getAllProduct = (id: string, queryParams: any) => {
  const queryString = new URLSearchParams(queryParams).toString();
  return callApi(`/product/${id}?${queryString}`, {}, "GET");
};

export const createProduct = (body: CreateProductDTO) => {
  return callApi(`/product`, { product: body }, "POST", false);
};

export const updateProduct = (body: UpdateProductDTO) => {
  return callApi(
    `/product/${body.product_id}`,
    {
      product: {
        product_code: body.product_code,
        name: body.name,
        description: body.description,
        uom: body.uom,
        min_quantity: body.min_quantity,
        max_quantity: body.max_quantity,
        product_category_id: body.product_category_id,
        image_path: body.image_path,
        slug: body.slug,
        partner_id: body.partner_id,
        IsActive: body.IsActive,
        updated_by: body.updated_by,
      },
    },
    "PUT",
    false
  );
};

export const getAllProductCategory = async (id: string, queryParams: any) => {
  const queryString = new URLSearchParams(queryParams).toString();
  return callApi(`/product-category/product/${id}?${queryString}`, {}, "GET");
};

export const createProductCategory = (data: CreateProductCategoryDTO) => {
  return callApi(`/product-category`, { productCategory: data }, "POST", false);
};

export const updateProductCategory = (data: UpdateProductCategoryDTO) => {
  return callApi(
    `/product-category/${data.product_category_id}`,
    {
      productCategory: {
        name: data.name,
        description: data.description,
        partner_id: data.partner_id,
        slug: data.slug,
        products: data.products,
        updated_by: data.updated_by,
        IsActive: data.IsActive,
      },
    },
    "PUT",
    false
  );
};
