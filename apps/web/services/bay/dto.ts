export interface BayT {
  id: string;
  name: string;
  description: string;
  partner_id: string;
  slug: string;
  is_active: boolean;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string | null;
}

export interface CreateBayDTO {
  name: string;
  description: string;
  partner_id: string;
  created_by: string;
  slug: string;
}

export interface UpdateBayDTO {
  bay_id: string;
  name: string;
  description: string;
  partner_id: string;
  slug: string;
  is_active: boolean;
  updated_by: string;
}
