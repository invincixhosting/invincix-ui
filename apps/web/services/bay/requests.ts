import { callApi } from "@/lib";
import { CreateBayDTO, UpdateBayDTO } from "./dto";

export const getAllBay = (id: string, queryParams: any) => {
  const queryString = new URLSearchParams(queryParams).toString();
  return callApi(`/bay/${id}?${queryString}`, {}, "GET");
};

export const createBay = (data: CreateBayDTO) => {
  return callApi(`/bay/`, { bay: data }, "POST", false);
};

export const updateBay = (data: UpdateBayDTO) => {
  return callApi(
    `/bay/${data.bay_id}`,
    {
      bay: {
        name: data.name,
        description: data.description,
        partner_id: data.partner_id,
        slug: data.slug,
        updated_by: data.updated_by,
        IsActive: data.is_active,
      },
    },
    "PUT",
    false
  );
};
