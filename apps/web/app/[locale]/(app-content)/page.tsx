import { getCurrentUser } from "@/lib";

export default async function Page() {
  const user = await getCurrentUser();

  return (
    <div className="relative flex min-h-screen flex-col space-y-1 md:space-y-6">
      <main className="container flex flex-col pb-4 md:grid flex-1 gap-12 ">
        Hello
      </main>
    </div>
  );
}
