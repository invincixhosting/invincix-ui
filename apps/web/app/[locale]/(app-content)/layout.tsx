import { getCurrentUser } from "@/lib";
import { Layout as AuthLayout } from "@/components/layout/layout";
import { redirect } from "next/navigation";
import * as PATH from "@/const/paths";

interface LayoutProps {
  children: React.ReactNode;
  params: { locale: string };
}

export default async function Layout({ children, params }: LayoutProps) {
  // const user = await getCurrentUser();

  // if (!user) {
  //   redirect(PATH.LOGIN);
  // } else {
  // switch (user.transformedData[0].rolename) {
  //   case "Mechanic":
  //     redirect(PATH.MECHANIC_PENDING_TASK);
  //   case "Partner":
  //     redirect(PATH.PARTNER_HOME);
  //   default:
  //     redirect(PATH.LOGIN);
  // }
  // }

  return (
    <AuthLayout locale={params.locale} user={user}>
      {children}
    </AuthLayout>
  );
}
