import { authOptions } from "@/const/authOptions";
import "@ui/styles/globals.css";
import { getServerSession } from "next-auth";
import ThemeProvider from "ui/containers/theme-provider";
import AuthProvider from "ui/containers/auth-provider";
import QueryWrapper from "ui/containers/query-wrapper";
import { Toaster } from "ui/components/ui/toaster";
import { cn } from "ui/lib/utils";
import type { Metadata } from "next";

interface RootLayoutProps {
  children: React.ReactNode;
  params: {
    locale: string;
  };
}

export const metadata: Metadata = {
  title: "Telto Studio",
};

export default async function RootLayout({
  children,
  params,
}: RootLayoutProps) {
  const session = await getServerSession(authOptions);

  return (
    <html lang={params.locale} suppressHydrationWarning>
      <head>
        <link rel="icon" href="/favicon.ico" sizes="any" />
      </head>
      <QueryWrapper>
        <body className={cn("min-h-screen font-sans antialiased")}>
          <AuthProvider session={session}>
            <ThemeProvider
              attribute="class"
              defaultTheme="dark"
              enableSystem={false}
            >
              {children}
              <Toaster />
            </ThemeProvider>
          </AuthProvider>
        </body>
      </QueryWrapper>
    </html>
  );
}
