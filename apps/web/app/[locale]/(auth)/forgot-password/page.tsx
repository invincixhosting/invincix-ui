"use client";

import { signIn } from "next-auth/react";
import { useSearchParams, useRouter } from "next/navigation";
import { useState } from "react";
import { Input } from "@ui/components/form";
import { useForm } from "react-hook-form";
import { AlertTriangle } from "lucide-react";
import { Button } from "@ui/components/common";
import { PasswordInput } from "@ui/components/form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { Form } from "@ui/components/ui/form";
import { Alert } from "@ui/components/common/alert";
import { useToast } from "@ui/components/ui/use-toast";
import Mail from "@ui/icons/Mail";
import "../styles.css";
import { useTranslation } from "@/i18n/client";
import { LocaleTypes } from "@/i18n/settings";

const formSchema = z.object({
  email: z.string().min(2).max(50).email(),
});

export default function Page({ params }: { params: { locale: LocaleTypes } }) {
  const { t } = useTranslation(params.locale, "auth");
  const { toast } = useToast();
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
    },
  });
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  const searchParams = useSearchParams();
  const callbackUrl = searchParams.get("callbackUrl") || "/";

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      setLoading(true);

      const res = await signIn("credentials", {
        redirect: false,
        email: values.email,
        callbackUrl,
      });

      setLoading(false);

      if (res?.status && res?.status < 300 && res?.status >= 200) {
        toast({
          variant: "success",
          title: "Successfully logged in!",
          description: "Welcome!",
        });
        setTimeout(() => {
          router.push(callbackUrl);
        }, 500);
        setError("");
      } else {
        setError("Invalid email or password");
      }
    } catch (error: any) {
      setLoading(false);
      setError(error);
    }
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <div className="mx-8 my-8 z-50">
          {Boolean(error) && (
            <div className="my-3">
              <Alert
                icon={<AlertTriangle className="h-4 w-4" />}
                variant="destructive"
                title="Error"
                description={error}
              />
            </div>
          )}
          <div className="mb-3 mt-5">
            <Input
              form={form}
              variant="outline"
              name="email"
              endIcon={<Mail />}
              placeholder={t("form.email")}
            />
          </div>
        </div>
        <div className="flex justify-end">
          <div className="flex space-x-2 p-4 rounded-tl-lg bg-[#1D2026] action-box">
            <Button
              className="bg-white"
              size="lg"
              type="submit"
              loading={loading}
            >
              {t("submit")}
            </Button>
            <Button
              className="border-white text-white"
              size="lg"
              variant="outline"
              onClick={() => {
                router.push("/login");
              }}
            >
              {t("back_to_login")}
            </Button>
          </div>
        </div>
      </form>
    </Form>
  );
}
