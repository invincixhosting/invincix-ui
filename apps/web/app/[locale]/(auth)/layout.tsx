import TeltoLogo from "@/media/logos/TeltoLogo.svg";
import LoginLock from "@/media/images/LoginLock.svg";
import ProductLogo from "@/media/logos/ProductLogo.svg";
import CarVector from "@/media/images/LoginBg2.svg";
import ToolsVector from "@/media/images/LoginBg1.svg";
import LoginLine1 from "@/media/images/LoginLine1.svg";
import LoginLine2 from "@/media/images/LoginLine2.svg";
import AUnitInvincix from "@/media/images/AUnitInvincix.svg";
import GreetingRobotLeft from "@/media/images/GreetingRobotLeft.svg";
import Image from "next/image";
import "./styles.css";
import { LocaleTypes } from "@/i18n/settings";
import { createTranslation } from "@/i18n/server";
import ThemeButton from "@/components/theme-button";
import LanguageButton from "@/components/language-button";

export default async function Layout({
  children,
  params,
}: {
  children: React.ReactNode;
  params: { locale: LocaleTypes };
}) {
  const { t } = await createTranslation(params.locale, "auth");

  return (
    <div className="bg-[#6B6B6B] min-h-screen max-h-screen min-w-screen md:p-4 p-0 text-white overflow-hidden flex items-center justify-center">
      <div className="min-h-[calc(100vh-2rem)] max-h-[calc(100vh-2rem)] min-w-[calc(100vw-2rem)] max-w-[calc(100vw-2rem)] py-6 px-8 rounded-lg bg-[#1D2026] flex flex-col justify-between">
        <div className="flex items-center justify-between">
          <div>
            <Image src={TeltoLogo} alt="telto logo" width={120} height={50} />
            <p>{t("telto_brief")}</p>
          </div>
          <div className="hidden md:block relative top-[-20px]">
            <Image src={LoginLine1} alt="line" />
          </div>
          <div className="flex items-center space-x-3">
            <ThemeButton />
            <LanguageButton locale={params.locale} />
          </div>
        </div>
        <div className="grid grid-cols-3 gap-0">
          <div className="hidden md:block md:col-span-2 relative z-10">
            <div className="flex justify-end relative right-[200px]">
              <Image src={ToolsVector} alt="Tools vector" />
            </div>
            <div className="absolute left-[-25px] top-[30%] scale-110 z-10">
              <Image src={LoginLine2} alt="line" />
            </div>
            <div className="relative left-[250px] scale-150">
              <Image src={CarVector} alt="Car vector" />
            </div>
          </div>
          <div className="col-span-3 md:col-span-1">
            <div className="rounded-lg bg-gradient-to-b from-[#7700D561] to-[#056EA3] pt-8">
              <div className="flex items-center justify-center">
                <Image src={LoginLock} alt="Lock" />
              </div>
              <div className="relative z-50">{children}</div>
            </div>
          </div>
        </div>
        <div className="flex items-end justify-between">
          <Image src={ProductLogo} alt="product logo" />
          <p className="text-muted hidden md:block">
            ©2023. exprodedge. All Rights Reserved.
          </p>
          <div className="flex space-x-3 items-end">
            <Image src={AUnitInvincix} alt="Invincix Unit" />
            <div className="">
              <Image src={GreetingRobotLeft} alt="greeting robot" />
            </div>
          </div>
        </div>
        <p className="text-muted block md:hidden text-center">
          ©2023. exprodedge. All Rights Reserved.
        </p>
      </div>
    </div>
  );
}
