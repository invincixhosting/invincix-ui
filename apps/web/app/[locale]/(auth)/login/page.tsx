"use client";

import { signIn } from "next-auth/react";
import { useSearchParams, useRouter } from "next/navigation";
import { useState } from "react";
import { Input } from "@ui/components/form";
import { useForm } from "react-hook-form";
import { AlertTriangle } from "lucide-react";
import { Button } from "@ui/components/common";
import { PasswordInput } from "@ui/components/form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { Form } from "@ui/components/ui/form";
import { Alert } from "@ui/components/common/alert";
import { useToast } from "@ui/components/ui/use-toast";
import Mail from "@ui/icons/Mail";
import "../styles.css";
import Link from "next/link";
import { useTranslation } from "@/i18n/client";
import { LocaleTypes } from "@/i18n/settings";

const formSchema = z.object({
  email: z.string().min(2).max(50).email(),
  password: z.string(),
});

export default function Page({ params }: { params: { locale: LocaleTypes } }) {
  const { toast } = useToast();
  const { t } = useTranslation(params.locale, "auth");
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
      password: "",
    },
  });
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  const searchParams = useSearchParams();
  const callbackUrl = searchParams.get("callbackUrl") || "/";

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      setLoading(true);

      const res = await signIn("credentials", {
        redirect: false,
        email: values.email,
        password: values.password,
        callbackUrl,
      });

      setLoading(false);

      if (res?.status && res?.status < 300 && res?.status >= 200) {
        toast({
          variant: "success",
          title: t("login_success"),
          description: "Welcome!",
        });
        setTimeout(() => {
          router.push(callbackUrl);
        }, 500);
        setError("");
      } else {
        setError(t("invalid_email_or_password"));
      }
    } catch (error: any) {
      setLoading(false);
      setError(error);
    }
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <div className="mx-8 my-8 z-50">
          {Boolean(error) && (
            <div className="my-3">
              <Alert
                icon={<AlertTriangle className="h-4 w-4" />}
                variant="destructive"
                title="Error"
                description={error}
              />
            </div>
          )}
          <div className="mb-3 mt-5">
            <Input
              form={form}
              variant="outline"
              name="email"
              endIcon={<Mail />}
              placeholder={t("form.email")}
            />
          </div>
          <div className="mb-2">
            <PasswordInput
              variant="outline"
              name="password"
              form={form}
              placeholder={t("form.password")}
            />
          </div>
          <div className="text-right mb-5">
            <Link href="/forgot-password">{t("forgot_password")}</Link>
          </div>
        </div>
        <div className="flex justify-end">
          <div className="flex space-x-2 p-4 rounded-tl-lg bg-[#1D2026] action-box">
            <Button
              className="bg-white"
              size="lg"
              type="submit"
              loading={loading}
            >
              {t("login")}
            </Button>
            <Button
              className="border-white text-white"
              size="lg"
              variant="outline"
            >
              {t("register")}
            </Button>
          </div>
        </div>
      </form>
    </Form>
  );
}
