interface TopItemsT {
  name: string;
  image: string;
  count: number;
}

export interface OperationDashboardT {
  pendingCount: number;
  inProgressCount: number;
  readyCount: number;
  completeCount: number;
  newCustomerCount: number;
  repeatCustomerCount: number;
  topThreeServiceNames: TopItemsT[];
  topThreeProductNames: TopItemsT[];
  topThreeVehicleTypes: string[];
  totalVehiclesServedCount: number;
  successRate: string;
  revenueByMonth: {};
}

interface RevenueStockT {
  label: string;
  data: number[];
  backgroundColor: string;
}

export interface RevenueDashboardT {
  total_revenue: number;
  total_expense: number;
  total_profit: number;
  percentageProfit: string;
  percentageExpense: string;
  stock: RevenueStockT[];
  monthly_expense_totals: {
    [key: string]: number;
  };
  monthly_revenue_totals: {
    [key: string]: number;
  };
}

export interface ReviewDashboardT {
  total_customer: number;
  customerSatisfaction_Index: number;
  mechanicSatisfaction_Index: number;
  starPerformerNames: string[];
  total_reviews: number;
}

export interface CustomerFeedbackT {
  customerFeedback: {
    id: string;
    jobCard_id: string;
    customer_id: string;
    rating: string;
    remarks: string;
    platform_rating: string;
    platform_remarks: string;
    partner_id: string;
    created_at: string;
    modified_at: string;
    customer_name: string;
  }[];
}

export interface MechanicFeedbackT {
  mechanicFeedback: {
    id: string;
    mechanic_id: string;
    partner_id: string;
    rating: string;
    review: string;
    created_at: string;
    modified_at: string;
    mechanic_name: string;
  }[];
}
