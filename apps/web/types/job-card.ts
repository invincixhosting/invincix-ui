export interface JobCardT {
  id: string;
  jobCard_id: string;
  partner_id: string;
  task: string;
  duration: string;
  assigned_to: string;
  status: string;
  start_time: string;
  record_time: string;
  hour_log_time: string;
  end_time: string;
  module: string;
  module_id: string;
  is_active: boolean;
  is_deleted: boolean;
  jobcard_serviceId: string;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
  details: JobCardDetailT[];
  bay_name: string;
  assigned_by: string;
}

export interface SpareT {
  id: string;
  jobCard_id: string;
  partner_id: string;
  product_id: string;
  quantity: string;
  price: string | null | number;
  is_estimated: string;
  is_final: boolean | null;
  is_active: boolean;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
  product_name: string;
  product_image: string;
}

export interface ServiceT {
  id: string;
  jobCard_id: string;
  partner_id: string;
  service_id: string;
  service_price: string;
  is_estimated: string;
  assigned_to: string;
  jobcard_serviceId: string;
  is_final: null;
  is_active: boolean;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
  service_name: string;
  service_image: string;
  task_status: string;
}

export interface TaskT {
  id: string;
  jobCard_id: string;
  partner_id: string;
  task: string;
  duration: string;
  assigned_to: string;
  status: string;
  start_time: string;
  record_time: string;
  hour_log_time: string | null;
  end_time: string | null;
  module: string;
  module_id: string;
  is_active: boolean;
  is_deleted: boolean;
  jobcard_serviceId: string;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
}

export interface ComplaintT {
  id: string;
  jobCard_id: string;
  partner_id: string;
  complaints: string;
  assigned_to: string;
  duration: string;
  jobcard_serviceId: string;
  is_active: boolean;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
  task_status: string;
}

export interface JobCardDetailT {
  id: string;
  vehicle_id: string;
  customer_id: string;
  kilometer_driven: number;
  status: string;
  bay: string;
  partner_id: string;
  is_active: boolean;
  is_deleted: boolean;
  slug: string;
  estimation_status: string | null;
  estimation_data: any;
  image_paths: string[];
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
  vehicle_number: string;
  color: string;
  model_number: string;
  customer_name: string;
  email: string;
  phone_number: string;
  address: string;
  spare: SpareT[];
  services: ServiceT[];
  tasks: TaskT[];
  complaints: ComplaintT[];
  extraService: any[];
}
