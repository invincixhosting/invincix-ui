export interface CategoryT {
  name: string;
  functions: string[];
}

export interface TransformedDataT {
  roleid: string;
  rolename: string;
  category: CategoryT[];
}

export interface CurrencyT {
  id: string;
  name: string;
  exchange_rate: string;
  code: string;
  symbol: string;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
}

export interface UserT {
  id: string;
  email: string;
  name: string;
  resetCode: string | null;
  resetCodeExpiresAt: string | null;
  role: string[];
  user_type: string;
  partner_id: string;
  is_loginallowed: boolean;
  specialities: string[];
  slug: string;
  is_active: boolean;
  created_at: string;
  created_by: string;
  modified_at: string;
  modified_by: string;
  access_token: string;
  tax_percentage: number;
  logo: string;
  currency_data: CurrencyT;
  transformedData: TransformedDataT[];
}
