import {
  Home,
  ClipboardList,
  ClipboardCheck,
  Settings,
  BarChartBig,
  Users2,
  Receipt,
  DollarSign,
  Package,
  Archive,
} from "lucide-react";
import * as PATH from "./paths";

export const getUserMenu = (locale: string, userRole: string, t: any) => {
  const addLocaleToLink = (link: string) => {
    const pathWithLocale = `/${locale}${link}`;
    return pathWithLocale;
  };

  const transformMenu = (originalMenu) => {
    return originalMenu.map((item) => {
      return {
        ...item,
        link: addLocaleToLink(item.link),
        title: t("menu." + item.title),
      };
    });
  };

  switch (userRole) {
    case "Mechanic":
      return transformMenu(MechanicMenus);
    case "Supervisor":
      return transformMenu(SupervisorMenus);
    case "Partner":
      return transformMenu(PartnerMenus);
    case "Owner":
      return transformMenu(OwnerMenus);
    default:
      return [];
  }
};

interface MenuT {
  icon: React.ReactNode;
  link: string;
  title: string;
}

export const MechanicMenus: MenuT[] = [
  {
    icon: <ClipboardList />,
    title: "pending_tasks",
    link: PATH.MECHANIC_PENDING_TASK,
  },
  {
    icon: <ClipboardCheck />,
    title: "completed_tasks",
    link: PATH.MECHANIC_COMPLETED_TASK,
  },
];

export const PartnerMenus: MenuT[] = [
  {
    icon: <Home />,
    title: "home",
    link: PATH.PARTNER_HOME,
  },
  {
    icon: <Settings />,
    title: "settings",
    link: PATH.PARTNER_SETTINGS,
  },
  {
    icon: <BarChartBig />,
    title: "reports",
    link: PATH.PARTNER_REPORTS,
  },
  {
    icon: <Users2 />,
    title: "teams",
    link: PATH.PARTNER_TEAMS,
  },
  {
    icon: <Receipt />,
    title: "expenses/revenue_log",
    link: PATH.PARTNER_EXPENSES_REVENUE_LOG,
  },
];

export const OwnerMenus: MenuT[] = [
  {
    icon: <Home />,
    title: "home",
    link: PATH.OWNER_HOME,
  },
  {
    icon: <Settings />,
    title: "settings",
    link: PATH.OWNER_SETTINGS,
  },
];

export const SupervisorMenus: MenuT[] = [
  {
    icon: <Home />,
    title: "home",
    link: PATH.SUPERVISOR_HOME,
  },
  {
    icon: <ClipboardList />,
    title: "job_card",
    link: PATH.SUPERVISOR_JOB_CARD,
  },
  {
    icon: <DollarSign />,
    title: "payment",
    link: PATH.SUPERVISOR_PAYMENT,
  },
  {
    icon: <Package />,
    title: "spare_part_request",
    link: PATH.SUPERVISOR_SPARE_PART_REQUEST,
  },
  {
    icon: <Settings />,
    title: "configuration",
    link: PATH.SUPERVISOR_CONFIGURATION,
  },
  {
    icon: <Users2 />,
    title: "teams",
    link: PATH.SUPERVISOR_SPARE_TEAMS,
  },
  {
    icon: <Archive />,
    title: "archive_job_card",
    link: PATH.SUPERVISOR_SPARE_ARCHIVE_JOB_CARD,
  },
];
