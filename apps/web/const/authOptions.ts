import Credentials from "next-auth/providers/credentials";
import { AuthOptions } from "next-auth";

export const authOptions: AuthOptions = {
  providers: [
    Credentials({
      name: "Telto Studio",
      credentials: {
        email: {
          label: "email",
          type: "email",
          placeholder: "jsmith@example.com",
        },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials, req) {
        const payload = {
          email: credentials?.email,
          password: credentials?.password,
        };

        const res = await fetch(
          process.env.NEXTAUTH_API_ENDPOINT + "/auth/login",
          {
            method: "POST",
            body: JSON.stringify(payload),
            headers: {
              "Content-Type": "application/json",
            },
          }
        );

        const data = await res.json();
        if (data.statusCode >= 300) {
          throw new Error(data.message);
        }
        // If no error and we have data data, return it
        if (data.statusCode >= 200 && data.statusCode < 300 && data) {
          return data;
        }

        // Return null if user data could not be retrieved
        return null;
      },
    }),
  ],
  session: {
    maxAge: 30 * 24 * 60 * 60, // 30 days
  },
  pages: {
    signIn: "/",
  },
  callbacks: {
    async jwt({ token, user, account, profile }) {
      // This user return by provider {} as you mentioned above MY CONTENT {token:}
      if (user) {
        token.data = user;
      }
      return token;
    },

    session: async ({
      session,
      user,
      token,
    }: {
      session: any;
      user: any;
      token: any;
    }) => {
      // this token return above jwt()

      let _user = {
        ...token.data?.user,
        access_token: token?.data?.token?.access_token,
        transformedData: token?.data?.transformedData,
        tax_percentage: token?.data?.tax_percentage,
        logo: token?.data?.image_path,
        currency_data: token?.data?.currency_data,
      };

      session.user = _user;
      session.accessToken = token.data?.accessToken;
      session.refreshToken = token.data?.refreshToken;
      return {
        ...session,
        user: _user,
      };
    },
  },
};
