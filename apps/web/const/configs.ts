export const BRAND_NAME = "VERTICI";
export const BRAND_LINK = "/";
export const BRAND_LOGO_PATH = "@/media/logos/VerticiStudio.svg";
export const SIDEBAR_LOGO_PATH = "@/media/logos/greeting-robot.svg";