import { authOptions } from "@/const/authOptions";
import { getServerSession } from "next-auth/next";
import { getSession } from "next-auth/react";
import { redirect } from "next/navigation";
import * as PATH from "@/const/paths";
import { first } from "lodash";

export async function getCurrentUser(isServerSide: boolean = true) {
  const session = await (isServerSide
    ? getServerSession(authOptions)
    : getSession());
  if (session?.user) {
    return session?.user;
  } else {
    redirect(PATH.LOGIN);
  }
}

export async function validateUserPath() {
  const user = await getCurrentUser();
  const userRole = first(user?.transformedData)?.rolename || user?.user_type;
  console.log(
    "🚀 ~ file: index.ts:16 ~ validateUserPath ~ userRole:",
    userRole
  );

  if (!user) {
    redirect(PATH.LOGIN);
  } else {
    switch (userRole) {
      case "Supervisor":
        redirect(PATH.SUPERVISOR_HOME);
      case "Mechanic":
        redirect(PATH.MECHANIC_PENDING_TASK);
      case "Partner":
        redirect(PATH.PARTNER_HOME);
      case "Owner":
        redirect(PATH.OWNER_HOME);
      default:
        redirect(PATH.LOGIN);
    }
  }
}

export function millisToMinutesAndSeconds(millis: any) {
  var minutes = Math.floor(millis / 60000);
  var seconds: any = ((millis % 60000) / 1000).toFixed(0);
  return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
}

export function convertMinutes(minutes: any) {
  let days = Math.floor(minutes / 1440); // 1 day = 1440 minutes
  let hours = Math.floor((minutes % 1440) / 60); // 1 hour = 60 minutes
  let remainingMinutes = minutes % 60;

  if (days > 0) {
    return `${days} day${days > 1 ? "s" : ""}${
      hours > 0 ? `, ${hours} hour${hours > 1 ? "s" : ""}` : ""
    }${
      remainingMinutes > 0
        ? `, ${remainingMinutes} minute${remainingMinutes > 1 ? "s" : ""}`
        : ""
    }`;
  } else if (hours > 0) {
    return `${hours} hour${hours > 1 ? "s" : ""}${
      remainingMinutes > 0
        ? `, ${remainingMinutes} minute${remainingMinutes > 1 ? "s" : ""}`
        : ""
    }`;
  } else {
    return `${remainingMinutes} minute${remainingMinutes > 1 ? "s" : ""}`;
  }
}

export async function callApi(
  url: string,
  payload: object,
  method: "POST" | "GET" | "DELETE" | "PUT",
  isServerSide: boolean = true
) {
  const fullUrl = process.env.NEXT_PUBLIC_API_ENDPOINT + url;
  const token = (await getCurrentUser(isServerSide))?.access_token;
  if (Boolean(token)) {
    try {
      let res;
      if (method == "GET") {
        res = await fetch(fullUrl, {
          headers: {
            "Content-Type": "application/json",
            Accept: "*",
            Authorization: `Bearer ${token}`,
          },
        });
      } else {
        res = await fetch(fullUrl, {
          method: method,
          body: JSON.stringify(payload || {}),
          headers: {
            "Content-Type": "application/json",
            Accept: "*",
            Authorization: `Bearer ${token}`,
          },
        });
      }
      const data = await res.json();
      if (data.statusCode >= 300) {
        if (isServerSide) {
          redirect(PATH.LOGIN);
        }
      }
      // If no error and we have data data, return it
      if (data) {
        return data;
      }

      // Return null if user data could not be retrieved
      return null;
    } catch (error) {
      console.log("🚀 ~ file: index.ts:60 ~ error:", error);
      return null;
    }
  } else {
    redirect(PATH.LOGIN);
  }
}
