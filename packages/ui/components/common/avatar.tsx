import { Avatar as SAvatar, AvatarFallback, AvatarImage } from "../ui/avatar";

interface PropsT {
  imgSrc: string;
  imgAlt: string;
  fallback: React.ReactNode;
  className?: string;
}

export function Avatar(props: PropsT) {
  return (
    <SAvatar className={`border border-[#DEFA8E] ${props?.className || ""}`}>
      <AvatarImage src={props.imgSrc} alt={props.imgAlt} />
      <AvatarFallback>{props.fallback}</AvatarFallback>
    </SAvatar>
  );
}
