export default function Moon({
  size = 20,
  color = "white",
  className = "",
}: {
  size?: number;
  color?: string;
  className?: string;
}) {
  return (
    <svg
      className={className}
      width={size}
      height={size}
      viewBox="0 0 21 21"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M10.9605 20.7907C16.4834 20.7907 20.9605 16.3135 20.9605 10.7907C20.9605 10.328 20.267 10.2515 20.0278 10.6475C18.8895 12.5313 16.8221 13.7907 14.4605 13.7907C10.8707 13.7907 7.96051 10.8805 7.96051 7.29068C7.96051 4.92913 9.21988 2.86173 11.1037 1.72344C11.4997 1.48415 11.4232 0.79068 10.9605 0.79068C5.43766 0.79068 0.96051 5.26783 0.96051 10.7907C0.96051 16.3135 5.43766 20.7907 10.9605 20.7907Z"
        fill={color}
      />
    </svg>
  );
}
