export default function Bell({
  size = 20,
  color = "white",
  className = "",
}: {
  size?: number;
  color?: string;
  className?: string;
}) {
  return (
    <svg
      className={className}
      width={size}
      height={size}
      viewBox="0 0 22 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M10.6641 19.3226C17.0853 19.3226 20.0559 18.4989 20.3428 15.1925C20.3428 11.8884 18.2717 12.1009 18.2717 8.04687C18.2717 4.88026 15.2703 1.27734 10.6641 1.27734C6.05787 1.27734 3.05642 4.88026 3.05642 8.04687C3.05642 12.1009 0.985352 11.8884 0.985352 15.1925C1.27338 18.5114 4.24397 19.3226 10.6641 19.3226Z"
        stroke={color}
        strokeWidth="1.70801"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M13.3844 22.7495C11.8311 24.4743 9.40801 24.4947 7.83984 22.7495"
        stroke={color}
        strokeWidth="1.70801"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}
